package com.mydriver.driver.view.reservationlist;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.mydriver.driver.R;
import com.mydriver.driver.view.base.ButterKnifeEventReportingFragment;
import com.sixt.core.eventbus.SxEventBus;

import butterknife.InjectView;

public class ReservationListTabFragment extends ButterKnifeEventReportingFragment implements ReservationListTabView {

    @InjectView(R.id.tabs_reservations)
    PagerSlidingTabStrip mTabs;

    @InjectView(R.id.view_pager_reservations)
    ViewPager mViewPager;

    private ReservationListTabPresenter mPresenter;
    private ReservationListTabPagerAdapter mPagerAdapter;

    public static ReservationListTabFragment newInstance() {
        return new ReservationListTabFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        mPresenter = new ReservationListTabPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reservation_list_tab_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mPagerAdapter == null) {
            mPagerAdapter = new ReservationListTabPagerAdapter(getChildFragmentManager(), getActivity(), mPresenter.getUnseenReservations());
        }
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setCurrentItem(0);
        mTabs.setViewPager(mViewPager);
        mTabs.setTabPaddingLeftRight(0);
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.start();
    }

    @Override
    public void onStop() {
        mPresenter.stop();
        super.onStop();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_reservation_list, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem showNotificationItem = menu.findItem(R.id.menu_rides_show_notifications);
        showNotificationItem.setVisible(mPresenter.isPushNotificationsToggleMenuItemEnabled());
        if (mPresenter.isPushNotificationsEnabled()) {
            showNotificationItem.setIcon(R.drawable.ico_notifications_enabled_white);
        } else {
            showNotificationItem.setIcon(R.drawable.ico_notifications_disabled_white);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_rides_call_hotline:
                SxEventBus.postEvent(new HotlineNumberListDoUpdateEvent());
                break;
            case R.id.menu_rides_show_notifications:
                mPresenter.switchPushNotificationsEnabled();
                break;
        }
        return true;
    }

    @Override
    public void updatePushNotificationStatus() {
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void updateUnseenReservationCount(int unseenReservations) {
        mPagerAdapter.setUnseenReservations(unseenReservations);
        mTabs.notifyDataSetChanged();
    }
}