package com.mydriver.driver.controller;


import android.content.Intent;
import android.net.Uri;

import com.mydriver.driver.MainApplication;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;

public class NavigateToAddressPluginController extends SxPluginController {

    public void onEvent(NavigateToAddressEvent event) {
        Uri navigationUri =  Uri.parse("geo:0,0?q=" + event.getDestination().latitude + "," + event.getDestination().longitude + " (" + event.getName() + ")");
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, navigationUri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MainApplication.getInstance().startActivity(intent);
    }
}
