package com.mydriver.driver.view.reservationdetails.reassigndriver;

import com.mydriver.driver.view.base.GenericListItem;

import java.util.List;

interface DriverColleagueListView {
    public void drawDriverColleagues(List<GenericListItem> items);
    public void hideProgressBar();
}
