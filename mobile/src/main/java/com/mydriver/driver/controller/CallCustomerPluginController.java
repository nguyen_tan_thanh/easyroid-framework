package com.mydriver.driver.controller;


import android.content.Intent;
import android.net.Uri;

import com.mydriver.driver.MainApplication;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;

import org.apache.commons.lang3.StringUtils;

public class CallCustomerPluginController extends SxPluginController {

    public void onEvent(CallCustomerEvent event) {
        if (StringUtils.isEmpty(event.getPhoneNumber())) {
            return;
        }
        doCallCustomer(event.getPhoneNumber());
    }

    private void doCallCustomer(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MainApplication.getInstance().startActivity(intent);
    }
}
