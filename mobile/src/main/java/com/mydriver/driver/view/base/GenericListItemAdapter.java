package com.mydriver.driver.view.base;


import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.mydriver.driver.R;
import com.sixt.core.eventbus.SxEventBus;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class GenericListItemAdapter extends RecyclerView.Adapter<GenericListItemAdapter.ViewHolder>{

    private final List<GenericListItem> items = new ArrayList<>();
    private OnItemSelectedListener onItemSelectedListener;
    private View.OnCreateContextMenuListener onCreateContextMenuListener;
    private int lastLongClickPosition = -1;
    private final int layoutResourceId;

    public static interface OnItemSelectedListener {
        void onItemSelected(GenericListItem item, int position);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.genericListItemTitle) TextView mTitleTextView;
        @InjectView(R.id.genericListItemSubTitle) TextView mSubTitleTextView;
        @InjectView(R.id.genericListItemIcon) ImageView mIcon;
        @InjectView(R.id.genericListItemActionButton) ImageButton mActionButton;
        @InjectView(R.id.genericListItemMainContainer) ViewGroup mMainContainer;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

    public GenericListItemAdapter() {
        this(R.layout.generic_list_item);
    }

    public GenericListItemAdapter(@LayoutRes int layoutResourceId) {
        this.layoutResourceId = layoutResourceId;
    }

    @Override
    public GenericListItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutResourceId, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        if (onCreateContextMenuListener != null) {
            viewHolder.mMainContainer.setOnCreateContextMenuListener(onCreateContextMenuListener);
            viewHolder.mMainContainer.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    lastLongClickPosition = viewHolder.getPosition();
                    return false;
                }
            });
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GenericListItemAdapter.ViewHolder viewHolder, int position) {
        final GenericListItem itemModel = items.get(position);
        viewHolder.mTitleTextView.setText(itemModel.getTitle());
        if (itemModel.getIconResourceId() == GenericListItem.NO_ICON_RES_ID) {
            viewHolder.mIcon.setVisibility(View.GONE);
        } else {
            viewHolder.mIcon.setVisibility(View.VISIBLE);
            viewHolder.mIcon.setImageResource(itemModel.getIconResourceId());
        }
        if (StringUtils.isEmpty(itemModel.getSubTitle())) {
            viewHolder.mSubTitleTextView.setVisibility(View.GONE);
        } else {
            viewHolder.mSubTitleTextView.setVisibility(View.VISIBLE);
            viewHolder.mSubTitleTextView.setText(itemModel.getSubTitle());
        }

        if (itemModel.getActionIconResourceId() == GenericListItem.NO_ICON_RES_ID) {
            viewHolder.mActionButton.setVisibility(View.GONE);
        } else {
            viewHolder.mActionButton.setVisibility(View.VISIBLE);
            viewHolder.mActionButton.setImageResource(itemModel.getActionIconResourceId());
        }
        if (itemModel.getActionEvent() == null) {
            viewHolder.mActionButton.setOnClickListener(null);
        } else {
            viewHolder.mActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SxEventBus.postEvent(itemModel.getActionEvent());
                }
            });
        }
        if (onItemSelectedListener == null) {
            viewHolder.mMainContainer.setOnClickListener(null);
            viewHolder.mMainContainer.setClickable(false);
            viewHolder.mMainContainer.setFocusable(false);
        } else {
            viewHolder.mMainContainer.setClickable(true);
            viewHolder.mMainContainer.setFocusable(true);
            viewHolder.mMainContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemSelectedListener != null) {
                        onItemSelectedListener.onItemSelected(itemModel, items.indexOf(itemModel));
                    }
                }
            });
        }
        viewHolder.mMainContainer.setSelected(itemModel.isSelected());
        viewHolder.mMainContainer.setMinimumHeight(viewHolder.mMainContainer.getResources().getDimensionPixelSize(R.dimen.Android_Design_rhythmOneAndASixth));
    }

    public void setOnItemSelectedListener(OnItemSelectedListener listener) {
        onItemSelectedListener = listener;
    }

    public void setOnCreateContextMenuListener(View.OnCreateContextMenuListener onCreateContextMenuListener) {
        this.onCreateContextMenuListener = onCreateContextMenuListener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public GenericListItem getItem(int position) {
        return items.get(position);
    }

    public GenericListItem getItemForContextMenu() {
        if (onCreateContextMenuListener == null || lastLongClickPosition < 0) {
            throw new IllegalStateException("No contextMenuListener registred or no item for context menu found");
        }

        return getItem(lastLongClickPosition);
    }

    public void setItems(List<GenericListItem> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }
}
