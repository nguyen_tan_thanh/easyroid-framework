package com.mydriver.driver.view.reservationdetails;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mydriver.driver.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ReservationProgressBar extends RelativeLayout {

    private static final int ANIMATION_DURATION_IN_MILLISECONDS = 500;

    public static enum Step {
        NO_STEP(-1),
        STEP_ACCEPTED(R.drawable.ico_progress_1),
        STEP_STARTED(R.drawable.ico_progress_2),
        STEP_COMPLETED(R.drawable.ico_progress_3);

        public final int imageResourceId;

        Step(int imageResourceId) {
            this.imageResourceId = imageResourceId;
        }
    }

    @InjectView(R.id.reservationProgressOne) ImageView mCurrentImage;
    @InjectView(R.id.reservationProgressTwo) ImageView mNextImage;
    @InjectView(R.id.reservationProgressHeaderStepOneText) TextView mStepOneText;
    @InjectView(R.id.reservationProgressHeaderStepTwoText) TextView mStepTwoText;
    @InjectView(R.id.reservationProgressHeaderStepThreeText) TextView mStepThreeText;

    private Step mCurrentStep = Step.NO_STEP;

    @SuppressWarnings("UnusedDeclaration")
    public ReservationProgressBar(Context context) {
        this(context, null);
    }

    public ReservationProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ReservationProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.reservation_details_progressbar, this, true);
        ButterKnife.inject(this, view);
        setCurrentStep(Step.STEP_ACCEPTED);
    }

    public void setCurrentStep(Step step) {
        if (step == mCurrentStep) {
            return;
        }
        mCurrentStep = step;
        if (mCurrentStep == Step.NO_STEP) {
            setVisibility(GONE);
            return;
        }
        setVisibility(View.VISIBLE);
        mNextImage.setImageResource(mCurrentStep.imageResourceId);
        animateToNextStep(mCurrentImage, mNextImage);
        switchCurrentWithNextImage();
        updateTextColors(step);
    }

    @SuppressWarnings("UnusedDeclaration")
    public Step getCurrentStep() {
        return mCurrentStep;
    }

    private void updateTextColors(Step step) {
        if (step == Step.STEP_ACCEPTED) {
            mStepTwoText.setTextAppearance(getContext(), R.style.MyDriver_TextAppearance_MicroGrey);
            mStepThreeText.setTextAppearance(getContext(), R.style.MyDriver_TextAppearance_MicroGrey);
            return;
        }
        if (step == Step.STEP_STARTED) {
            mStepTwoText.setTextAppearance(getContext(), R.style.MyDriver_TextAppearance_MicroGreen);
            mStepThreeText.setTextAppearance(getContext(), R.style.MyDriver_TextAppearance_MicroGrey);
            return;
        }
        if (step == Step.STEP_COMPLETED) {
            mStepTwoText.setTextAppearance(getContext(), R.style.MyDriver_TextAppearance_MicroGreen);
            mStepThreeText.setTextAppearance(getContext(), R.style.MyDriver_TextAppearance_MicroGreen);
            return;
        }
        throw new IllegalStateException("Step is illegal: " + step);
    }

    private void animateToNextStep(ImageView fromImageView, ImageView nextImageView) {
        nextImageView.setVisibility(View.VISIBLE);
        ObjectAnimator animation1 = ObjectAnimator.ofFloat(nextImageView, "alpha", 1f);
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(fromImageView, "alpha", 0f);
        AnimatorSet animators = new AnimatorSet();
        animators.setDuration(ANIMATION_DURATION_IN_MILLISECONDS);
        animators.addListener(new Animator.AnimatorListener() {

            @Override public void onAnimationStart(Animator animation) {}
            @Override public void onAnimationRepeat(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {
                mNextImage.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mNextImage.setVisibility(View.GONE);
            }
        });
        animators.playTogether(animation1, animation2);
        animators.start();
    }

    private void switchCurrentWithNextImage() {
        ImageView dummyImage = mCurrentImage;
        mCurrentImage = mNextImage;
        mNextImage = dummyImage;
    }
}