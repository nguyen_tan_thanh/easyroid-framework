package com.mydriver.driver.view.driverprofile;


import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.ShowDriverEditPasswordEvent;
import com.mydriver.driver.event.actionshow.ShowDriverEditPhoneNumberEvent;
import com.mydriver.driver.view.base.GenericListItem;
import com.sixt.mydriver.model.SxMdAssignedArea;
import com.sixt.mydriver.model.SxMdDriver;

import java.util.ArrayList;
import java.util.List;

class DriverDetailsListItemFactory {

    private DriverDetailsListItemFactory() {}

    public static GenericListItem newDriverNameItem(SxMdDriver driver) {
        String fullName = driver.getFirstName() +" "+ driver.getLastName();
        GenericListItem item = new GenericListItem(fullName, R.drawable.ico_driver_black);
        item.setSubTitle(driver.getEmail());
        return item;
    }

    public static GenericListItem newPhoneNumberItem(SxMdDriver driver) {
        GenericListItem item = new GenericListItem(driver.getMobile(), R.drawable.ico_phone_black);
        item.setActionIconResourceId(R.drawable.ico_edit_pencil_yellow);
        item.setActionEvent(new ShowDriverEditPhoneNumberEvent(driver));
        return item;
    }

    public static GenericListItem newPasswordItem(SxMdDriver driver) {
        GenericListItem item = new GenericListItem("• • • • • • • •", R.drawable.ico_password_black);
        item.setSubTitle(MainApplication.getStringResource(R.string.driver_profile_password));
        item.setActionIconResourceId(R.drawable.ico_edit_pencil_yellow);
        item.setActionEvent(new ShowDriverEditPasswordEvent(driver));
        return item;
    }

    public static List<GenericListItem> newAssignedAreaItemList(SxMdDriver driver) {
        List<GenericListItem> items = new ArrayList<>();
        for (SxMdAssignedArea area : driver.getCompany().getAssignedAreas()) {
            items.add(new GenericListItem(area.getName(), R.drawable.ico_mydriver_city_black));
        }
        return items;
    }
}