package com.mydriver.driver.controller;

import com.mydriver.driver.event.actionshow.ShowLoginEvent;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;

public class DriverDoLogoutPluginController extends SxPluginController {

    private final DriverPreferences driverPreferences;

    public DriverDoLogoutPluginController(DriverPreferences driverPreferences) {
        this.driverPreferences = driverPreferences;
    }

    public void onEvent(DriverDoLogoutEvent event) {
        driverPreferences.clearAuthToken();
        removeStickyEvent(ReservationsRequestedUpdatedEvent.class);
        removeStickyEvent(ReservationsAcceptedUpdatedEvent.class);
        removeStickyEvent(ReservationsCompletedUpdatedEvent.class);
        removeStickyEvent(DriverUpdatedEvent.class);
        post(new PushNotificationActivationDoChangeEvent(false));
        post(new ShowLoginEvent());
    }
}
