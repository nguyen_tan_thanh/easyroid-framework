package com.sixt.core.util;

/**
 * Class containing all the constants being used within the app.
 * Copyright (c) 2013 Sixt. All rights reserved.
 *
 * @author Enrique Lopez Manas <enrique.lopez-manas@sixt.com>
 */

public class SxConst {

    public enum Day {
        SxReservationStatusBooked,
        SxReservationStatusCancellable,
        SxReservationStatusActiv,
    }

    public static final String X_API_MOBILE = "hfh1ukf765iutqed4mvilmbzfexdywak";

    public static final String SIXT_HOST = "https://app.sixt.de";

    public static final String SIXT_MOBILEWS_API_VERSION_1 = "1";

    public static final String SIXT_MOBILEWS_API_VERSION_2 = "2";

    public static final String SIXT_MOBILEWS_API_VERSION = SIXT_MOBILEWS_API_VERSION_1;

    public static final String ED_HOST = "https://www.mydriver.com";

    public static final String ED_TEST_HOST = "https://partner.sixt.de";

    public static final String DN_HOST = "https://www.drive-now.com";

    public static final String ET_HOST = "https://webservices.sixt.com";

    public static final String RIJNDAL_SESSION_KEY = "Vst4vay_Strana-O";

    public static final String RIJNDAL_IV = "1234567890123456";

    public static final String PREFS_NAME = "PrefsFile";

    public static final String GOOGLE_ANALYTICS_PREF_NAME = "gaActivated";


    //// New versions vor WS
    public static final String SX_REQUEST_LOGIN = "/php/profilews/v2.login";

    public static final String SX_REQUEST_OFFER_DETAILS = "/php/reservationws/v1.json.offer.details";

    public static final String SX_REQUEST_OFFER_LIST = "/php/reservationws/v2.json.offer.list";

    public static final String SX_REQUEST_STATION_DETAILS = "/php/branchws/v1.json.stationdetails";

    public static final String SX_REQUEST_STATION_SUGGEST = "/php/branchws/v2.json.stationsuggest";

    public static final String SX_REQUEST_OFFER_PRICE_CALCULATION = "/php/reservationws/v1.json.offer.calculation.price";

    public static final String SX_REQUEST_OFFER_CALCULATION = "/php/reservationws/v1.json.offer.calculation.offer";

    public static final String SX_REQUEST_STATIC_LIST = "/php/reservationws/v2.json.staticlists";

    public static final String SX_REQUEST_DRIVER_DATA = "/php/reservationws/v1.json.driverdata";

    public static final String SX_REQUEST_PASSWORD_RECOVERY = "/php/profilews/v1.json.password_recovery";

    public static final String SX_REQUEST_VEHICLE_IMAGE = "/php/reservationws/v1.json.vehicleimage";

    public static final String SX_REQUEST_BOOKING = "/php/reservationws/v1.json.offer.booking";

    public static final String SX_REQUEST_RENTAL_INFORMATION = "/php/reservationws/v1.json.rental.informations";

    public static final String SX_REQUEST_ALL_RENTAL_ACTIVITIES = "/php/profilews/v2.all_rental_activities";

    public static final String SX_REQUEST_SINGLE_RENTAL_ACTIVITY = "/php/profilews/v2.single_rental_activity";

    public static final String SX_REQUEST_RESERVATION_CANCEL = "/php/profilews/v1.json.reservation_cancel";

    public static final String SX_REQUEST_WALLET = "/php/mobilews/wallet.reservation.create";

    public static final String SX_REQUEST_RENTAL_CONTRACT = "/php/profilews/v2.json.rental_scan";

    /* BEGIN VEHICLE SELECTION */

    public static final String SX_REQUEST_VEHICLE_SELECTION_VEHICLES = "/php/reservationws/v1.json.vehicleselection.vehicles";

    public static final String SX_REQUEST_VEHICLE_SELECTION_EXTRAS = "/php/reservationws/v1.json.vehicleselection.extras";

    public static final String SX_REQUEST_VEHICLE_SELECTION_PRICE = "/php/reservationws/v1.json.vehicleselection.price";

    public static final String SX_REQUEST_VEHICLE_SELECTION_SELECTVEHICLE = "/php/reservationws/v1.json.vehicleselection.selectvehicle";

    /* END VEHICLE SELECTION */

    /*BEGIN  SPECIAL OFFERS */
    public static final String SX_REQUEST_SPECIAL_OFFER_LIST = "/php/reservationws/v1.json.specialoffer.list";
    /*END  SPECIAL OFFERS */

    /* BEGIN DAMAGE */

    public static final String SX_REQUEST_PICTORGRAM = "/php/mobilews/img.damage.get_pictogram";

    public static final String SX_REQUEST_DAMAGE_LIST = "/php/mobilews/json.damage.get_list";

    public static final String SX_REQUEST_DAMAGE_PHOTO_REQUEST = "/php/mobilews/img.damage.get_photo";

    /* END DAMAGE */

    public static final String SX_REQUEST_VOUCHER_VALIDATION = "/php/reservationws/v1.json.offer.voucher";


    public static final String REQUEST_OFFERCONFIG = "/php/reservationws/json.offerconfig";

    public static final String REQUEST_CALCULATE = "/php/reservationws/json.calculate";

    public static final String REQUEST_LOGIN = "/php/customerservicews/json.login";

    public static final String REQUEST_STATION_SEARCH = "/php/branchws/json.search";

    public static final String REQUEST_BOOKING = "/php/reservationws/json.booking";

    public static final String REQUEST_COUNTRIES = "/php/branchws/json.countries";

    public static final String REQUEST_CUSTOMER = "/php/reservationws/json.customer";

    public static final String REQUEST_OVERVIEW = "/php/customerservicews/json.overview";

    public static final String REQUEST_SINGLERESERVATION = "/php/customerservicews/json.singlereservation";

    public static final String REQUEST_VEHICLEIMG = "/php/reservationws/vehicleimage";

    public static final String REQUEST_RESERVATIONCANCEL = "/php/customerservicews/json.reservationcancel";

    public static final String EDRIVER_POSITION_UPDATE = "/php/edriverws/json.driver.update";

    public static final float WALKING_SPEED = 4.8f;

    public static final float ALARM_DISTANCE = 500;

    public static final int DEFAULT_RENTAL_DURATION = 2;

    public static final String DEFAULT_DATEFORMAT = "yyyyMMdd kkmm";

}