package com.mydriver.driver.view.reservationmap.presenter;


import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mydriver.driver.R;
import com.mydriver.driver.view.reservationmap.ReservationMapView;
import com.sixt.core.eventbus.presenter.SimpleChildPresenter;
import com.sixt.mydriver.model.SxMdReservation;

class MapOriginDestinationMarkerPresenter extends SimpleChildPresenter<ReservationMapView> {

    private final Reservation reservation;
    private Marker originMarker;
    private Marker destinationMarker;

    public MapOriginDestinationMarkerPresenter(Reservation reservation) {
        this.reservation = reservation;
    }

    @Override
    public void start(ReservationMapView reservationMapView) {
        super.start(reservationMapView);
        drawReservationLocationMarkers();
    }

    private void drawReservationLocationMarkers() {
        cleanMarkers();
        originMarker = view.addMarker(getOriginMarkerOptions());
        if (reservation.getType() == SxMdReservation.Type.DISTANCE) {
            destinationMarker = view.addMarker(getDestinationMarkerOptions());
        }
    }

    private void cleanMarkers() {
        if (originMarker != null) {
            originMarker.remove();
            originMarker = null;
        }
        if (destinationMarker != null) {
            destinationMarker.remove();
            destinationMarker = null;
        }
    }

    private MarkerOptions getOriginMarkerOptions() {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(reservation.getOrigin().getLatitude(), reservation.getOrigin().getLongitude()));
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ico_map_marker_start_a));
        return markerOptions;
    }

    private MarkerOptions getDestinationMarkerOptions() {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(reservation.getDestination().getLatitude(), reservation.getDestination().getLongitude()));
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ico_map_marker_destination_b));
        return markerOptions;
    }
}
