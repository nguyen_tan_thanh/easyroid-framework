package com.sixt.core.util;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;

public class SxCurrencyUtils {

    private SxCurrencyUtils() {}

    private static final HashMap<String, NumberFormat> cache = new HashMap<>();

    private static String createCacheKey(String currencyCode) {
        String countryCode = Locale.getDefault().getCountry();
        return countryCode + "_" + currencyCode;
    }

    public static NumberFormat getLocalizedPriceFormatter(String currencyCode) {
        String cacheKey = createCacheKey(currencyCode);
        NumberFormat numberFormat = cache.get(cacheKey);
        if (numberFormat == null) {
            numberFormat = getNumberFormat(currencyCode);
            cache.put(cacheKey, numberFormat);
        }
        return numberFormat;
    }

    private static NumberFormat getNumberFormat(String currencyCode) {
        NumberFormat numberFormat = DecimalFormat.getCurrencyInstance();
        try {
            Currency currency = Currency.getInstance(currencyCode);
            numberFormat.setCurrency(currency);
        } catch (IllegalArgumentException e) {
            DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) numberFormat).getDecimalFormatSymbols();
            decimalFormatSymbols.setCurrencySymbol(currencyCode);
            ((DecimalFormat) numberFormat).setDecimalFormatSymbols(decimalFormatSymbols);
        }
        return numberFormat;
    }

    public static String getLocalizedPriceFormattedString(float amount, String currencyCode) {
        NumberFormat numberFormat = getLocalizedPriceFormatter(currencyCode);
        return numberFormat.format(amount);
    }
}
