/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <string.h>
#include <jni.h>

#define X_API_MOBILE "hfh1ukf765iutqed4mvilmbzfexdywak"
#define AES_MYDRIVER_ENCRYPTION "d4mvilmbzfexdywakhfh1ukf765iutqe"
#define AES_SIXT_ENCRYPTION "90sad0askd009asöalftferghm388fexq"

jstring
Java_com_sixt_rac_racV2_service_SxSiBackgroundService_getXAPIMOBILEFromJNI2( JNIEnv* env, jobject thiz)
{
    return (*env)->NewStringUTF(env, X_API_MOBILE);
}

jstring
Java_com_sixt_common_manager_SxBaseManager_getXAPIMOBILEFromJNI( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, X_API_MOBILE);
}

jstring
Java_com_sixt_mydriver_service_SxMdBackGroundService_getMyDriverEncryptionKeyFromJNI( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, AES_MYDRIVER_ENCRYPTION);
}


jstring
Java_com_sixt_common_webservice_SxJniConnection_getSixtSecretKey( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, AES_SIXT_ENCRYPTION);
}

jstring
Java_com_sixt_common_manager_SxBaseManager_getDevelopmentCookieValue( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, "zE6AS6");
}

jstring
Java_com_sixt_common_manager_SxBaseManager_getTestCookieValue( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, "We88epru");
}

jstring
Java_com_sixt_common_manager_SxBaseManager_getStagingCookieValue( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, "jui76R");
}

jstring
Java_com_sixt_common_manager_SxBaseManager_getEturnProdClusterCookieValue( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, "sw3hAT");
}

jstring
Java_com_sixt_common_manager_SxBaseManager_getEturnTrainingCookieValue( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, "s4eCrecr");
}

jstring
Java_com_sixt_common_manager_SxBaseManager_getEdriverProductionCookieValue( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, "cAdEsw6k");
}

jstring
Java_com_sixt_common_webservice_SxJniConnection_getXAPIMOBILEFromJNI( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, X_API_MOBILE);
}

jstring
Java_com_sixt_common_webservice_SxJniConnection_getDevelopmentCookieValue( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, "zE6AS6");
}

jstring
Java_com_sixt_common_webservice_SxJniConnection_getTestCookieValue( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, "We88epru");
}

jstring
Java_com_sixt_common_webservice_SxJniConnection_getStagingCookieValue( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, "jui76R");
}

jstring
Java_com_sixt_common_webservice_SxJniConnection_getEturnProdClusterCookieValue( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, "sw3hAT");
}

jstring
Java_com_sixt_common_webservice_SxJniConnection_getEturnTrainingCookieValue( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, "s4eCrecr");
}

jstring
Java_com_sixt_common_webservice_SxJniConnection_getEdriverProductionCookieValue( JNIEnv* env,
                                                  jobject thiz )
{
    return (*env)->NewStringUTF(env, "cAdEsw6k");
}