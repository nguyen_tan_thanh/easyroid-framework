package com.sixt.core.google.maps.routes.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class GMapsRouteList {

    @SerializedName("routes")
    private List<GMapsRoute> items;

    private String status;
}
