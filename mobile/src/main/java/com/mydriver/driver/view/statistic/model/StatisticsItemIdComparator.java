package com.mydriver.driver.view.statistic.model;


import java.util.Comparator;

public class StatisticsItemIdComparator implements Comparator<StatisticsOverviewItemIdModel> {

    @Override
    public int compare(StatisticsOverviewItemIdModel lhs, StatisticsOverviewItemIdModel rhs) {
        if (lhs.getYear() < rhs.getYear()) {
            return 1;
        }
        if (lhs.getYear() > rhs.getYear()) {
            return -1;
        }
        if (lhs.getMonth() < rhs.getMonth()) {
            return 1;
        }
        if (lhs.getMonth() > rhs.getMonth()) {
            return -1;
        }
        return 0;
    }
}
