package com.mydriver.driver.view.reservationlist.tabs;

import com.sixt.core.eventbus.SxEventBus;

class ReservationsRequestedPresenter extends ReservationListPresenter {

    public void onItemSelected(int position) {
        super.onItemSelected(position);
        Reservation reservation = mReservations.get(position).getReservation();
        if (!reservation.isSeen()) {
            reservation.setSeen(true);
            ReservationPreferences.getInstance().addSeenReservation(reservation);
        }
    }

    public void onEvent(ReservationsRequestedUpdatedEvent event) {
        updateReservations(event.getReservations());
    }

    @Override
    public void refreshReservations() {
        SxEventBus.postEvent(new ReservationsRequestedDoUpdateEvent());
    }
}
