package com.mydriver.driver.view.reservationlist.tabs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

public class ReservationListAdapter extends ArrayAdapter<ReservationListItemModel> implements StickyListHeadersAdapter {

    private final List<String> mSections = new ArrayList<>();

    public ReservationListAdapter(Context context) {
        super(context, R.layout.reservation_list_item);
    }

    @Override
    public View getHeaderView(int position, View view, ViewGroup viewGroup) {
        View headerView = view;
        ReservationTitleViewHolder titleViewHolder;
        if (headerView == null) {
            headerView = LayoutInflater.from(getContext()).inflate(R.layout.sticky_header_list_title, viewGroup, false);
            titleViewHolder = new ReservationTitleViewHolder(headerView);
            headerView.setTag(titleViewHolder);
        } else {
            titleViewHolder = (ReservationTitleViewHolder) headerView.getTag();
        }

        titleViewHolder.titleTime.setText(getItem(position).getHeaderText().toUpperCase(MainApplication.getUserLocale()));
        return headerView;
    }

    @Override
    public long getHeaderId(int position) {
        ReservationListItemModel item = getItem(position);
        return mSections.indexOf(item.getHeaderText());
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ReservationViewHolder reservationViewHolder;

        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.reservation_list_item, parent, false);
            reservationViewHolder = new ReservationViewHolder(view);
            view.setTag(reservationViewHolder);
        } else {
            reservationViewHolder = (ReservationViewHolder) view.getTag();
        }

        ReservationListItemModel model = getItem(position);
        Reservation reservation = model.getReservation();
        reservationViewHolder.textViewDate.setText(DateUtils.getDisplayDate(reservation.getPlannedStartDate()));
        reservationViewHolder.textViewTime.setText(DateUtils.getDisplayTime(reservation.getPlannedStartDate()));
        reservationViewHolder.textViewClass.setText(reservation.getOffer().getVehicleType().getName().getResourceTextId());
        reservationViewHolder.textViewPrice.setText(model.getDisplayedPrice());
        reservationViewHolder.textViewPickup.setText(reservation.getOrigin().getName());

        if (reservation.isRequested() || reservation.isCompleted()) {
            reservationViewHolder.textViewCustomer.setVisibility(View.GONE);
        } else {
            String fullName = reservation.getTravellingCustomer().getFirstName() +" "+ reservation.getTravellingCustomer().getLastName();
            reservationViewHolder.textViewCustomer.setText(fullName);
            reservationViewHolder.textViewCustomer.setVisibility(View.VISIBLE);
        }

        if (reservation.getDestination() != null) {
            reservationViewHolder.textViewDropOff.setVisibility(View.VISIBLE);
            reservationViewHolder.textViewDropOff.setText(reservation.getDestination().getName());
            reservationViewHolder.textViewDuration.setVisibility(View.GONE);
        } else {
            reservationViewHolder.textViewDropOff.setVisibility(View.GONE);
            reservationViewHolder.textViewDuration.setVisibility(View.VISIBLE);
            reservationViewHolder.textViewDuration.setText(getContext().getString(R.string.reservation_duration_hours, reservation.getDurationInHours()));
        }

        if (reservation.getItem().isRequested() && !reservation.isSeen()) {
            reservationViewHolder.textViewNew.setVisibility(View.VISIBLE);
        } else {
            reservationViewHolder.textViewNew.setVisibility(View.GONE);
        }

        return view;
    }

    public void updateReservationList(List<ReservationListItemModel> reservationListItems) {
        mSections.clear();
        mSections.addAll(createSections(reservationListItems));
        clear();
        addAll(reservationListItems);
    }

    private Set<String> createSections(List<ReservationListItemModel> reservations) {
        Set<String> sections = new HashSet<>();
        for(ReservationListItemModel item : reservations) {
            sections.add(item.getHeaderText());
        }
        return sections;
    }

    class ReservationViewHolder {
        @InjectView(R.id.textViewReservationDate) TextView textViewDate;
        @InjectView(R.id.textViewReservationTime) TextView textViewTime;
        @InjectView(R.id.textViewReservationNew) TextView textViewNew;
        @InjectView(R.id.textViewReservationPickup) TextView textViewPickup;
        @InjectView(R.id.textViewReservationDropOff) TextView textViewDropOff;
        @InjectView(R.id.textViewReservationCustomer) TextView textViewCustomer;
        @InjectView(R.id.textViewReservationClass) TextView textViewClass;
        @InjectView(R.id.textViewReservationPrice) TextView textViewPrice;
        @InjectView(R.id.textViewReservationDuration) TextView textViewDuration;

        public ReservationViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    static class ReservationTitleViewHolder {
        @InjectView(R.id.stickyHeaderListTitleTextView) TextView titleTime;

        public ReservationTitleViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}