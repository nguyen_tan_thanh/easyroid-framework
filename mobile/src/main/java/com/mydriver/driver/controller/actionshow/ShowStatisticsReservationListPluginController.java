package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.controller.base.AnimatingSingleFragmentPluginController;
import com.mydriver.driver.event.actionshow.ShowStatisticsReservationListEvent;
import com.mydriver.driver.view.statistic.list.StatisticsReservationListFragment;

public class ShowStatisticsReservationListPluginController extends AnimatingSingleFragmentPluginController<ShowStatisticsReservationListEvent, StatisticsReservationListFragment> {

    private String title = "";

    public ShowStatisticsReservationListPluginController() {
        super(StatisticsReservationListFragment.class);
    }

    public void onEvent(ShowStatisticsReservationListEvent event) {
        title = event.getStatisticsItem().getMonth() +" "+ event.getStatisticsItem().getId().getYear();
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldDisplayFragment(ShowStatisticsReservationListEvent event) {
        return true;
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return true;
    }

    @Override
    protected StatisticsReservationListFragment createFragment(ShowStatisticsReservationListEvent event) {
        return StatisticsReservationListFragment.newInstance(event.getStatisticsItem());
    }

    @Override
    protected String createActionBarTitle() {
        return title;
    }
}
