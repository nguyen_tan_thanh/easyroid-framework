package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.controller.base.AnimatingSingleFragmentPluginController;
import com.mydriver.driver.event.actionshow.ShowReservationListsEvent;
import com.mydriver.driver.view.reservationlist.ReservationListTabFragment;

public class ShowReservationListPluginController extends AnimatingSingleFragmentPluginController<ShowReservationListsEvent, ReservationListTabFragment> {

    public ShowReservationListPluginController() {
        super(ReservationListTabFragment.class);
    }

    public void onEvent(ShowReservationListsEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldDisplayFragment(ShowReservationListsEvent showReservationListsEvent) {
        return true;
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return false;
    }

    @Override
    protected ReservationListTabFragment createFragment(ShowReservationListsEvent showReservationListsEvent) {
        return ReservationListTabFragment.newInstance();
    }

    @Override
    protected String createActionBarTitle() {
        return MainApplication.getStringResource(R.string.reservations_title);
    }
}
