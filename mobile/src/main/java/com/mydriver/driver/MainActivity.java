package com.mydriver.driver;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.mydriver.driver.event.actionshow.ShowLoginEvent;
import com.mydriver.driver.event.actionshow.ShowReservationDetailsFromPushNotification;
import com.mydriver.driver.view.leftmenu.LeftMenuFragment;
import com.mydriver.driver.view.login.LoginActivity;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.event.TransactionFragmentEvent;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainActivity extends HockeyActivity implements MainUi {

    private static final String EXTRA_PUSH_NOTIFICATION_DATA = "extra_push_notification_data";

    @InjectView(R.id.activityMainDrawerLayout) DrawerLayout mDrawerLayout;
    @InjectView(R.id.activityMainContentContainer) FrameLayout mContentFrame;
    @InjectView(R.id.activityMainLeftMenuContainer) FrameLayout mMenuLeftDrawerFrame;
    @InjectView(R.id.activityMainToolbar) Toolbar mToolbar;
    @InjectView(R.id.activityMainProgressSpinner) ProgressBar mProgressBar;

    private MainActivityDrawerPresenter mDrawerPresenter;

    public static Intent newIntent(Bundle extras) {
        Intent intent = new Intent(MainApplication.getInstance(), MainActivity.class);
        if (extras != null) {
            intent.putExtras(extras);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    public static Intent newIntent(PushNotificationData pushNotificationData) {
        Bundle extras = new Bundle();
        extras.putSerializable(EXTRA_PUSH_NOTIFICATION_DATA, pushNotificationData);
        return newIntent(extras);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        setSupportActionBar(mToolbar);
        mDrawerPresenter = new MainActivityDrawerPresenter();
        mDrawerPresenter.onCreate(this, mToolbar, mDrawerLayout);
        handleLoginState();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerPresenter.onPostCreate();
        if (savedInstanceState == null) {
            handleIntentExtras(getIntent());
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntentExtras(intent);
    }

    private void handleIntentExtras(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras == null || extras.isEmpty()) {
            return;
        }
        PushNotificationData pushNotificationData = (PushNotificationData) extras.getSerializable(EXTRA_PUSH_NOTIFICATION_DATA);
        if (pushNotificationData == null) {
            return;
        }
        SxEventBus.postEvent(new ShowReservationDetailsFromPushNotification(pushNotificationData.getReservationId()));
    }

    private void handleLoginState() {
        DriverUpdatedEvent driverUpdatedEvent = SxEventBus.getSticky(DriverUpdatedEvent.class);
        if (driverUpdatedEvent == null || driverUpdatedEvent.getDriver() == null) {
            Intent intent = LoginActivity.newIntent(getIntent().getExtras());
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        SxEventBus.postEvent(new FeedbackMessageDoRegisterActivityEvent(this));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerPresenter.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerPresenter.isDrawerIndicatorEnabled()) {
                    return mDrawerPresenter.onOptionsItemSelected(item);
                }
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void updateActionBar() {
        if (isBackStackEmpty()) {
            mDrawerPresenter.showDrawerIndicator();
        } else {
            mDrawerPresenter.showBackIndicator(this);
        }
        super.updateActionBar();
    }

    @Override
    protected int getContainerId(TransactionFragmentEvent event) {
        Class<? extends Fragment> fragmentClass = event.getFragmentClass();
        if (fragmentClass.equals(LeftMenuFragment.class)) {
            return R.id.activityMainLeftMenuContainer;
        } else {
            return R.id.activityMainContentContainer;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDrawerPresenter.stopEventListening();
    }

    @Override
    public void onBackPressed() {
        if (isBackStackEmpty()) {
            startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));
            return;
        }
        super.onBackPressed();
    }

    public void onEvent(ShowLoginEvent event) {
        finish();
    }
}