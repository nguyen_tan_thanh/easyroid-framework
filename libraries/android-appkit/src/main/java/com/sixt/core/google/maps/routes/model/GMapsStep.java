package com.sixt.core.google.maps.routes.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class GMapsStep {

    private TextValuePair<String, Integer> distance;

    private TextValuePair<String, Integer> duration;

    private GMapsEncodedPolyline polyline;

    @SerializedName("travel_mode")
    private GMapsRoute.Mode travelMode;

    @SerializedName("start_location")
    private LatLng startLocation;

    @SerializedName("end_location")
    private LatLng endLocation;
}
