package com.mydriver.driver.view.driverprofile;


import com.mydriver.driver.view.base.GenericListItem;

import java.util.List;

interface DriverProfileView {
    void drawDriverDetails(List<GenericListItem> driverDetails);
    void drawAssignedAreas(List<GenericListItem> assignedAreas);
    void drawDriverRating(float averageRating);
}
