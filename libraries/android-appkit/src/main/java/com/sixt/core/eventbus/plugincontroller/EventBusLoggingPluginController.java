package com.sixt.core.eventbus.plugincontroller;

import android.util.Log;

public class EventBusLoggingPluginController extends SxPluginController {

    private static final String TAG = "EventBus";

    public void onEvent(Object event) {
        Log.d(TAG, event.toString());
    }
}
