package com.sixt.core.util;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class SxMdDateUtils {

    private SxMdDateUtils() {}

    private static final DateFormat dateAndTimeFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
    private static final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
    private static final DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT);

    public static String getDisplayCurrentDateAndTime() {
        return dateAndTimeFormat.format(Calendar.getInstance().getTime());
    }

    public static String getDisplayDateAndTime(Calendar date) {
        return dateAndTimeFormat.format(date.getTime());
    }

    public static String getDisplayDate(Calendar date) {
        return dateFormat.format(date.getTime());
    }

    public static String getDisplayTime(Calendar date) {
        return timeFormat.format(date.getTime());
    }

    public static String getDisplayDayOfWeek(Calendar date) {
        return date.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
    }

    public static boolean isToday(Calendar date) {
        Calendar now = new GregorianCalendar();
        return now.get(Calendar.DAY_OF_YEAR) == date.get(Calendar.DAY_OF_YEAR) && now.get(Calendar.YEAR) == date.get(Calendar.YEAR);
    }
}
