package com.mydriver.driver.view.reservationlist.tabs;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.sixt.core.util.SxCurrencyUtils;
import com.sixt.mydriver.model.SxMdOffer;
import com.sixt.mydriver.model.SxMdReservation;

import java.util.Calendar;

import lombok.Data;

@Data
public class ReservationListItemModel {

    public static final String HEADER_STARTED = MainApplication.getStringResource(R.string.reservation_started);
    public static final String HEADER_TODAY = MainApplication.getStringResource(R.string.reservation_today);
    public static final String HEADER_LATER_THIS_WEEK = MainApplication.getStringResource(R.string.reservation_later_this_week);

    private final String headerText;

    private final Reservation reservation;
    private final String displayedPrice;

    public ReservationListItemModel(Reservation reservation) {
        this.reservation = reservation;
        this.headerText = createHeaderText(reservation);
        this.displayedPrice = createDisplayedPrice(reservation);
    }

    private String createHeaderText(Reservation reservation) {
        Calendar plannedStartDate = reservation.getPlannedStartDate();
        if (reservation.getStatus() == SxMdReservation.Status.STARTED) {
            return HEADER_STARTED;
        }
        if (isToday(plannedStartDate)) {
            return HEADER_TODAY;
        }
        if (isThisWeek(plannedStartDate)) {
            return HEADER_LATER_THIS_WEEK;
        }
        return getMonthAndYear(plannedStartDate);
    }

    private boolean isToday(Calendar date) {
        Calendar today = Calendar.getInstance();
        if (today.get(Calendar.DAY_OF_YEAR) != date.get(Calendar.DAY_OF_YEAR)) {
            return false;
        }
        return today.get(Calendar.YEAR) == date.get(Calendar.YEAR);
    }

    private boolean isThisWeek(Calendar date) {
        Calendar today = Calendar.getInstance();
        if (today.get(Calendar.WEEK_OF_YEAR) != date.get(Calendar.WEEK_OF_YEAR)) {
            return false;
        }
        return today.get(Calendar.YEAR) == date.get(Calendar.YEAR);
    }

    private String getMonthAndYear(Calendar date) {
        String month = date.getDisplayName(Calendar.MONTH, Calendar.LONG, MainApplication.getUserLocale());
        int year = date.get(Calendar.YEAR);
        return month +" "+ year;
    }

    private String createDisplayedPrice(Reservation reservation) {
        return SxCurrencyUtils.getLocalizedPriceFormattedString(reservation.getFloatingPointDriverRideValueNet(), reservation.getOffer().getCurrency());
    }
}