package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.event.actionshow.ShowSimpleAlertDialogEvent;
import com.mydriver.driver.view.alert.SimpleAlertDialogFragment;
import com.sixt.core.eventbus.plugincontroller.SxSingleFragmentPluginController;

public class ShowSimpleAlertDialogPluginController extends SxSingleFragmentPluginController<ShowSimpleAlertDialogEvent, SimpleAlertDialogFragment> {

    public ShowSimpleAlertDialogPluginController() {
        super(SimpleAlertDialogFragment.class);
    }

    public void onEvent(ShowSimpleAlertDialogEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return false;
    }

    @Override
    protected SimpleAlertDialogFragment createFragment(ShowSimpleAlertDialogEvent event) {
        return SimpleAlertDialogFragment.newInstance(event.getTitle(), event.getMessage());
    }
}