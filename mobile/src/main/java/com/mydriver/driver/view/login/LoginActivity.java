package com.mydriver.driver.view.login;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.mydriver.driver.MainActivity;
import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.sixt.core.util.KeyboardUtils;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.ui.ProgressButton;
import com.sixt.mydriver.SxMdConstants;

import org.apache.commons.lang3.StringUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class LoginActivity extends FragmentActivity implements LoginView {

    @InjectView(R.id.activityLoginUsernameEditText) EditText mUsernameEditText;
    @InjectView(R.id.activityLoginPasswordEditText) EditText mPasswordEditText;
    @InjectView(R.id.activityLoginLoginButton) ProgressButton mLoginButton;
    @InjectView(R.id.activityLoginFormContainer) ViewGroup mFormContainer;
    @InjectView(R.id.activityLoginProgressBar) ProgressBar mProgressBar;

    private int mEnvironmentSwitchClickCounter;
    private LoginPresenter mPresenter;

    public static Intent newIntent(Bundle extras) {
        Intent intent = new Intent(MainApplication.getInstance(), LoginActivity.class);
        if (extras != null) {
            intent.putExtras(extras);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.inject(this);
        mPresenter = new LoginPresenter(DriverPreferences.getInstance());
        mPresenter.start(this);
        mPresenter.setInitialEmail();
    }

    @Override
    protected void onStart() {
        super.onStart();
        SxEventBus.postEvent(new FeedbackMessageDoRegisterActivityEvent(this));
    }

    @OnClick(R.id.activityLoginLogo)
    public void onClickEnvironmentSwitch() {
        mEnvironmentSwitchClickCounter++;
        if (mEnvironmentSwitchClickCounter == 10) {
            mEnvironmentSwitchClickCounter = 0;
            showEnvironmentSwitchDialog();
        }
    }

    @OnClick(R.id.activityLoginLoginButton)
    public void onClickLoginButton() {
        String usernameErrorMessage = getValidationMessage(mUsernameEditText);
        String passwordErrorMessage = getValidationMessage(mPasswordEditText);
        mUsernameEditText.setError(usernameErrorMessage);
        mPasswordEditText.setError(passwordErrorMessage);
        if (usernameErrorMessage != null || passwordErrorMessage != null) {
            return;
        }
        mLoginButton.enableLoadingState();
        KeyboardUtils.hideSoftKeyboard(this);
        SxEventBus.postEvent(new DriverDoAuthenticateEvent(false, mUsernameEditText.getText().toString(), mPasswordEditText.getText().toString()));
    }

    @OnClick(R.id.activityLoginJoinUsTextView)
    public void onClickJoinUsButton() {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(SxMdConstants.MY_DRIVER_JOIN_US_URL));
        startActivity(i);
    }

    @OnTextChanged({R.id.activityLoginUsernameEditText, R.id.activityLoginPasswordEditText})
    public void onTextChangeUsernamePassword() {
        hideValidationErrors();
    }

    private void showEnvironmentSwitchDialog() {
        mPresenter.showEnvironmentSwitchDialog(this);
    }

    @Override
    public void showInvalidCredentialsValidationErrors() {
        String error = getString(R.string.login_credential_validation_error_message);
        mUsernameEditText.setError(error);
        mPasswordEditText.setError(error);
    }

    @Override
    public void hideValidationErrors() {
        mUsernameEditText.setError(null);
        mPasswordEditText.setError(null);
    }

    @Override
    protected void onDestroy() {
        mPresenter.stop();
        super.onDestroy();
    }

    @Override
    public void disableLoginButtonProgressState() {
        mLoginButton.disableLoadingState();
    }

    @Override
    public void showLoginForm() {
        mProgressBar.setVisibility(View.GONE);
        mFormContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void directToMainActivity() {
        MainApplication.getInstance().startActivity(MainActivity.newIntent(getIntent().getExtras()));
        finish();
    }

    @Override
    public void fillEmailLastLoggedInDriver(String email) {
        mUsernameEditText.setText(email);
    }

    @Override
    public void setLoginState() {
        mLoginButton.enableLoadingState();
    }

    private String getValidationMessage(EditText input) {
        if (StringUtils.isEmpty(input.getText().toString())) {
            return getResources().getString(R.string.login_input_empty_validation_error, input.getHint());
        }
        return null;
    }

    @OnTextChanged({R.id.activityLoginUsernameEditText, R.id.activityLoginPasswordEditText})
    public void changeButtonState() {
        if (StringUtils.isEmpty(mUsernameEditText.getText()) || StringUtils.isEmpty(mPasswordEditText.getText())) {
            mLoginButton.setEnabled(false);
        } else {
            mLoginButton.setEnabled(true);
        }
    }
}