# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/enriquelopez/android-sdks/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-optimizationpasses 5
-dontskipnonpubliclibraryclassmembers
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-repackageclasses ''

#To remove debug logs:
-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
}

#Genarate proguard related files so stacktraces and logs can be decrypted
-dump proguard/class_files.txt
-printseeds proguard/seeds.txt
-printusage proguard/unused.txt
-printmapping proguard/mapping.txt

# Butterknife
-dontwarn butterknife.internal.**
-keep class **$$ViewInjector { *; }
-keepnames class * { @butterknife.InjectView *;}

# EventBus
-keepclassmembers class ** {public void onEvent*(**);}
-keepclassmembers class ** {public void onEventMainThread*(**);}
-keepclassmembers class ** {public void onEventBackgroundThread*(**);}

##---------------Begin: proguard configuration for Gson  ----------
-keepattributes Signature
-keep class sun.misc.Unsafe { *; }
-keep class com.mydriver.driver.model.** { *; }
-keep class com.sixt.mydriver.model.** { *; }
-keep class com.sixt.mydriver.request.** { *; }
-keep class com.sixt.common.google.maps.geocode.model.** { *; }
-keep class com.sixt.common.google.maps.routes.model.** { *; }

# Explicitly preserve all serialization members. The Serializable interface
# is only a marker interface, so it wouldn't save them.
# You can comment this out if your library doesn't use serialization.
# If your code contains serializable classes that have to be backward
# compatible, please refer to the manual.
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

# SimpleFramework XML
-keep interface org.simpleframework.xml.core.Label {public *;}
-keep interface org.simpleframework.xml.core.Parameter {public *;}
-keep interface org.simpleframework.xml.core.Extractor {public *;}
-keep class * implements org.simpleframework.xml.core.Label {public *;}
-keep class * implements org.simpleframework.xml.core.Parameter {public *;}
-keep class * implements org.simpleframework.xml.core.Extractor {public *;}

# Lombok
-dontwarn lombok.core.configuration.**
-keep class lombok.core.configuration.** { *; }

# Robospice Retrofit
-dontwarn com.google.**
-dontwarn retrofit.**
-dontwarn com.octo.android.robospice.**
-dontwarn com.squareup.**
-dontwarn org.apache.commons.**
-dontwarn javax.xml.stream.**
-dontwarn rx.**
-keep class com.google.** { *; }
-keep class retrofit.** { *; }
-keep class com.octo.android.robospice.** { *; }
-keep class org.apache.commons.** { *; }
-keep class javax.xml.stream.** { *; }
-keep class com.squareup.** { *; }
-keep class rx.** { *; }

# Parse
-dontwarn com.parse.**
-dontwarn com.facebook.**
-keep class com.parse.** { *; }
-keep class com.facebook.** { *; }

# JodaTime
-dontwarn org.joda.convert.**
