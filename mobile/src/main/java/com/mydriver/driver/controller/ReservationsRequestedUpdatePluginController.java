package com.mydriver.driver.controller;


import com.mydriver.driver.MainApplication;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdReservationManager;
import com.sixt.mydriver.model.SxMdReservation;
import com.sixt.mydriver.model.SxMdReservationList;

import java.util.Collections;
import java.util.List;

public class ReservationsRequestedUpdatePluginController extends SxPluginController {

    private final SxMdReservationManager reservationManager;
    private final ReservationPreferences mReservationPreferences = ReservationPreferences.getInstance();
    private boolean isRequestRunning;

    private SxMdReservationManager.OnReservationsListReceivedListener reservationsLoadedListener = new SxMdReservationManager.OnReservationsListReceivedListener() {
        @Override
        public void onFailure(Throwable error) {
            onRequestFailure(error);
            isRequestRunning = false;
        }

        @Override
        public void onSuccess(SxMdReservationList result) {
            processServerSideReservations(result);
            isRequestRunning = false;
        }
    };


    public ReservationsRequestedUpdatePluginController(SxMdReservationManager reservationManager) {
        this.reservationManager = reservationManager;
    }

    public void onEvent(ReservationsRequestedDoUpdateEvent event) {
        if (isRequestRunning) {
            return;
        }
        reservationManager.getDriverRequestedReservations(MainApplication.getInstance(), reservationsLoadedListener);
    }

    public void onEvent(ReservationsRequestedRequestReturnedEvent event) {
        processServerSideReservations(event.getReservations());
    }

    private void markReservationsAsSeen(List<Reservation> reservations) {
        for (Reservation reservation : reservations) {
            reservation.setSeen(mReservationPreferences.isReservationSeen(reservation.getReservationId()));
        }
    }

    private void processServerSideReservations(List<SxMdReservation> reservations) {
        List<Reservation> requestedReservations = ModelWrapperUtils.wrapReservations(reservations);
        Collections.sort(requestedReservations, new ReservationComparator());
        markReservationsAsSeen(requestedReservations);
        postSticky(new ReservationsRequestedUpdatedEvent(requestedReservations));
    }

    private void onRequestFailure(Throwable error) {
        SxEventBus.postEvent(new NetworkErrorEvent(error));
    }
}
