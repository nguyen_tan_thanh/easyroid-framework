package com.sixt.core.eventbus.ui;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.event.ActivityLifeCycleEvent;
import com.sixt.core.eventbus.event.FragmentLifeCycleEvent;


public class SxLifeCycleEventReporter {

    private SxLifeCycleEventReporter() {}

    public static void reportOnCreate(Fragment fragment) {
        reportEvent(FragmentLifeCycleEvent.onCreate(getFragmentClass(fragment)));
    }

    public static void reportOnCreate(Activity activity) {
        reportEvent(ActivityLifeCycleEvent.onCreate(activity.getClass()));
    }

    public static void reportOnPostCreate(Activity activity) {
        reportEvent(ActivityLifeCycleEvent.onPostCreate(activity.getClass()));
    }

    public static void reportOnStart(Fragment fragment) {
        reportEvent(FragmentLifeCycleEvent.onStart(getFragmentClass(fragment)));
    }

    public static void reportOnStart(Activity activity) {
        reportEvent(ActivityLifeCycleEvent.onStart(activity.getClass()));
    }

    public static void reportOnAttach(Fragment fragment) {
        reportEvent(FragmentLifeCycleEvent.onAttach(getFragmentClass(fragment)));
    }

    public static void reportOnResume(Fragment fragment) {
        reportEvent(FragmentLifeCycleEvent.onResume(getFragmentClass(fragment)));
    }

    public static void reportOnResume(Activity activity) {
        reportEvent(ActivityLifeCycleEvent.onResume(activity.getClass()));
    }

    public static void reportOnPause(Fragment fragment) {
        reportEvent(FragmentLifeCycleEvent.onPause(getFragmentClass(fragment)));
    }

    public static void reportOnPause(Activity activity) {
        reportEvent(ActivityLifeCycleEvent.onPause(activity.getClass()));
    }

    public static void reportOnDetach(Fragment fragment) {
        reportEvent(FragmentLifeCycleEvent.onDetach(getFragmentClass(fragment)));
    }

    public static void reportOnStop(Fragment fragment) {
        reportEvent(FragmentLifeCycleEvent.onStop(getFragmentClass(fragment)));
    }

    public static void reportOnStop(Activity activity) {
        reportEvent(ActivityLifeCycleEvent.onStop(activity.getClass()));
    }

    public static void reportOnDestroy(Fragment fragment) {
        reportEvent(FragmentLifeCycleEvent.onDestroy(getFragmentClass(fragment)));
    }

    public static void reportOnDestroy(Activity activity) {
        reportEvent(ActivityLifeCycleEvent.onDestroy(activity.getClass()));
    }

    private static Class<? extends Fragment> getFragmentClass(Fragment fragment) {
        return fragment.getClass();
    }

    private static void reportEvent(Object event) {
        SxEventBus.getInstance().post(event);
    }
}
