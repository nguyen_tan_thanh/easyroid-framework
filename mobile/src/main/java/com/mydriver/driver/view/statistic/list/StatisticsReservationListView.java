package com.mydriver.driver.view.statistic.list;


import com.mydriver.driver.view.reservationlist.tabs.ReservationListItemModel;
import com.mydriver.driver.view.statistic.model.StatisticsOverviewItemModel;

import java.util.List;

interface StatisticsReservationListView {
    void updateReservationsList(List<ReservationListItemModel> reservations);
    void updateHeader(StatisticsOverviewItemModel model);
}
