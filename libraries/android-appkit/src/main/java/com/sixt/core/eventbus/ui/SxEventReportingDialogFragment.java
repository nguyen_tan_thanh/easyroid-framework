package com.sixt.core.eventbus.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class SxEventReportingDialogFragment extends DialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SxLifeCycleEventReporter.reportOnCreate(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        SxLifeCycleEventReporter.reportOnStart(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        SxLifeCycleEventReporter.reportOnAttach(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        SxLifeCycleEventReporter.reportOnResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        SxLifeCycleEventReporter.reportOnPause(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        SxLifeCycleEventReporter.reportOnDetach(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        SxLifeCycleEventReporter.reportOnStop(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SxLifeCycleEventReporter.reportOnDestroy(this);
    }
}
