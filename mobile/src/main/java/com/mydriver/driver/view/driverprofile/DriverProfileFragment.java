package com.mydriver.driver.view.driverprofile;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.mydriver.driver.R;
import com.mydriver.driver.view.base.ButterKnifeEventReportingFragment;
import com.mydriver.driver.view.base.DividerDecoration;
import com.mydriver.driver.view.base.GenericListItem;
import com.mydriver.driver.view.base.GenericListItemAdapter;
import com.mydriver.driver.view.base.WrappingRecyclerViewLayoutManager;

import java.util.List;

import butterknife.InjectView;

public class DriverProfileFragment extends ButterKnifeEventReportingFragment implements DriverProfileView {

    private DriverProfilePresenter mPresenter;

    @InjectView(R.id.fragmentDriverProfileDriverDetailsList)
    RecyclerView mDriverDetailsListView;

    @InjectView(R.id.fragmentDriverProfileRating)
    RatingBar mDriverRating;

    @InjectView(R.id.fragmentDriverProfileAssignedAreaList)
    RecyclerView mAssignedAreaListView;

    public static DriverProfileFragment newInstance() {
        return new DriverProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new DriverProfilePresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.driver_profile_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.start(this);
    }

    @Override
    public void onDestroyView() {
        mPresenter.stop();
        super.onDestroyView();
    }

    @Override
    public void drawDriverDetails(List<GenericListItem> driverDetails) {
        GenericListItemAdapter adapter = new GenericListItemAdapter();
        adapter.setItems(driverDetails);
        mDriverDetailsListView.addItemDecoration(new DividerDecoration(getResources().getDrawable(R.drawable.divider_vertical_with_inset)));
        mDriverDetailsListView.setLayoutManager(new WrappingRecyclerViewLayoutManager(getActivity()));
        mDriverDetailsListView.setAdapter(adapter);
    }

    @Override
    public void drawAssignedAreas(List<GenericListItem> assignedAreas) {
        GenericListItemAdapter adapter = new GenericListItemAdapter();
        adapter.setItems(assignedAreas);
        mAssignedAreaListView.addItemDecoration(new DividerDecoration(getResources().getDrawable(R.drawable.divider_vertical_with_inset)));
        mAssignedAreaListView.setLayoutManager(new WrappingRecyclerViewLayoutManager(getActivity()));
        mAssignedAreaListView.setAdapter(adapter);
    }

    @Override
    public void drawDriverRating(float averageRating) {
        mDriverRating.setRating(averageRating);
    }
}
