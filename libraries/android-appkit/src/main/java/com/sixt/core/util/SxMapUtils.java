package com.sixt.core.util;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import java.util.List;

public class SxMapUtils {

    public static List<LatLng> decodePolyline(String encodedPolyline) {
        return PolyUtil.decode(encodedPolyline);
    }

    public static LatLng getApproximateRouteCenter(List<LatLng> polyline) {
        if (polyline.size() < 2) {
            throw new IllegalArgumentException("Polyline must include at least two Points");
        }
        int index = polyline.size() / 2;
        if (polyline.size() % 2 == 0) {
            return getMiddleOfTwoLocations(polyline.get(index-1), polyline.get(index));
        } else {
            return polyline.get(index);
        }
    }

    private static LatLng getMiddleOfTwoLocations(LatLng location1, LatLng location2) {
        double latitude = location1.latitude + (location2.latitude - location1.latitude) / 2;
        double longitude = location1.longitude + (location2.longitude - location1.longitude) / 2;
        return new LatLng(latitude, longitude);
    }
}
