package com.mydriver.driver.view.driverprofile.editphone;

import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.presenter.EventListeningPresenter;
import com.sixt.core.util.SxValidator;

class DriverEditPhonePresenter extends EventListeningPresenter {

    private final DriverEditPhoneView view;

    DriverEditPhonePresenter(DriverEditPhoneView view) {
        this.view = view;
    }

    public void changePhone(String phoneNumber) {
        if (SxValidator.isPhoneNumberValid(phoneNumber)) {
            SxEventBus.postEvent(new DriverDoEditPhoneNumberEvent(phoneNumber));
        } else {
            view.showError();
        }
    }
}