package com.mydriver.driver.view.reservationlist;

interface ReservationListTabView {
    void updatePushNotificationStatus();
    void updateUnseenReservationCount(int unseenReservations);
}