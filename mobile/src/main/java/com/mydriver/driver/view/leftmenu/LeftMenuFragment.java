package com.mydriver.driver.view.leftmenu;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mydriver.driver.R;
import com.mydriver.driver.view.base.ButterKnifeEventReportingFragment;
import com.mydriver.driver.view.base.GenericListItem;
import com.mydriver.driver.view.base.GenericListItemAdapter;
import com.mydriver.driver.view.base.WrappingRecyclerViewLayoutManager;
import com.sixt.core.app.SxAppConst;

import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

public class LeftMenuFragment extends ButterKnifeEventReportingFragment implements LeftMenuView {

    @InjectView(R.id.leftMenuFragmentItemListView)
    RecyclerView mMenuItemListView;

    @InjectView(R.id.leftMenuFragmentLogoutButton)
    TextView mLogoutButton;

    @InjectView(R.id.leftMenuFragmentVersionText)
    TextView mTextViewVersion;

    @InjectView(R.id.leftMenuEnvironmentText)
    TextView mTextViewEnvironment;

    private LeftMenuPresenter mPresenter;
    private GenericListItemAdapter mLeftMenuAdapter;
    private int mEnvironmentSwitchClickCounter;

    public static LeftMenuFragment newInstance() {
        return new LeftMenuFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.left_menu_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLeftMenuAdapter = new GenericListItemAdapter();
        mPresenter = new LeftMenuPresenter();
        mMenuItemListView.setLayoutManager(new WrappingRecyclerViewLayoutManager(getActivity()));
        mMenuItemListView.setAdapter(mLeftMenuAdapter);
        mLeftMenuAdapter.setOnItemSelectedListener(new GenericListItemAdapter.OnItemSelectedListener() {
            @Override
            public void onItemSelected(GenericListItem item, int position) {
                mPresenter.onMenuItemSelected(item);
            }
        });
        mPresenter.start(this);
    }

    @Override
    public void onDestroyView() {
        mPresenter.stopEventListening();
        super.onDestroyView();
    }

    @Override
    public void drawMenuItems(List<GenericListItem> leftMenuItems) {
        mLeftMenuAdapter.setItems(leftMenuItems);
    }

    @Override
    public void drawVersion(String version) {
        mTextViewVersion.setText(version);
    }

    @Override
    public void updateMenuItems() {
        mLeftMenuAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateEnvironmentInformation(SxAppConst.SxMdAppEnvironment environment) {
        if (environment.equals(SxAppConst.SxMdAppEnvironment.PRODUCTION)) {
            mTextViewEnvironment.setVisibility(View.GONE);
        } else {
            mTextViewEnvironment.setVisibility(View.VISIBLE);
        }
        mTextViewEnvironment.setText(environment.toString());
    }

    @OnClick(R.id.leftMenuFragmentLogoutButton)
    public void logout(Button logoutButton) {
        mPresenter.logout();
    }

    @OnClick(R.id.leftMenuFragmentVersionText)
    public void showEnvironmentSwitcher() {
        mEnvironmentSwitchClickCounter++;
        if (mEnvironmentSwitchClickCounter == 10) {
            mEnvironmentSwitchClickCounter = 0;
            //TODO: Reenable by release if we consider it or something similar
            //mPresenter.showEnvironmentSwitchDialog(getActivity());
        }
    }
}