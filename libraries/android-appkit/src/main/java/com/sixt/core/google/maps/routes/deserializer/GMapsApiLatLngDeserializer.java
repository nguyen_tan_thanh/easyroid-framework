package com.sixt.core.google.maps.routes.deserializer;


import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class GMapsApiLatLngDeserializer implements JsonDeserializer<LatLng> {

    @Override
    public LatLng deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        double latitude = ((JsonObject) jsonElement).get("lat").getAsDouble();
        double longitude = ((JsonObject) jsonElement).get("lng").getAsDouble();
        LatLng result = new LatLng(latitude, longitude);
        return result;
    }
}
