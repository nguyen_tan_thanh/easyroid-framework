package com.mydriver.driver.view.reservationmap;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public interface ReservationMapView {
    Marker addMarker(MarkerOptions markerOptions);
    Polyline addPolyline(PolylineOptions polylineOptions);
    void moveCameraToPosition(LatLng position, float zoomLevel, boolean animate);
    void moveCameraToArea(LatLngBounds area, int padding, boolean animate);
}
