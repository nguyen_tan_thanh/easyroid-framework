package com.mydriver.driver.view.reservationdetails;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Button;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.view.base.ButterKnifeEventReportingFragment;
import com.mydriver.driver.event.actionshow.ShowDriverColleagueListEvent;
import com.mydriver.driver.view.base.DividerDecoration;
import com.mydriver.driver.view.base.GenericListItem;
import com.mydriver.driver.view.base.GenericListItemAdapter;
import com.mydriver.driver.view.base.WrappingRecyclerViewLayoutManager;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.ui.ProgressButton;

import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

public class ReservationDetailsFragment extends ButterKnifeEventReportingFragment implements ReservationDetailsView {
    private static final String EXTRA_RESERVATION = "extra_reservation";

    @InjectView(R.id.fragmentReservationDetailsWorkflowContainer)
    LinearLayout mFragmentReservationDetailsWorkflowContainer;

    @InjectView(R.id.fragmentReservationDetailsList)
    RecyclerView mReservationDetailsListView;

    @InjectView(R.id.fragmentReservationDetailsContinueButton)
    ProgressButton mActionButton;

    @InjectView(R.id.fragmentReservationDetailsProgressBar)
    ReservationProgressBar mProgressBar;

    @InjectView(R.id.fragmentReservationDetailsActionButtonContainer)
    ViewGroup mAcceptedReservationActionButtonContainer;

    @InjectView(R.id.fragmentReservationDetailsReassignDriverButton)
    Button mReassignDriverButton;

    private ReservationDetailsPresenter mPresenter;
    private GenericListItemAdapter mReservationDetailsAdapter;

    public static ReservationDetailsFragment newInstance(Reservation reservation) {
        ReservationDetailsFragment reservationsDetailFragment = new ReservationDetailsFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(EXTRA_RESERVATION, reservation);
        reservationsDetailFragment.setArguments(arguments);
        return reservationsDetailFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ReservationDetailsPresenter(this, (Reservation) getArguments().getSerializable(EXTRA_RESERVATION));
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_ride_details_map:
                mPresenter.showMap();
                break;
        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_reservation_details, menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reservation_details_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fixForSamsung();
        mReservationDetailsAdapter = new GenericListItemAdapter();
        mReservationDetailsListView.addItemDecoration(new DividerDecoration(getResources().getDrawable(R.drawable.divider_vertical_with_inset)));
        mReservationDetailsListView.setLayoutManager(new WrappingRecyclerViewLayoutManager(getActivity()));
        mReservationDetailsListView.setAdapter(mReservationDetailsAdapter);
        mPresenter.start();
    }

    @Override
    public void onDestroyView() {
        mPresenter.stop();
        super.onDestroyView();
    }

    @OnClick(R.id.fragmentReservationDetailsContinueButton)
    public void onReservationContinueButtonClick() {
        mPresenter.performReservationAction();
    }

    @OnClick(R.id.fragmentReservationDetailsCallHotlineButton)
    public void onCallHotlineButtonClick() {
        mPresenter.callHotline();
    }

    @OnClick(R.id.fragmentReservationDetailsReassignDriverButton)
    public void onReassignDriverButtonClick() {
        Reservation reservation = (Reservation) getArguments().getSerializable(EXTRA_RESERVATION);
        SxEventBus.postEvent(new ShowDriverColleagueListEvent(reservation));
    }

    @Override
    public void drawReservationDetails(List<GenericListItem> reservationDetails) {
        mReservationDetailsAdapter.setItems(reservationDetails);
    }

    @Override
    public void setProgressStep(ReservationProgressBar.Step step) {
        mProgressBar.setCurrentStep(step);
        mAcceptedReservationActionButtonContainer.setVisibility(mPresenter.shouldShowAcceptedReservationActionButtons() ? View.VISIBLE : View.GONE);
        if (step == ReservationProgressBar.Step.STEP_COMPLETED) {
            mFragmentReservationDetailsWorkflowContainer.setVisibility(View.GONE);
            mActionButton.setVisibility(View.GONE);
        } else {
            mActionButton.setVisibility(View.VISIBLE);
            mActionButton.setText(getActionButtonLabel(step));
        }
    }

    @Override
    public void setActionButtonLoading(boolean loading) {
        if (loading) {
            mActionButton.enableLoadingState();
        } else {
            mActionButton.disableLoadingState();
        }
    }

    @Override
    public void alertReservationTooEarlyToBeAccepted() {
        mPresenter.alertReservationTooEarlyToBeAccepted(getResources().getString(R.string.title_too_early), getResources().getString(R.string.message_too_early));
    }

    @Override
    public void showReassignDriverButton(boolean shouldShow) {
        mReassignDriverButton.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }

    private String getActionButtonLabel(ReservationProgressBar.Step step) {
        switch (step) {
            case NO_STEP:
                return MainApplication.getStringResource(R.string.accept_reservation);
            case STEP_ACCEPTED:
                return MainApplication.getStringResource(R.string.confirm_pickup);
            case STEP_STARTED:
                return MainApplication.getStringResource(R.string.confirm_dropoff);
            default:
                throw new IllegalArgumentException("Illegal step. No button label defined for step " + step);
        }
    }

    private void fixForSamsung() {
        /**
         * This fixes the problem with the Samsung galaxy S3 and the color in the background of the bottom layout
         */
        if ( Build.MANUFACTURER.equals("samsung") && Build.MODEL.contains("I9300") && Build.VERSION.RELEASE.equals(("4.1.2"))) {
            mFragmentReservationDetailsWorkflowContainer.setBackgroundColor(Color.WHITE);
        }
    }
}