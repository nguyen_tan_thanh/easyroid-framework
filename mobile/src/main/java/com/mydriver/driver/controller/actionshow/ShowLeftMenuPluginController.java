package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.event.actionshow.ShowLeftMenuEvent;
import com.mydriver.driver.view.leftmenu.LeftMenuFragment;
import com.sixt.core.eventbus.plugincontroller.SxSingleFragmentPluginController;


public class ShowLeftMenuPluginController extends SxSingleFragmentPluginController<ShowLeftMenuEvent, LeftMenuFragment> {

    public ShowLeftMenuPluginController() {
        super(LeftMenuFragment.class);
    }

    public void onEvent(ShowLeftMenuEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldDisplayFragment(ShowLeftMenuEvent event) {
        return true;
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return false;
    }

    @Override
    protected LeftMenuFragment createFragment(ShowLeftMenuEvent event) {
        return LeftMenuFragment.newInstance();
    }
}
