package com.mydriver.driver.view.driverprofile;


import com.mydriver.driver.view.base.GenericListItem;
import com.sixt.core.eventbus.presenter.EventListeningPresenter;
import com.sixt.mydriver.model.SxMdDriver;

import java.util.ArrayList;
import java.util.List;

class DriverProfilePresenter extends EventListeningPresenter {

    private DriverProfileView view;
    private SxMdDriver driver;

    public void onEvent(DriverUpdatedEvent event) {
        driver = event.getDriver();
        List<GenericListItem> driverDetails = createDriverDetails();
        List<GenericListItem> assignedAreas = DriverDetailsListItemFactory.newAssignedAreaItemList(driver);
        view.drawDriverDetails(driverDetails);
        view.drawDriverRating(driver.getRating().getAverageValue());
        view.drawAssignedAreas(assignedAreas);
    }

    public void start(DriverProfileView view) {
        this.view = view;
        startEventListening();
    }

    public void stop() {
        stopEventListening();
        this.view = null;
    }

    private List<GenericListItem> createDriverDetails() {
        List<GenericListItem> details = new ArrayList<>();
        details.add(DriverDetailsListItemFactory.newDriverNameItem(driver));
        details.add(DriverDetailsListItemFactory.newPhoneNumberItem(driver));
        details.add(DriverDetailsListItemFactory.newPasswordItem(driver));
        return details;
    }
}