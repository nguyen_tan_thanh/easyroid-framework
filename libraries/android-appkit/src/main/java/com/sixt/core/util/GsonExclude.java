package com.sixt.core.util;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.annotations.SerializedName;

public class GsonExclude implements ExclusionStrategy{

	@Override
	public boolean shouldSkipClass(Class<?> arg0) {
		return false;
	}

	@Override
	public boolean shouldSkipField(FieldAttributes field) {
		SerializedName sn = field.getAnnotation(SerializedName.class);
	    if(sn != null)
	        return false;
	    return true;
	}

}
