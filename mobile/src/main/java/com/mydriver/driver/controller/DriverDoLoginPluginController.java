package com.mydriver.driver.controller;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.ShowFeedbackMessageEvent;
import com.octo.android.robospice.exception.NoNetworkException;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdAuthenticationManager;
import com.sixt.mydriver.manager.SxMdDriverManager;
import com.sixt.mydriver.model.FeedbackMessage;
import com.sixt.mydriver.model.SxMdAuthenticationToken;
import com.sixt.mydriver.model.SxMdDriver;
import com.sixt.mydriver.service.SxMdServerSideException;
import com.sixt.mydriver.service.SxMdUnauthorizedException;

public class DriverDoLoginPluginController extends SxPluginController {

    private final SxMdAuthenticationManager mAuthenticationManager;
    private final SxMdDriverManager mDriverManager;
    private final DriverPreferences mDriverPreferences;

    private SxMdAuthenticationManager.OnAuthTokenReceivedListener mReceivedListener = new SxMdAuthenticationManager.OnAuthTokenReceivedListener() {
        @Override
        public void onFailure(Throwable error) {
            DriverLoginReturnedEvent errorEvent = new DriverLoginReturnedEvent(error);
            if (error instanceof SxMdUnauthorizedException) {
                errorEvent.setErrorBecauseOfWrongCredentials(true);
            } else {
                showErrorFeedbackMessage(error);
            }
            post(errorEvent);
        }

        @Override
        public void onSuccess(SxMdAuthenticationToken result)    {
            onAuthenticationSuccess(result);
        }
    };

    private SxMdDriverManager.OnDriverReceivedListener mDriverReceivedListener = new SxMdDriverManager.OnDriverReceivedListener() {
        @Override
        public void onFailure(Throwable error) {
            post(new DriverLoginReturnedEvent(error));
            showErrorFeedbackMessage(error);
        }

        @Override
        public void onSuccess(SxMdDriver result) {
            onLoginSuccess(result);
        }
    };

    public DriverDoLoginPluginController(SxMdAuthenticationManager authenticationManager, SxMdDriverManager driverManager, DriverPreferences driverPreferences){
        mAuthenticationManager = authenticationManager;
        mDriverManager = driverManager;
        mDriverPreferences = driverPreferences;
    }

    private void onAuthenticationSuccess(SxMdAuthenticationToken authToken) {
        mDriverPreferences.setAuthToken(authToken.getToken());
        mDriverPreferences.setPushNotificationsEnabled(true);
        post(new DriverDoLoginEvent(authToken.getToken()));
    }

    private void showErrorFeedbackMessage(Throwable error) {
        String messageText = getErrorMessageText(error);
        if (messageText == null) {
            return;
        }
        FeedbackMessage message = new FeedbackMessage(FeedbackMessage.Type.ERROR, messageText);
        post(new ShowFeedbackMessageEvent(message));
    }

    private String getErrorMessageText(Throwable error) {
        if (error.getCause() instanceof NoNetworkException) {
            return MainApplication.getStringResource(R.string.login_no_connection_error);
        }
        if (error instanceof SxMdServerSideException) {
            SxMdServerSideException serverSideException = (SxMdServerSideException) error;
            return serverSideException.getServerError().getErrorMessage();
        }
        return error.getLocalizedMessage();
    }

    private void onLoginSuccess(SxMdDriver driver) {
        mDriverPreferences.setEmailLastLoggedInDriver(driver.getEmail());
        postSticky(new DriverUpdatedEvent(driver));
        post(new DriverLoginReturnedEvent(driver));
    }

    private String getDeviceName() {
        return "device-name";
    }

    public void onEvent(final DriverDoAuthenticateEvent event) {
        mAuthenticationManager.getAuthToken(MainApplication.getInstance(), event.getUsername(), event.getPassword(), MainApplication.getUserLocale().getLanguage(), getDeviceName(), mReceivedListener);
    }

    public void onEvent(DriverDoLoginEvent event) {
        SxMdAuthenticationManager.setAuthToken(event.getAuthToken());
        mDriverManager.getAuthenticatedDriver(MainApplication.getInstance(), mDriverReceivedListener);
    }
}
