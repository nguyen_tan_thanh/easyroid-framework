package com.mydriver.driver.controller;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.view.driverprofile.editphone.DriverEditPhoneFragment;
import com.sixt.core.eventbus.event.DetachFragmentEvent;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdDriverManager;
import com.sixt.mydriver.model.SxMdDriver;

public class DriverDoEditPhoneNumberPluginController extends SxPluginController {

    private final SxMdDriverManager driverManager;
    private final SxMdDriverManager.OnDriverReceivedListener receivedListener = new SxMdDriverManager.OnDriverReceivedListener() {
        
        @Override
        public void onFailure(Throwable error) {
            post(new NetworkErrorEvent(error));
        }

        @Override
        public void onSuccess(SxMdDriver result) {
            postSticky(new DriverUpdatedEvent(result));
            post(new DetachFragmentEvent(DriverEditPhoneFragment.class));
        }
    } ;

    public DriverDoEditPhoneNumberPluginController(SxMdDriverManager driverManager) {
        this.driverManager = driverManager;
    }

    public void onEvent(DriverDoEditPhoneNumberEvent event) {
        driverManager.changePhoneNumberDriver(event.getPhoneNumber(), MainApplication.getInstance(),receivedListener);
    }
}