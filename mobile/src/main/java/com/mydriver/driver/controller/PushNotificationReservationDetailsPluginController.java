package com.mydriver.driver.controller;


import com.mydriver.driver.event.actionshow.ShowReservationDetailsEvent;
import com.mydriver.driver.event.actionshow.ShowReservationDetailsFromPushNotification;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;

import java.util.List;

public class PushNotificationReservationDetailsPluginController extends SxPluginController {

    private final static int NO_RESERVATION = -1;
    private List<Reservation> acceptedReservations;
    private int reservationIdToShow = NO_RESERVATION;

    public void onEvent(ReservationsAcceptedUpdatedEvent event) {
        acceptedReservations = event.getReservations();
        if (reservationIdToShow != NO_RESERVATION) {
            showReservationDetails();
        }
    }

    public void onEvent(ShowReservationDetailsFromPushNotification event) {
        reservationIdToShow = event.getReservationId();
        if (acceptedReservations != null) {
            showReservationDetails();
        }
    }

    private void showReservationDetails() {
        Reservation reservation = getReservationWithId(reservationIdToShow);
        reservationIdToShow = NO_RESERVATION;
        if (reservation == null) {
            return;
        }
        post(new ShowReservationDetailsEvent(reservation));
    }

    private Reservation getReservationWithId(int reservationId) {
        for (Reservation reservation : acceptedReservations) {
            if (reservation.getReservationId() == reservationId) {
                return reservation;
            }
        }
        return null;
    }
}
