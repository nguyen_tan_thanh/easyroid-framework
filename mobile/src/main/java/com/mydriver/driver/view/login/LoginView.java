package com.mydriver.driver.view.login;


interface LoginView {
    void showInvalidCredentialsValidationErrors();
    void hideValidationErrors();
    void disableLoginButtonProgressState();
    void showLoginForm();
    void directToMainActivity();
    void fillEmailLastLoggedInDriver(String email);
    void setLoginState();
}
