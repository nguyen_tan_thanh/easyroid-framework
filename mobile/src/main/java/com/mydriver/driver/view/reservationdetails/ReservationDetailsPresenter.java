package com.mydriver.driver.view.reservationdetails;

import com.mydriver.driver.event.actionshow.ShowSimpleAlertDialogEvent;
import com.mydriver.driver.event.actionshow.ShowReservationMapEvent;
import com.mydriver.driver.view.base.GenericListItem;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.presenter.EventListeningPresenter;
import com.sixt.core.util.SxCalendarUtil;
import com.sixt.mydriver.model.SxMdReservation;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

class ReservationDetailsPresenter extends EventListeningPresenter {
    private final ReservationDetailsView reservationView;
    private Reservation reservation;

    @Getter
    private final List<GenericListItem> reservationDetails;

    public ReservationDetailsPresenter(ReservationDetailsView reservationView, Reservation reservation) {
        this.reservationView = reservationView;
        this.reservation = reservation;
        reservationDetails = createReservationDetails();
    }

    public void start() {
        startEventListening();
        reservationView.drawReservationDetails(reservationDetails);
        reservationView.setProgressStep(getProgressStep());
        reservationView.showReassignDriverButton(SxMdReservation.Status.ACCEPTED == reservation.getStatus());

        postTrackViewEvent();
    }

    public void stop() {
        stopEventListening();
    }

    private List<GenericListItem> createReservationDetails() {
        List<GenericListItem> items = new ArrayList<>();
        if (reservation.isAcceptedOrStarted()) {
            items.add(ReservationDetailsListItemFactory.newCustomerNameItem(reservation));
        }
        items.add(ReservationDetailsListItemFactory.newTripDateAndTimeItem(reservation));
        items.add(ReservationDetailsListItemFactory.newTripOriginItem(reservation));
        if (reservation.getType() == SxMdReservation.Type.DISTANCE) {
            items.add(ReservationDetailsListItemFactory.newTripDestinationItem(reservation));
            items.add(ReservationDetailsListItemFactory.newTripDistanceItem(reservation));
        } else {
            items.add(ReservationDetailsListItemFactory.newTripDurationItem(reservation));
        }
        items.add(ReservationDetailsListItemFactory.newDriverProvisionItem(reservation));
        items.add(ReservationDetailsListItemFactory.newVehicleInfoItem(reservation));
        if (!StringUtils.isEmpty(reservation.getFlightNumber())) {
            items.add(ReservationDetailsListItemFactory.newFlightNumberItem(reservation));
        }
        if (!StringUtils.isEmpty(reservation.getTrainNumber())) {
            items.add(ReservationDetailsListItemFactory.newTrainNumberItem(reservation));
        }
        if (!StringUtils.isEmpty(reservation.getComment())) {
            items.add(ReservationDetailsListItemFactory.newCommentsItem(reservation));
        }
        if (reservation.getStatus() == SxMdReservation.Status.ACCEPTED) {
            items.add(ReservationDetailsListItemFactory.newSelectedVehicleItem(reservation));
        }
        return items;
    }

    public void showMap() {
        SxEventBus.postEvent(new ShowReservationMapEvent(reservation));
    }

    public void performReservationAction() {
        if (reservation.isRequested()) {
            SxEventBus.postEvent(new ReservationConfirmAcceptEvent(reservation));
            return;
        }
        if (reservation.getStatus().equals(SxMdReservation.Status.ACCEPTED)) {
            if (reservation.getPlannedStartDate().before(SxCalendarUtil.getIntervalUntilAcceptMyDriverReservation() )) {
                reservationView.setActionButtonLoading(true);
                SxEventBus.postEvent(new ReservationConfirmPickUpEvent(reservation));
            } else {
                reservationView.alertReservationTooEarlyToBeAccepted();
            }
            return;
        }
        if (reservation.getStatus().equals(SxMdReservation.Status.STARTED)) {
            reservationView.setActionButtonLoading(true);
            SxEventBus.postEvent(new ReservationConfirmDropOffEvent(reservation));
            return;
        }

        throw new IllegalStateException("illegal reservation state: " + reservation.getStatus());
    }

    public ReservationProgressBar.Step getProgressStep() {
        if (reservation.isRequested()) {
            return ReservationProgressBar.Step.NO_STEP;
        }
        if (reservation.getStatus().equals(SxMdReservation.Status.ACCEPTED)) {
            return ReservationProgressBar.Step.STEP_ACCEPTED;
        }
        if (reservation.getStatus().equals(SxMdReservation.Status.STARTED)) {
            return ReservationProgressBar.Step.STEP_STARTED;
        }
        if (reservation.getStatus().equals(SxMdReservation.Status.COMPLETED)) {
            return ReservationProgressBar.Step.STEP_COMPLETED;
        }
        throw new IllegalStateException("illegal reservation state: " + reservation.getStatus());
    }

    public void onEvent(ReservationStatusUpdatedEvent event) {
        if (event.getReservation().getReservationId() != reservation.getReservationId()) {
            return;
        }
        reservationView.setActionButtonLoading(false);
        reservation = event.getReservation();
        reservationDetails.clear();
        reservationDetails.addAll(createReservationDetails());
        reservationView.drawReservationDetails(reservationDetails);
        reservationView.setProgressStep(getProgressStep());
        reservationView.showReassignDriverButton(SxMdReservation.Status.ACCEPTED == reservation.getStatus());

        postTrackViewEvent();
    }

    private void postTrackViewEvent() {
        post(new TrackReservationDetailsViewEvent(reservation));
    }

    public void onEvent(ReservationStatusUpdateFailedEvent event) {
        reservationView.setActionButtonLoading(false);
    }

    public boolean shouldShowAcceptedReservationActionButtons() {
        return reservation.isAcceptedOrStarted();
    }

    public void alertReservationTooEarlyToBeAccepted(String title, String message) {
        post(new ShowSimpleAlertDialogEvent(title, message));
    }

    public void callHotline() {
        SxEventBus.postEvent(new HotlineNumberListDoUpdateEvent());
    }
}