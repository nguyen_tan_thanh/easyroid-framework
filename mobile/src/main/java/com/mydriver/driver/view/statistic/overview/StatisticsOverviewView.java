package com.mydriver.driver.view.statistic.overview;


import com.mydriver.driver.view.statistic.model.StatisticsRow;

import java.util.List;

interface StatisticsOverviewView {
    void updateStatisticsOverview(List<StatisticsRow> rowItems);
}
