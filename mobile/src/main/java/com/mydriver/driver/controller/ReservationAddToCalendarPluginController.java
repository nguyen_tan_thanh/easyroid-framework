package com.mydriver.driver.controller;

import android.content.Intent;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;

public class ReservationAddToCalendarPluginController extends SxPluginController {

    public void onEvent(ReservationAddToCalendarEvent event) {
        if (event.getReservation() == null) {
            return;
        }
        addReservationToCalendar(event.getReservation());
    }

    private void addReservationToCalendar(Reservation reservation) {
        long startTime = reservation.getPlannedStartDate().getTimeInMillis();
        long endTime = reservation.getPlannedStartDate().getTimeInMillis();
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("beginTime",startTime);
        intent.putExtra("allDay", false);
        intent.putExtra("endTime", endTime);
        intent.putExtra("title", MainApplication.getStringResource(R.string.mydriver_trip));
        intent.putExtra("description",  reservation.getComment());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MainApplication.getInstance().startActivity(intent);
    }
}