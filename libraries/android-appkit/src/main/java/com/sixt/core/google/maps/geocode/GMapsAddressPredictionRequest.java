package com.sixt.core.google.maps.geocode;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.sixt.core.google.maps.GMapsApiEndpoint;
import com.sixt.core.google.maps.geocode.model.GMapsAddressPredictionList;
import com.sixt.core.webservice.request.SxBaseRequest;


public class GMapsAddressPredictionRequest extends SxBaseRequest<GMapsApiEndpoint, GMapsAddressPredictionList> {

    private static final String SERVER_URL = "http://maps.googleapis.com/maps/api";
    private final String address;
    private final String language;
    private String region;
    private final boolean hasSensor = true;

    public GMapsAddressPredictionRequest(String address, String language) {
        super(GMapsApiEndpoint.class, GMapsAddressPredictionList.class, SERVER_URL);
        this.address = address;
        this.language = language;
    }

    public GMapsAddressPredictionRequest(String address, String language, String region) {
        super(GMapsApiEndpoint.class, GMapsAddressPredictionList.class, SERVER_URL);
        this.address = address;
        this.language = language;
        this.region = region;
    }

    @Override
    public long getCacheDuration() {
        return DurationInMillis.ONE_MINUTE;
    }

    @Override
    public Object getIdentifier() {
        return (SERVER_URL + GMapsApiEndpoint.URL_GET_ADDRESS_PREDICTION + address + language + getRegion()).hashCode();
    }

    @Override
    public GMapsAddressPredictionList loadDataFromNetwork() throws Exception {
        return getEndpoint().getAddressPrediction(address, language, hasSensor, getComponents(), getRegion());
    }

    public String getComponents() {
        return "";
    }

    public String getRegion() {
        if (region == null) {
            return "";
        }
        return region;
    }
}
