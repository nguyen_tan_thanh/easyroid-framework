package com.sixt.core.util;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.TypeVariable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SxGsonUppercaseTypeAdapterFactory implements TypeAdapterFactory{

	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
	       Class<T> rawType = (Class<T>) type.getRawType();
	       if(rawType.isArray()){
	    	   return null;
	       }

	       final Map<String, T> lowercaseToConstant = new HashMap<String, T>();
	       
	       Field[] fields = rawType.getFields();
	       Field[] declFields = rawType.getDeclaredFields();
	       TypeVariable<Class<T>>[] vars = rawType.getTypeParameters();
	       
	       
	       for(Field field : rawType.getDeclaredFields()){
	    	   lowercaseToConstant.put(toUppercase(field.getName()), (T) field.getName());
	       }
	       
	       
	       return new TypeAdapter<T>() {
	         public void write(JsonWriter out, T value) throws IOException {
	           if (value == null) {
	             out.nullValue();
	           } else {
	             out.value(toUppercase(value));
	           }
	         }

	         public T read(JsonReader reader) throws IOException {
	           if (reader.peek() == JsonToken.NULL) {
	             reader.nextNull();
	             return null;
	           } else {
	             return lowercaseToConstant.get(reader.nextString());
	           }
	         }
	       };
	     }

	     private String toUppercase(Object o) {
	       return o.toString().toUpperCase(Locale.US);
	     }
	
}
