package com.mydriver.driver.view.driverprofile.editpassword;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mydriver.driver.R;
import com.mydriver.driver.view.base.ButterKnifeEventReportingFragment;
import com.sixt.core.util.KeyboardUtils;

import butterknife.InjectView;

public class DriverEditPasswordFragment extends ButterKnifeEventReportingFragment implements DriverEditPasswordView {

    @InjectView(R.id.editCurrentPassword)
    EditText mEditPasswordOld;

    @InjectView(R.id.editNewPassword)
    EditText mEditPasswordNew;

    @InjectView(R.id.editConfirmNewPassword)
    EditText mEditPasswordNewConfirm;

    private DriverEditPasswordPresenter mPresenter;

    public static DriverEditPasswordFragment newInstance() {
        return new DriverEditPasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mPresenter = new DriverEditPasswordPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.driver_change_password_fragment, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_change_phone_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save_details:
                KeyboardUtils.hideSoftKeyboard(getActivity());
                mPresenter.changePassword(mEditPasswordOld.getText().toString(), mEditPasswordNew.getText().toString(), mEditPasswordNewConfirm.getText().toString());
                break;
        }
        return true;
    }

    @Override
    public void showErrorPasswordDoNotMatch() {
        String error = getString(R.string.wrong_password_change_information);
        mEditPasswordNewConfirm.setError(error);
        mEditPasswordNew.setError(error);
    }

    @Override
    public void showErrorWrongOldPassword() {
        String error = getString(R.string.wrong_old_password);
        mEditPasswordOld.setError(error);

    }

    @Override
    public void hideError() {
        mEditPasswordOld.setError(null);
        mEditPasswordNew.setError(null);
    }
}