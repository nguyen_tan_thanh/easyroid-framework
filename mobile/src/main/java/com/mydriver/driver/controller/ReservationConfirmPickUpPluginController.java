package com.mydriver.driver.controller;

import com.mydriver.driver.MainApplication;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdReservationManager;
import com.sixt.mydriver.model.SxMdReservation;

import java.util.Collections;
import java.util.List;

public class ReservationConfirmPickUpPluginController extends SxPluginController {

    private final SxMdReservationManager reservationManager;

    private final SxMdReservationManager.OnReservationReceivedListener receivedListener = new SxMdReservationManager.OnReservationReceivedListener() {
        @Override
        public void onFailure(Throwable error) {
            post(new NetworkErrorEvent(error));
            post(new ReservationStatusUpdateFailedEvent());
        }

        @Override
        public void onSuccess(SxMdReservation result) {
            post(new ReservationStatusUpdatedEvent(new Reservation(result)));
            updateAcceptedReservationList(result);
        }
    };

    public ReservationConfirmPickUpPluginController(SxMdReservationManager reservationManager) {
        this.reservationManager = reservationManager;
    }

    public void onEvent(ReservationConfirmPickUpEvent event) {
        reservationManager.startReservation(MainApplication.getInstance(), event.getReservation().getReservationId(), receivedListener);
    }

    protected void updateAcceptedReservationList(SxMdReservation startedReservation) {
        List<Reservation> acceptedReservations = getStickyEvent(ReservationsAcceptedUpdatedEvent.class).getReservations();
        Reservation acceptedReservation = null;
        for (Reservation reservation : acceptedReservations) {
            if (reservation.getItem().getReservationId() == startedReservation.getReservationId()) {
                acceptedReservation = reservation;
            }
        }
        if (acceptedReservation != null) {
            acceptedReservations.remove(acceptedReservation);
        }
        acceptedReservations.add(new Reservation(startedReservation, true));
        Collections.sort(acceptedReservations, new ReservationComparator());
        postSticky(new ReservationsAcceptedUpdatedEvent(acceptedReservations));
    }

}