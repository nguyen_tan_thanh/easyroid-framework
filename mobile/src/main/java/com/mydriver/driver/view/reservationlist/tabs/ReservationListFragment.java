package com.mydriver.driver.view.reservationlist.tabs;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.mydriver.driver.R;
import com.mydriver.driver.view.base.ButterKnifeEventReportingFragment;
import com.mydriver.driver.view.reservationlist.ReservationListTabItem;

import java.util.ArrayList;

import butterknife.InjectView;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class ReservationListFragment extends ButterKnifeEventReportingFragment implements ReservationListView {

    private static final String EXTRA_RESERVATIONS_STATUS = "reservation_tab_status";

    @InjectView(R.id.reservations_swipe_container)
    SwipeRefreshLayout mSwipeContainer;

    @InjectView(R.id.listViewReservations)
    StickyListHeadersListView mReservationsListView;

    private ReservationListPresenter mPresenter;
    private ReservationListAdapter mAdapter;

    public static ReservationListFragment newInstance(ReservationListTabItem tab) {
        ReservationListFragment reservationListFragment = new ReservationListFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(EXTRA_RESERVATIONS_STATUS, tab);
        reservationListFragment.setArguments(arguments);
        return reservationListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ReservationListTabItem tabItem = (ReservationListTabItem) getArguments().getSerializable(EXTRA_RESERVATIONS_STATUS);
        mPresenter = ReservationListPresenterFactory.createReservationsPresenter(tabItem);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reservation_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupSwipeContainer();
        setupReservationsList();
        mPresenter.start(this);
    }

    private void setupSwipeContainer() {
        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.refreshReservations();
            }
        });
        mSwipeContainer.setColorSchemeResources(
                R.color.MyDriver_Design_blue,
                R.color.MyDriver_Design_yellow,
                R.color.MyDriver_Design_green,
                R.color.MyDriver_Design_red);
    }

    @Override
    public void onDestroyView() {
        mPresenter.stop();
        super.onDestroyView();
    }

    private void setupReservationsList() {
        mAdapter = new ReservationListAdapter(getActivity());
        mReservationsListView.setAdapter(mAdapter);
        mReservationsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mPresenter.onItemSelected(position);
            }
        });
    }

    @Override
    public void updateReservationsList(ArrayList<ReservationListItemModel> reservations) {
        mSwipeContainer.setRefreshing(false);
        mAdapter.updateReservationList(reservations);
    }
}
