package com.mydriver.driver.view.statistic.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class StatisticsOverviewItemIdModel implements Serializable {
    private final int year;
    private final int month;

    public StatisticsOverviewItemIdModel(int year, int month) {
        this.year = year;
        this.month = month;
    }
}
