package com.mydriver.driver.controller;

import com.mydriver.driver.MainApplication;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdReservationManager;
import com.sixt.mydriver.model.SxMdReservation;

import java.util.List;

public class ReservationConfirmDropOffPluginController extends SxPluginController {

    private final SxMdReservationManager reservationManager;

    private final SxMdReservationManager.OnReservationReceivedListener receivedListener = new SxMdReservationManager.OnReservationReceivedListener() {
        @Override
        public void onFailure(Throwable error) {
            post(new NetworkErrorEvent(error));
            post(new ReservationStatusUpdateFailedEvent());
        }

        @Override
        public void onSuccess(SxMdReservation result) {
            updateReservationLists(new Reservation(result));
            post(new ReservationStatusUpdatedEvent(new Reservation(result)));
        }
    };

    public ReservationConfirmDropOffPluginController(SxMdReservationManager reservationManager) {
        this.reservationManager = reservationManager;
    }

    public void onEvent(ReservationConfirmDropOffEvent event) {
        reservationManager.completeReservation(MainApplication.getInstance(), event.getReservation().getReservationId(), receivedListener);
    }

    private void updateReservationLists(Reservation newCompletedReservation) {
        List<Reservation> acceptedReservations = getStickyEvent(ReservationsAcceptedUpdatedEvent.class).getReservations();
        List<Reservation> completedReservations = getStickyEvent(ReservationsCompletedUpdatedEvent.class).getReservations();
        int indexToBeRemoved = -1;
        for (int i = 0; i < acceptedReservations.size(); i++) {
            if (acceptedReservations.get(i).getReservationId() == newCompletedReservation.getReservationId()) {
                indexToBeRemoved = i;
                break;
            }
        }
        if (indexToBeRemoved > -1) {
            acceptedReservations.remove(indexToBeRemoved);
        }
        completedReservations.add(newCompletedReservation);
        postSticky(new ReservationsAcceptedUpdatedEvent(acceptedReservations));
        postSticky(new ReservationsCompletedUpdatedEvent(completedReservations));
    }
}