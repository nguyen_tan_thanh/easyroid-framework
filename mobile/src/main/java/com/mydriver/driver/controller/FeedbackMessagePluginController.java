package com.mydriver.driver.controller;

import android.app.Activity;
import android.widget.Toast;

import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.DismissFeedbackMessageEvent;
import com.mydriver.driver.event.actionshow.ShowFeedbackMessageEvent;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.model.FeedbackMessage;

import org.apache.commons.lang3.NotImplementedException;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class FeedbackMessagePluginController extends SxPluginController {

    private final static long SNACK_BAR_INFINITE_DURATION_IN_MILLISECONDS = 604800000; //One week
    private final Map<String, Snackbar> shownInfiniteSnackBars = new HashMap<>();
    private WeakReference<Activity> lastStartedActivity;

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(FeedbackMessageDoRegisterActivityEvent event) {
        lastStartedActivity = new WeakReference<>(event.getActivity());
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(ShowFeedbackMessageEvent event) {
        if (lastStartedActivity.get() == null) {
            return;
        }

        switch (event.getFeedbackMessage().getType()) {
            case INFO:
                showInfo(event.getFeedbackMessage());
                break;
            case WARNING:
                showWarning(event.getFeedbackMessage());
                break;
            case ERROR:
                showError(event.getFeedbackMessage());
                break;
            case TOAST:
                showToast(event.getFeedbackMessage());
                break;
        }
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(DismissFeedbackMessageEvent event) {
        Snackbar snackbar = shownInfiniteSnackBars.get(event.getFeedbackMessageTag());
        if (snackbar == null) {
            return;
        }
        snackbar.dismiss();
        shownInfiniteSnackBars.remove(snackbar);
    }

    private void showError(FeedbackMessage feedbackMessage) {
        Snackbar snackbar = Snackbar.with(lastStartedActivity.get())
                .type(SnackbarType.SINGLE_LINE)
                .text(feedbackMessage.getMessage())
                .textColorResource(R.color.MyDriver_Design_white)
                .colorResource(R.color.MyDriver_Design_red);
        configureSnackBarDuration(snackbar, feedbackMessage);
        SnackbarManager.show(snackbar);
    }

    private void showToast(FeedbackMessage feedbackMessage) {
        if (feedbackMessage.getDuration() == FeedbackMessage.Duration.SHORT) {
            Toast.makeText(lastStartedActivity.get(), feedbackMessage.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(lastStartedActivity.get(), feedbackMessage.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void showWarning(FeedbackMessage feedbackMessage) {
        throw new NotImplementedException("Not implemented yet");
    }

    private void showInfo(FeedbackMessage feedbackMessage) {
        throw new NotImplementedException("Not implemented yet");
    }

    private void configureSnackBarDuration(Snackbar snackbar, FeedbackMessage message) {
        if (message.getDuration() == FeedbackMessage.Duration.INFINITE) {
            snackbar.duration(SNACK_BAR_INFINITE_DURATION_IN_MILLISECONDS);
            snackbar.swipeToDismiss(false);
            snackbar.setTag(message.getId());
            shownInfiniteSnackBars.put(message.getId(), snackbar);
            return;
        }
        else if (message.getDuration() == FeedbackMessage.Duration.SHORT) {
            snackbar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT);
        } else {
            snackbar.duration(Snackbar.SnackbarDuration.LENGTH_LONG);
        }
    }
}
