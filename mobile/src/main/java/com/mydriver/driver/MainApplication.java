package com.mydriver.driver;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.sixt.core.eventbus.SxEventBus;
import com.squareup.leakcanary.LeakCanary;

import java.util.Locale;

public class MainApplication extends Application {
    
    private static MainApplication sInstance;
    private MainEventBusRegistry mRegistry;

    public static MainApplication getInstance() {
        return sInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        sInstance = this;
        super.onCreate();
        LeakCanary.install(this);
        startEventProcessing();
    }

    private void startEventProcessing() {
        mRegistry = new MainEventBusRegistry(this);
        mRegistry.registerDefaultSubscribers();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        sInstance = null;
        mRegistry.unregisterAllSubscribers();
        mRegistry = null;
    }

    public static String getStringResource(int resId) {
        return sInstance.getResources().getString(resId);
    }

    public static String getStringResource(int resId, Object... formatArgs) {
        return sInstance.getResources().getString(resId, formatArgs);
    }

    public static Locale getUserLocale() {
        return getInstance().getResources().getConfiguration().locale;
    }

    public static boolean isUserLoggedIn() {
        DriverUpdatedEvent event = SxEventBus.getSticky(DriverUpdatedEvent.class);
        return event != null && event.getDriver() != null;
    }
}