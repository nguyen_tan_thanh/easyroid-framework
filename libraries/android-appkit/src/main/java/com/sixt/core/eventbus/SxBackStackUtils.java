package com.sixt.core.eventbus;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Pair;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

public class SxBackStackUtils {

    private SxBackStackUtils() {}

    public static void bringFragmentToFront(FragmentManager fragmentManager, String tag, int containerId) {
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment == null) {
            return;
        }

        ArrayList<Pair<String, Fragment>> poppedElements = getTopmostFragmentsInclusive(fragmentManager, fragment);
        String topmostFragmentTitle = poppedElements.get(0).first;
        addPoppedFragments(fragmentManager, containerId, poppedElements);
        addTopmostFragment(fragmentManager, containerId, fragment, topmostFragmentTitle);
    }

    private static ArrayList<Pair<String, Fragment>> getTopmostFragmentsInclusive(FragmentManager fragmentManager, Fragment fragment) {
        ArrayList<Pair<String, Fragment>> poppedElements = new ArrayList<>();
        for (int i = fragmentManager.getBackStackEntryCount() - 1; i >= 0; i--) {
            String nameAt = fragmentManager.getBackStackEntryAt(i).getName();
            String titleAt = fragmentManager.getBackStackEntryAt(i).getBreadCrumbTitle().toString();
            Fragment fragmentAt = fragmentManager.findFragmentByTag(nameAt);
            fragmentManager.popBackStackImmediate();

            poppedElements.add(new Pair<>(titleAt, fragmentAt));

            if (fragmentAt == fragment) {
                break;
            }
        }
        return poppedElements;
    }

    private static void addPoppedFragments(FragmentManager fragmentManager, int containerId, ArrayList<Pair<String, Fragment>> fragments) {
        for (int i = 0; i < fragments.size() -1; i++) {
            FragmentTransaction transactionAdd = fragmentManager.beginTransaction();
            Fragment fragment = fragments.get(i).second;
            String tag = fragment.getClass().getName();

            transactionAdd.replace(containerId, fragment, tag);
            transactionAdd.addToBackStack(tag);
            transactionAdd.setBreadCrumbTitle(fragments.get(i).first);
            transactionAdd.commit();
        }
    }

    private static void addTopmostFragment(FragmentManager fragmentManager, int containerId, Fragment fragment, String title) {
        FragmentTransaction transactionAdd = fragmentManager.beginTransaction();
        transactionAdd.replace(containerId, fragment, fragment.getClass().getName());
        transactionAdd.addToBackStack(fragment.getClass().getName());
        transactionAdd.setBreadCrumbTitle(title);
        transactionAdd.commit();
    }

    public static String getBackStackCurrentItemTitle(FragmentManager fragmentManager) {
        if (fragmentManager.getBackStackEntryCount() == 0) {
            return "";
        }
        FragmentManager.BackStackEntry currentEntry = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1);
        if (currentEntry == null || StringUtils.isEmpty(currentEntry.getBreadCrumbTitle())) {
            return "";
        }
        return currentEntry.getBreadCrumbTitle().toString();
    }

    public static SxHomeButtonMode getBackStackCurrentItemHomeButtonMode(FragmentManager fragmentManager) {
        if (fragmentManager.getBackStackEntryCount() == 0) {
            return SxHomeButtonMode.DRAWER_TOGGLE;
        }

        String tag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment.getArguments() == null || fragment.getArguments().getSerializable(SxHomeButtonMode.BUNDLE_ID) == null) {
            return SxHomeButtonMode.DRAWER_TOGGLE;
        }
        return  (SxHomeButtonMode) fragment.getArguments().getSerializable(SxHomeButtonMode.BUNDLE_ID);
    }

    private static int getFragmentIndex(FragmentManager fragmentManager, Fragment fragment) {
        for (int i = 0; i<fragmentManager.getBackStackEntryCount(); i++) {
            if (fragmentManager.getBackStackEntryAt(i) == fragment) {
                return i;
            }
        }
        return -1;
    }
}
