package com.mydriver.driver.view.reservationmap.presenter;

import com.google.android.gms.maps.model.LatLng;
import com.mydriver.driver.view.reservationmap.ReservationMapView;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.presenter.PresenterHierarchy;

public class MapMasterPresenter {

    private final PresenterHierarchy<ReservationMapView> presenterHierarchy;
    private final Reservation reservation;
    private final MapCameraPresenter mapCameraPresenter;

    public MapMasterPresenter(Reservation reservation) {
        this.reservation = reservation;
        mapCameraPresenter = new MapCameraPresenter(reservation);
        presenterHierarchy = new PresenterHierarchy<>(
                mapCameraPresenter,
                new MapOriginDestinationMarkerPresenter(reservation),
                new MapRoutePresenter(reservation),
                new MapOwnLocationMarkerPresenter()
        );
    }

    public void start(ReservationMapView view) {
        presenterHierarchy.start(view);
    }

    public void stop() {
        presenterHierarchy.stop();
    }

    public void onOwnLocationButtonClick() {
        mapCameraPresenter.onOwnLocationButtonClick();
    }

    public void onNavigateButtonClick() {
        LatLng navigationDestination = new LatLng(reservation.getOrigin().getLatitude(), reservation.getOrigin().getLongitude());
        SxEventBus.postEvent(new NavigateToAddressEvent(navigationDestination, reservation.getOrigin().getName()));
    }
}
