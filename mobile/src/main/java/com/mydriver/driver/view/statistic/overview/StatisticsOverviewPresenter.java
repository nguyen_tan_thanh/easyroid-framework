package com.mydriver.driver.view.statistic.overview;

import com.mydriver.driver.view.statistic.model.StatisticsItemIdComparator;
import com.mydriver.driver.view.statistic.model.StatisticsOverviewItemIdModel;
import com.mydriver.driver.view.statistic.model.StatisticsOverviewItemModel;
import com.mydriver.driver.view.statistic.model.StatisticsRow;
import com.sixt.core.eventbus.presenter.EventListeningPresenter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

class StatisticsOverviewPresenter extends EventListeningPresenter {

    private final List<StatisticsRow> rowItems = new ArrayList<>();
    private List<Reservation> completedReservations;
    private StatisticsOverviewView view;

    public void start(StatisticsOverviewView view) {
        this.view = view;
        startEventListening();
        if (completedReservations == null) {
            refreshStatistics();
        }
    }

    public void stop() {
        stopEventListening();
    }

    public void refreshStatistics() {
        post(new ReservationsCompletedDoUpdateEvent());
    }

    public void onEvent(ReservationsCompletedUpdatedEvent event) {
        completedReservations = event.getReservations();
        rowItems.clear();
        createRows(completedReservations);
        view.updateStatisticsOverview(rowItems);
    }

    private void createRows(List<Reservation> reservations) {
        List<StatisticsOverviewItemModel> statisticsItems = new ArrayList<>();
        SortedMap<StatisticsOverviewItemIdModel, List<Reservation>> reservationMapping = new TreeMap<>(new StatisticsItemIdComparator());

        for (Reservation reservation : reservations) {
            int year = reservation.getStartDate().get(Calendar.YEAR);
            int monthAsInteger = reservation.getStartDate().get(Calendar.MONTH);
            StatisticsOverviewItemIdModel key = new StatisticsOverviewItemIdModel(year, monthAsInteger);
            if (reservationMapping.containsKey(key)) {
                List<Reservation> value = reservationMapping.get(key);
                value.add(reservation);
            } else {
                List<Reservation> value = new ArrayList<>();
                value.add(reservation);
                reservationMapping.put(key, value);
            }
        }
        for (StatisticsOverviewItemIdModel itemId : reservationMapping.keySet()) {
            StatisticsOverviewItemModel item = new StatisticsOverviewItemModel(itemId, reservationMapping.get(itemId));
            statisticsItems.add(item);
        }

        for (StatisticsOverviewItemModel item : statisticsItems) {
            if (rowItems.isEmpty() || rowItems.get(rowItems.size() - 1).isRowFull()) {
                StatisticsRow row = new StatisticsRow(item);
                rowItems.add(row);
                continue;
            }
            rowItems.get(rowItems.size() -1).setSecond(item);
        }
    }
}
