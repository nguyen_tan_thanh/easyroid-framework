package com.mydriver.driver.view.reservationdetails;

import com.mydriver.driver.view.base.GenericListItem;

import java.util.List;

interface ReservationDetailsView {
    void setProgressStep(ReservationProgressBar.Step step);
    void drawReservationDetails(List<GenericListItem> reservationDetails);
    void setActionButtonLoading(boolean loading);
    void alertReservationTooEarlyToBeAccepted();
    void showReassignDriverButton(boolean shouldShow);
}
