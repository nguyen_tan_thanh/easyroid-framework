package com.sixt.core.google.maps.routes.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class GMapsLeg {

    private TextValuePair<String, Integer> distance;

    private TextValuePair<String, Integer> duration;

    @SerializedName("start_address")
    private String startAddress;

    @SerializedName("end_address")
    private String endAddress;

    @SerializedName("start_location")
    private LatLng startLocation;

    @SerializedName("end_location")
    private LatLng endLocation;

    private List<GMapsStep> steps;

}
