package com.mydriver.driver.view.base;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;


public class WrappingRecyclerViewLayoutManager extends LinearLayoutManager {

    private static final int DIMENSION_WIDTH = 0;
    private static final int DIMENSION_HEIGHT = 1;

    public WrappingRecyclerViewLayoutManager(Context context) {
        super(context, VERTICAL, false);
    }

    @SuppressWarnings("unused")
    public WrappingRecyclerViewLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    private final int[] mMeasuredDimension = new int[2];

    @Override
    public void onMeasure(RecyclerView.Recycler recycler, RecyclerView.State state, int widthSpec, int heightSpec) {
        final int widthMode = View.MeasureSpec.getMode(widthSpec);
        final int heightMode = View.MeasureSpec.getMode(heightSpec);
        final int widthSize = View.MeasureSpec.getSize(widthSpec);
        final int heightSize = View.MeasureSpec.getSize(heightSpec);
        int width = 0;
        int height = 0;
        for (int i = 0; i < getItemCount(); i++) {
            measureScrapChild(
                    recycler, i,
                    View.MeasureSpec.makeMeasureSpec(i, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(i, View.MeasureSpec.UNSPECIFIED),
                    mMeasuredDimension);
            if (getOrientation() == HORIZONTAL) {
                width = width + mMeasuredDimension[DIMENSION_WIDTH];
                if (i == 0) {
                    height = mMeasuredDimension[DIMENSION_HEIGHT];
                }
            } else {
                height = height + mMeasuredDimension[DIMENSION_HEIGHT];
                if (i == 0) {
                    width = mMeasuredDimension[DIMENSION_WIDTH];
                }
            }
        }
        switch (widthMode) {
            case View.MeasureSpec.EXACTLY:
                width = widthSize;
            case View.MeasureSpec.AT_MOST:
            case View.MeasureSpec.UNSPECIFIED:
        }
        switch (heightMode) {
            case View.MeasureSpec.EXACTLY:
                height = heightSize;
            case View.MeasureSpec.AT_MOST:
            case View.MeasureSpec.UNSPECIFIED:
        }

        setMeasuredDimension(width, height);
    }

    private void measureScrapChild(RecyclerView.Recycler recycler, int position, int widthSpec, int heightSpec, int[] measuredDimension) {
        View view = recycler.getViewForPosition(position);
        if (view != null) {
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) view.getLayoutParams();
            int childWidthSpec = ViewGroup.getChildMeasureSpec(widthSpec, getPaddingLeft() + getPaddingRight(), params.width);
            int childHeightSpec = ViewGroup.getChildMeasureSpec(heightSpec, getPaddingTop() + getPaddingBottom(), params.height);
            view.measure(childWidthSpec, childHeightSpec);
            Rect outRect = new Rect();
            calculateItemDecorationsForChild(view, outRect);
            measuredDimension[DIMENSION_WIDTH] = view.getMeasuredWidth() + params.leftMargin + params.rightMargin;
            measuredDimension[DIMENSION_HEIGHT] = view.getMeasuredHeight() + params.bottomMargin + params.topMargin + outRect.bottom + outRect.top;
            recycler.recycleView(view);
        }
    }
}
