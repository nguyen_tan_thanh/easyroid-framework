package com.mydriver.driver.controller;

import android.content.Intent;
import android.net.Uri;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.ShowFeedbackMessageEvent;
import com.mydriver.driver.event.actionshow.ShowHotlineSelectionDialogEvent;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdHotlineNumberManager;
import com.sixt.mydriver.model.FeedbackMessage;
import com.sixt.mydriver.model.SxMdHotlineNumberList;

public class CallHotlinePluginController extends SxPluginController {

    private final SxMdHotlineNumberManager hotlineNumberManager;

    private SxMdHotlineNumberManager.OnHotlineListReceivedListener mHotlineNumberListListener = new SxMdHotlineNumberManager.OnHotlineListReceivedListener() {
        @Override
        public void onFailure(Throwable error) {
            showNoHotlineNumberErrorMessage();
        }

        @Override
        public void onSuccess(SxMdHotlineNumberList result) {
            onHotlineNumberListReceived(result) ;
        }
     };

    public CallHotlinePluginController(SxMdHotlineNumberManager manager) {
        this.hotlineNumberManager = manager;
    }

    public void onEvent(HotlineNumberListDoUpdateEvent event) {
        getHotlineNumbers();
    }

    public void onEvent(CallHotlineEvent event) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + event.getPhoneNumber()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MainApplication.getInstance().startActivity(intent);
    }

    private void showNoHotlineNumberErrorMessage() {
        FeedbackMessage message = new FeedbackMessage(FeedbackMessage.Type.ERROR, MainApplication.getStringResource(R.string.call_hotline_error_no_number));
        post(new ShowFeedbackMessageEvent(message));
    }

    private void getHotlineNumbers() {
        hotlineNumberManager.getDriverHotlineNumberList(MainApplication.getInstance(), mHotlineNumberListListener);
    }

    private void onHotlineNumberListReceived(SxMdHotlineNumberList result) {
        post(new ShowHotlineSelectionDialogEvent(result));
    }
}
