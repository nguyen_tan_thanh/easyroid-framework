package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.controller.base.AnimatingSingleFragmentPluginController;
import com.mydriver.driver.event.actionshow.ShowDriverProfileEvent;
import com.mydriver.driver.view.driverprofile.DriverProfileFragment;
import com.sixt.core.eventbus.SxEventBus;

public class ShowDriverProfilePluginController extends AnimatingSingleFragmentPluginController<ShowDriverProfileEvent, DriverProfileFragment> {

    public ShowDriverProfilePluginController() {
        super(DriverProfileFragment.class);
    }

    public void onEvent(ShowDriverProfileEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldDisplayFragment(ShowDriverProfileEvent event) {
        DriverUpdatedEvent driverUpdatedEvent = SxEventBus.getSticky(DriverUpdatedEvent.class);
        return !(driverUpdatedEvent == null || driverUpdatedEvent.getDriver() == null);
    }

    @Override
    protected String createActionBarTitle() {
        return MainApplication.getStringResource(R.string.menu_profile);
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return false;
    }

    @Override
    protected DriverProfileFragment createFragment(ShowDriverProfileEvent event) {
        return DriverProfileFragment.newInstance();
    }
}