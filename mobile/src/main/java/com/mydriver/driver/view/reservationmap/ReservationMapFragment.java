package com.mydriver.driver.view.reservationmap;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mydriver.driver.R;
import com.mydriver.driver.view.base.ButterKnifeEventReportingFragment;
import com.mydriver.driver.view.reservationmap.presenter.MapMasterPresenter;

import butterknife.InjectView;
import butterknife.OnClick;

public class ReservationMapFragment extends ButterKnifeEventReportingFragment implements ReservationMapView {

    private static final String EXTRA_RESERVATION = "extra_reservation";
    private static final String TAG_MAP_FRAGMENT = "tag_map_fragment";

    @InjectView(R.id.reservationMapFragmentNavigateButton)
    ImageButton mNavigateButton;

    @InjectView(R.id.reservationMapFragmentOwnLocationButton)
    ImageButton mOwnLocationButton;

    @InjectView(R.id.reservationMapFragmentMapContainer)
    ViewGroup mMapContainer;

    private GoogleMap mGoogleMap;
    private MapMasterPresenter mPresenter;

    public static ReservationMapFragment newInstance(Reservation reservation) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(EXTRA_RESERVATION, reservation);
        ReservationMapFragment fragment =  new ReservationMapFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.reservation_map_fragment, container, false);
        Reservation reservation = (Reservation) getArguments().getSerializable(EXTRA_RESERVATION);
        mPresenter = new MapMasterPresenter(reservation);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.reservationMapFragmentMapContainer, SupportMapFragment.newInstance(), TAG_MAP_FRAGMENT).commit();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleMap == null) {
            ((SupportMapFragment) getChildFragmentManager().findFragmentByTag(TAG_MAP_FRAGMENT)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    googleMap.getUiSettings().setZoomControlsEnabled(false);
                    googleMap.getUiSettings().setRotateGesturesEnabled(false);
                    googleMap.getUiSettings().setTiltGesturesEnabled(false);
                    googleMap.getUiSettings().setIndoorLevelPickerEnabled(false);
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    mGoogleMap = googleMap;
                    mPresenter.start(ReservationMapFragment.this);
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        mPresenter.stop();
        super.onDestroyView();
    }

    @OnClick(R.id.reservationMapFragmentOwnLocationButton)
    public void onMapCurrentPositionButtonClick() {
        mPresenter.onOwnLocationButtonClick();
    }

    @OnClick(R.id.reservationMapFragmentNavigateButton)
    public void onNavigationButtonClick() {
        mPresenter.onNavigateButtonClick();
    }

    @Override
    public Marker addMarker(MarkerOptions markerOptions) {
        return mGoogleMap.addMarker(markerOptions);
    }

    @Override
    public Polyline addPolyline(PolylineOptions polylineOptions) {
        return mGoogleMap.addPolyline(polylineOptions);
    }

    @Override
    public void moveCameraToPosition(LatLng position, float zoomLevel, boolean animate) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(position, zoomLevel);
        if (animate) {
            mGoogleMap.animateCamera(cameraUpdate);
        } else {
            mGoogleMap.moveCamera(cameraUpdate);
        }
    }

    @Override
    public void moveCameraToArea(LatLngBounds area, int padding, boolean animate) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(area, padding);
        if (animate) {
            mGoogleMap.animateCamera(cameraUpdate);
        } else {
            mGoogleMap.moveCamera(cameraUpdate);
        }
    }
}