package com.mydriver.driver.view.vehicleselection;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mydriver.driver.R;
import com.mydriver.driver.view.base.StyledEventReportingDialogFragment;
import com.mydriver.driver.view.base.DividerDecoration;
import com.mydriver.driver.view.base.GenericListItem;
import com.mydriver.driver.view.base.GenericListItemAdapter;
import com.mydriver.driver.view.base.WrappingRecyclerViewLayoutManager;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class VehicleSelectionDialogFragment extends StyledEventReportingDialogFragment implements VehicleSelectionView {

    private static final String EXTRA_RESERVATION = "extra_reservation";

    @InjectView(R.id.vehiclesListDialogListView) RecyclerView mVehicleListView;
    @InjectView(R.id.vehiclesListViewEmptyContainer) ViewGroup mVehicleListViewEmptyContainer;
    @InjectView(R.id.vehiclesListViewLoadingIndicator) ProgressBar mProgressBar;
    @InjectView(R.id.vehiclesListViewEmptyMessage) TextView mEmptyMessageTextView;

    private VehicleSelectionPresenter mPresenter;
    private GenericListItemAdapter mAdapter;

    public static VehicleSelectionDialogFragment newInstance(Reservation reservation) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(EXTRA_RESERVATION, reservation);
        VehicleSelectionDialogFragment fragment = new VehicleSelectionDialogFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View content = getActivity().getLayoutInflater().inflate(R.layout.vehicle_list_dialog_content, null);
        ButterKnife.inject(this, content);
        setStateLoading();
        mAdapter = new GenericListItemAdapter();
        mAdapter.setOnItemSelectedListener(new GenericListItemAdapter.OnItemSelectedListener() {
            @Override
            public void onItemSelected(GenericListItem item, int position) {
                mPresenter.onSelectVehicle(position);
            }
        });
        mVehicleListView.addItemDecoration(new DividerDecoration(getResources().getDrawable(R.drawable.divider_vertical)));
        mVehicleListView.setLayoutManager(new WrappingRecyclerViewLayoutManager(getActivity()));
        mVehicleListView.setAdapter(mAdapter);
        mPresenter = new VehicleSelectionPresenter(this, (Reservation) getArguments().getSerializable(EXTRA_RESERVATION));
        mPresenter.startEventListening();
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.select_vehicle)
                .setView(content)
                .setNegativeButton(R.string.dialog_cancel, null)
                .create();
    }

    @Override
    public void drawVehicleList(List<GenericListItem> vehicleList) {
        mAdapter.setItems(vehicleList);
        if (vehicleList.isEmpty()) {
            mVehicleListViewEmptyContainer.setVisibility(View.VISIBLE);
        } else {
            mVehicleListViewEmptyContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void setStateLoadComplete() {
        mProgressBar.setVisibility(View.GONE);
        mEmptyMessageTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setStateLoading() {
        mVehicleListViewEmptyContainer.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        mEmptyMessageTextView.setVisibility(View.GONE);
    }

    @Override
    public void close() {
        getDialog().dismiss();
    }

    @Override
    public void onDestroy() {
        mPresenter.stopEventListening();
        super.onDestroy();
    }
}