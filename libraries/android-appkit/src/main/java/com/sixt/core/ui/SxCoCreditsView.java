package com.sixt.core.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sixt.sixtappkit.R;

public class SxCoCreditsView extends RelativeLayout {

    private TextView mLeftContentTextView;
    private TextView mRightContentTextView;
    private ImageView mIconImageView;

    public SxCoCreditsView(Context context) {
        super(context);
        init();
    }

    public SxCoCreditsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SxCoCreditsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.credits_view, this);

        this.setPadding(0, 80, 0, 80);

        mLeftContentTextView = (TextView) findViewById(R.id.creditsLeftContainerTextView);
        mRightContentTextView = (TextView) findViewById(R.id.creditsRightContainerTextView);
        mIconImageView = (ImageView) findViewById(R.id.creditsLauncherImageView);
    }

    public void setLeftContent(String[] leftContent) {
        mLeftContentTextView.setText(getFormattedString(leftContent));
    }

    public void setRightContent(String[] rightContent) {
        mRightContentTextView.setText(getFormattedString(rightContent));
    }

    public void setImageSrc(Drawable imageDrawable) {
        mIconImageView.setImageDrawable(imageDrawable);
    }

    private String getFormattedString(String[] content) {
        String contentFormatted = "";
        for (String item:content) {
            contentFormatted += item + "\n";
        }
        return contentFormatted.substring(0, contentFormatted.length()-1);
    }
}
