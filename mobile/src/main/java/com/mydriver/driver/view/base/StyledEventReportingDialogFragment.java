package com.mydriver.driver.view.base;


import android.content.res.Resources;
import android.view.View;
import android.widget.TextView;

import com.mydriver.driver.R;
import com.sixt.core.eventbus.ui.SxEventReportingDialogFragment;

public class StyledEventReportingDialogFragment extends SxEventReportingDialogFragment {

    @Override
    public void onStart() {
        super.onStart();
        final Resources res = getResources();
        final int yellow = res.getColor(R.color.MyDriver_Design_yellow);

        final int titleId = res.getIdentifier("alertTitle", "id", "android");
        final View title = getDialog().findViewById(titleId);
        if (title != null) {
            ((TextView) title).setTextColor(yellow);
        }

        final int titleDividerId = res.getIdentifier("titleDivider", "id", "android");
        final View titleDivider = getDialog().findViewById(titleDividerId);
        if (titleDivider != null) {
            titleDivider.setBackgroundColor(yellow);
        }
    }

}
