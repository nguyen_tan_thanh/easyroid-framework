package com.mydriver.driver.view.reservationlist;

import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.presenter.EventListeningPresenter;

import java.util.List;

import lombok.Getter;

class ReservationListTabPresenter extends EventListeningPresenter {

    private ReservationListTabView view;

    @Getter private boolean pushNotificationsEnabled = false;
    @Getter private boolean pushNotificationsToggleMenuItemEnabled = true;
    @Getter private int unseenReservations = 0;

    public ReservationListTabPresenter(ReservationListTabView reservationListTabView) {
        this.view = reservationListTabView;
    }

    public void start() {
        startEventListening();
        loadReservationListIfNecessary();
    }

    private void loadReservationListIfNecessary() {
        ReservationsAcceptedUpdatedEvent event = SxEventBus.getSticky(ReservationsAcceptedUpdatedEvent.class);
        if (event != null) {
            return;
        }
        post(new ReservationsAllDoUpdateEvent(true));
    }

    public void stop() {
        stopEventListening();
    }

    public void onEvent(ReservationsRequestedUpdatedEvent event) {
        int unseenReservationsAfterUpdate = calculateUnseenReservations(event.getReservations());
        if (unseenReservationsAfterUpdate != unseenReservations) {
            unseenReservations = unseenReservationsAfterUpdate;
            view.updateUnseenReservationCount(unseenReservations);
        }
    }

    public void onEvent(PushNotificationActivationChangedEvent event) {
        pushNotificationsEnabled = event.isActive();
        pushNotificationsToggleMenuItemEnabled = true;
        view.updatePushNotificationStatus();
    }

    public void switchPushNotificationsEnabled() {
        post(new PushNotificationActivationDoChangeEvent(!pushNotificationsEnabled));
        pushNotificationsToggleMenuItemEnabled = false;
    }

    private int calculateUnseenReservations(List<Reservation> reservations) {
        int seenReservations = 0;
        for (Reservation reservation : reservations) {
            if (reservation.isSeen()) {
                seenReservations++;
            }
        }
        return reservations.size() - seenReservations;
    }
}