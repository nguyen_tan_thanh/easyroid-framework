package com.mydriver.driver.controller;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.ShowFeedbackMessageEvent;
import com.mydriver.driver.pushnotification.PushNotificationManager;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.model.FeedbackMessage;
import com.sixt.mydriver.model.SxMdDriver;

public class PushNotificationPluginController extends SxPluginController {

    private final PushNotificationManager pushNotificationManager;
    private final DriverPreferences driverPreferences;
    private SxMdDriver driver;

    private final PushNotificationManager.OnPushNotificationRequestReturnedListener listener = new PushNotificationManager.OnPushNotificationRequestReturnedListener() {
        @Override
        public void onSuccess(boolean active) {
            driverPreferences.setPushNotificationsEnabled(active);
            postSticky(new PushNotificationActivationChangedEvent(active));
            FeedbackMessage feedbackMessage;
            if (active) {
                feedbackMessage = new FeedbackMessage(FeedbackMessage.Type.TOAST, MainApplication.getStringResource(R.string.notifications_enabled));
            } else {
                feedbackMessage = new FeedbackMessage(FeedbackMessage.Type.TOAST, MainApplication.getStringResource(R.string.notifications_disabled));
            }
            post(new ShowFeedbackMessageEvent(feedbackMessage));
        }

        @Override
        public void onFailure(Throwable error) {
            postSticky(new PushNotificationActivationChangedEvent(driverPreferences.getPushNotificationsEnabled()));
        }
    };

    public PushNotificationPluginController(DriverPreferences driverPreferences, PushNotificationManager pushNotificationManager) {
        this.pushNotificationManager = pushNotificationManager;
        this.driverPreferences = driverPreferences;
    }

    public void onEvent(DriverUpdatedEvent event) {
        driver = event.getDriver();
        if (driver != null && driverPreferences.getPushNotificationsEnabled()) {
            pushNotificationManager.subscribeForPushService(event.getDriver(), listener);
        }
        else if (driver == null) {
            pushNotificationManager.unsubscribeForPushService(listener);
        }
    }

    public void onEvent(PushNotificationActivationDoChangeEvent event) {
        if (event.shouldActivate()) {
            pushNotificationManager.subscribeForPushService(driver, listener);
        } else {
            pushNotificationManager.unsubscribeForPushService(listener);
        }
    }
}
