package com.mydriver.driver.view.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sixt.core.eventbus.ui.SxEventReportingFragment;

import butterknife.ButterKnife;

public abstract class ButterKnifeEventReportingFragment extends SxEventReportingFragment {

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
