package com.mydriver.driver.view.reservationlist.tabs;

import java.util.ArrayList;

interface ReservationListView {
    public void updateReservationsList(ArrayList<ReservationListItemModel> reservations);
}
