package com.mydriver.driver.view.statistic.list;


import com.mydriver.driver.event.actionshow.ShowReservationDetailsEvent;
import com.mydriver.driver.view.reservationlist.tabs.ReservationListItemModel;
import com.mydriver.driver.view.statistic.model.StatisticsOverviewItemModel;
import com.sixt.core.eventbus.SxEventBus;

import java.util.ArrayList;
import java.util.List;

class StatisticsReservationListPresenter {

    private final StatisticsOverviewItemModel statisticsItem;
    private final List<ReservationListItemModel> listItems = new ArrayList<>();

    public StatisticsReservationListPresenter(StatisticsOverviewItemModel statisticsItem) {
        this.statisticsItem = statisticsItem;
        for (Reservation reservation : statisticsItem.getReservations()) {
            listItems.add(new ReservationListItemModel(reservation));
        }
    }

    public void start(StatisticsReservationListView view) {
        view.updateReservationsList(listItems);
        view.updateHeader(statisticsItem);
    }

    public void onItemSelected(int position) {
        SxEventBus.postEvent(new ShowReservationDetailsEvent(statisticsItem.getReservations().get(position)));
    }
}
