package com.mydriver.driver.pushnotification;


import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.mydriver.driver.MainActivity;
import com.parse.ParsePushBroadcastReceiver;

public class PushNotificationReceiver extends ParsePushBroadcastReceiver {

    private static final String EXTRA_PARSE_DATA = "com.parse.Data";

    @Override
    public void onPushOpen(Context context, Intent intent) {
        String parseJsonData = intent.getStringExtra(EXTRA_PARSE_DATA);
        PushNotificationData data = new Gson().fromJson(parseJsonData, PushNotificationData.class);
        context.startActivity(MainActivity.newIntent(data));
    }
}
