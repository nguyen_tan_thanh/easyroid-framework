package com.sixt.core.google.maps.geocode;


import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.sixt.core.google.maps.GMapsApiGsonSpiceService;
import com.sixt.core.google.maps.geocode.model.GMapsAddressPredictionList;
import com.sixt.core.webservice.manager.SxBaseResponseReceivedListener;
import com.sixt.core.webservice.manager.SxBaseRobospiceManager;
import com.sixt.core.webservice.manager.SxRobospiceRequestListener;
import com.sixt.core.webservice.request.SxBaseRequest;

public class GMapsGeoCodeManager extends SxBaseRobospiceManager {

    public interface OnAddressReceivedListener extends SxBaseResponseReceivedListener<GMapsAddressPredictionList> {}

    private SxBaseRequest lastRequest;

    public GMapsGeoCodeManager() {
        super(GMapsApiGsonSpiceService.class);
    }

    public void getAddressPredictions(Context context, String address, String language, final OnAddressReceivedListener listener) {
        cancelEventuallyRunningRouteRequest();
        lastRequest = new GMapsAddressPredictionRequest(address, language);
        RequestListener<GMapsAddressPredictionList> requestListener = new ResponseListener(listener);
        super.executeRequest(context, lastRequest, requestListener);
    }

    public void getAddressPredictionsForRegion(Context context, String address, String language, String region, final OnAddressReceivedListener listener) {
        cancelEventuallyRunningRouteRequest();
        lastRequest = new GMapsAddressPredictionRequest(address, language, region);
        RequestListener<GMapsAddressPredictionList> requestListener = new ResponseListener(listener);
        super.executeRequest(context, lastRequest, requestListener);
    }

    public void getAddressForLocation(Context context, LatLng location, String language, final OnAddressReceivedListener listener) {
        cancelEventuallyRunningRouteRequest();
        lastRequest = new GMapsAddressSearchRequest(location, language);
        RequestListener<GMapsAddressPredictionList> requestListener = new ResponseListener(listener);
        super.executeRequest(context, lastRequest, requestListener);
    }

    private void cancelEventuallyRunningRouteRequest() {
        if (lastRequest == null || lastRequest.isCancelled()) {
            return;
        }
        lastRequest.cancel();
        lastRequest = null;
    }

    private class ResponseListener extends SxRobospiceRequestListener<GMapsAddressPredictionList> {
        public ResponseListener(SxBaseResponseReceivedListener<GMapsAddressPredictionList> listener) {
            super(listener);
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            lastRequest = null;
            super.onRequestFailure(spiceException);
        }

        @Override
        public void onRequestSuccess(GMapsAddressPredictionList result) {
            lastRequest = null;
            super.onRequestSuccess(result);
        }
    }
}
