package com.sixt.core.eventbus.ui;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.sixt.core.eventbus.SxBackStackUtils;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.SxEventBusRegistry;
import com.sixt.core.eventbus.event.AttachFragmentEvent;
import com.sixt.core.eventbus.event.BringFragmentToFrontEvent;
import com.sixt.core.eventbus.event.ClearBackStackFragmentsEvent;
import com.sixt.core.eventbus.event.DetachFragmentEvent;
import com.sixt.core.eventbus.event.TransactionFragmentEvent;

/**
 * NOTE: Since the eventbus based architecture only supports one instance of EventReportingActivity there should also exist one
 * extension of SxEventBasedFragmentActivity per application.
 */
public abstract class SxEventBasedFragmentActivity extends SxEventReportingActivity implements SxEventBusRegistry.EventBusSubscriber {

    protected String mEmptyBackStackActionBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                updateActionBar();
            }
        });

        SxEventBusRegistry.getInstance().registerSubscriber(this);
    }

    @Override
    protected void onDestroy() {
        SxEventBusRegistry.getInstance().unregisterSubscriber(this);
        super.onDestroy();
    }

    @Override
    public Object register(SxEventBus eventBus) {
        eventBus.registerSticky(this);
        return this;
    }

    @Override
    public void unregister(SxEventBus eventBus) {
        eventBus.unregister(this);
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(AttachFragmentEvent event) {
        Fragment fragment = event.getFragment();
        if (fragment instanceof DialogFragment) {
            openDialog((DialogFragment) fragment);
        } else {
            attachFragment(event);
        }
    }

    private void openDialog(DialogFragment dialog) {
        dialog.show(getSupportFragmentManager(), ((Object) dialog).getClass().getSimpleName());
    }

    private void attachFragment(AttachFragmentEvent event) {
        int containerId = getContainerId(event);
        if (!event.isAddToBackStack()) {
            clearBackStackFragments();
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (event.isAddToBackStack()) {
            transaction.addToBackStack(event.getFragmentClass().getName());
            transaction.setBreadCrumbTitle(event.getActionBarTitle());
        } else if (event.getActionBarTitle() != null) {
            mEmptyBackStackActionBarTitle = event.getActionBarTitle();
            getSupportActionBar().setTitle(event.getActionBarTitle());
        }
        transaction.setCustomAnimations(event.getEnterAnimation(), event.getExitAnimation(), event.getPopEnterAnimation(), event.getPopExitAnimation());
        transaction.replace(containerId, event.getFragment(), event.getFragmentClass().getName());
        transaction.commit();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(DetachFragmentEvent event) {
        if (!isFragmentAttached(event)) {
            return;
        }

        Fragment fragment = getFragment(event);
        String tag = getFragmentTag(event);
        removeFragment(fragment, tag);
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(BringFragmentToFrontEvent event) {
        SxBackStackUtils.bringFragmentToFront(getSupportFragmentManager(), event.getFragmentClass().getName(), getContainerId(event));
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(ClearBackStackFragmentsEvent event) {
        clearBackStackFragments();
    }

    protected abstract int getContainerId(TransactionFragmentEvent event);

    private void clearBackStackFragments() {
        FragmentManager fm = getSupportFragmentManager();
        while (fm.getBackStackEntryCount() != 0) {
            FragmentManager.BackStackEntry entry = fm.getBackStackEntryAt(0);
            Fragment fragment = fm.findFragmentByTag(entry.getName());
            if (fragment != null) {
                removeFragment(fragment, fragment.getTag());
            }
        }
    }

    private Fragment getFragment(DetachFragmentEvent event) {
        String tag = getFragmentTag(event);
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    private String getFragmentTag(DetachFragmentEvent event) {
        return event.getFragmentClass().getName();
    }

    private boolean isFragmentAttached(DetachFragmentEvent event) {
        Fragment fragment = getFragment(event);
        return fragment != null;
    }

    private void removeFragment(Fragment fragment, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(fragment);
        transaction.commitAllowingStateLoss();
        getSupportFragmentManager().popBackStackImmediate(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    protected boolean isBackStackEmpty() {
        FragmentManager fm = getSupportFragmentManager();
        return fm.getBackStackEntryCount() == 0;
    }

    protected void updateActionBar() {
        if (isBackStackEmpty()) {
            getSupportActionBar().setTitle(mEmptyBackStackActionBarTitle);
        } else {
            getSupportActionBar().setTitle(SxBackStackUtils.getBackStackCurrentItemTitle(getSupportFragmentManager()));
        }
        getSupportActionBar().invalidateOptionsMenu();
    }
}