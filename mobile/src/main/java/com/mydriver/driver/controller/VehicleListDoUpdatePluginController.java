package com.mydriver.driver.controller;

import com.mydriver.driver.MainApplication;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdVehicleManager;
import com.sixt.mydriver.model.SxMdVehicleList;

public class VehicleListDoUpdatePluginController extends SxPluginController {

    private final SxMdVehicleManager vehicleManager;

    private SxMdVehicleManager.OnVehicleListReceivedListener mVehicleListListener = new SxMdVehicleManager.OnVehicleListReceivedListener() {
        @Override
        public void onFailure(Throwable error) {
            //TODO Implement failure handling
        }

        @Override
        public void onSuccess(SxMdVehicleList result) {
            onVehiclesListReceived(result);
        }
    };

    public VehicleListDoUpdatePluginController(SxMdVehicleManager manager) {
        this.vehicleManager = manager;
    }

    private void onVehiclesListReceived(SxMdVehicleList result) {
        post(new VehicleListUpdatedEvent(result));
    }

    public void onEvent(VehicleListDoUpdateEvent event) {
        vehicleManager.getVehicleList(MainApplication.getInstance(), event.getReservation().getReservationId(), mVehicleListListener);
    }
}
