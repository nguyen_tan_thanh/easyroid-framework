package com.mydriver.driver.view.base;

import lombok.Data;

@Data
public class GenericListItem {

    public final static int NO_ICON_RES_ID = -1;

    private final String title;

    private int iconResourceId;
    private String subTitle;
    private int actionIconResourceId = NO_ICON_RES_ID;
    private Object actionEvent;
    private Object selectionEvent;
    private boolean selected;

    public GenericListItem(String title, int iconResourceId) {
        this.title = title;
        this.iconResourceId = iconResourceId;
    }

    public GenericListItem(String title) {
        this.title = title;
        this.iconResourceId = NO_ICON_RES_ID;
    }
}
