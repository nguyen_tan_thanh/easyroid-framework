package com.mydriver.driver.controller;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.mydriver.driver.event.actionshow.ShowEnvironmentSwitchDialogEvent;
import com.sixt.core.app.SxAppConst;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.core.app.SxAppEnvironmentManager;

public class EnvironmentSwitchPluginController extends SxPluginController {
    private String[] environments = {"Demo", "Test", "Staging", "Production"};
    private AlertDialog mDialog;

    public void onEvent(ShowEnvironmentSwitchDialogEvent event) {
        openEnvironmentSwitchDialog(event.getContext());
    }

    public void onEvent(EnvironmentUpdatedEvent event) {
        SxAppEnvironmentManager.setMyDriverAppEnvironment(event.getEnvironment());
    }

    private void openEnvironmentSwitchDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Switch Environment");
        builder.setSingleChoiceItems(environments, getSelectedEnvironmentPosition(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        postSticky(new EnvironmentUpdatedEvent(SxAppConst.SxMdAppEnvironment.DEMO));
                        break;
                    case 1:
                        postSticky(new EnvironmentUpdatedEvent(SxAppConst.SxMdAppEnvironment.TEST));
                        break;
                    case 2:
                        postSticky(new EnvironmentUpdatedEvent(SxAppConst.SxMdAppEnvironment.STAGING));
                        break;
                    case 3:
                        postSticky(new EnvironmentUpdatedEvent(SxAppConst.SxMdAppEnvironment.PRODUCTION));
                        break;
                }
                mDialog.dismiss();
            }
        });

        mDialog = builder.create();
        mDialog.show();
    }

    private int getSelectedEnvironmentPosition() {
        SxAppConst.SxMdAppEnvironment environment = SxAppEnvironmentManager.getMyDriverAppEnvironment();

        for (int i = 0; i < environments.length; i++) {
            if (environments[i].equalsIgnoreCase(environment.name())) {
                return i;
            }
        }
        return 3;
    }
}
