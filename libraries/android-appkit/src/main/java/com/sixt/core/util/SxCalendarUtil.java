package com.sixt.core.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class SxCalendarUtil {

    public static boolean isSameDate(Calendar day1, Calendar day2) {
        return day1 != null && day2 != null &&
                day2.get(Calendar.DAY_OF_MONTH) == day1.get(Calendar.DAY_OF_MONTH) &&
                day2.get(Calendar.MONTH) == day1.get(Calendar.MONTH) &&
                day2.get(Calendar.YEAR) == day1.get(Calendar.YEAR);
    }

    public static boolean isBefore(Calendar day1, Calendar day2) {
        return day1 != null && day2 != null &&
                new GregorianCalendar(day2.get(Calendar.YEAR), day2.get(Calendar.MONTH), day2.get(Calendar.DAY_OF_MONTH)).getTimeInMillis() >
                new GregorianCalendar(day1.get(Calendar.YEAR), day1.get(Calendar.MONTH), day1.get(Calendar.DAY_OF_MONTH)).getTimeInMillis();
    }

    public static boolean isAfter(Calendar day1, Calendar day2) {
        return day1 != null && day2 != null &&
                new GregorianCalendar(day2.get(Calendar.YEAR), day2.get(Calendar.MONTH), day2.get(Calendar.DAY_OF_MONTH)).getTimeInMillis() <
                new GregorianCalendar(day1.get(Calendar.YEAR), day1.get(Calendar.MONTH), day1.get(Calendar.DAY_OF_MONTH)).getTimeInMillis();

    }

    public static Calendar round30Minutes(Calendar calendar) {
        int uMinutes = calendar.get(Calendar.MINUTE);
        int mod = uMinutes % 30;
        calendar.add(Calendar.MINUTE, mod < 15 ? -mod : (30-mod));
        return calendar;
    }

    public static Calendar getCurrentTime() {
        Calendar currentCalendar = Calendar.getInstance();
        return currentCalendar;
    }

    public static Calendar getIntervalUntilAcceptMyDriverReservation() {
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.add(Calendar.SECOND, 60*30);
        return currentCalendar;
    }
}