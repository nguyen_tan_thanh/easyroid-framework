package com.sixt.core.google.maps.geocode.model;

import com.google.android.gms.maps.model.LatLng;

import lombok.Data;

@Data
public class GMapsGeometry {
    private LatLng location;
}
