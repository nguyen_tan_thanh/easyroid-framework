package com.mydriver.driver;

import android.annotation.SuppressLint;
import android.os.Bundle;

import com.mydriver.driver.BuildConfig;
import com.sixt.core.eventbus.ui.SxEventBasedFragmentActivity;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

@SuppressLint("Registered")
public abstract class HockeyActivity extends SxEventBasedFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkForUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
    }

    private void checkForCrashes() {
        if (BuildConfig.HOCKEY_ENABLED) {
            CrashManager.register(this, BuildConfig.HOCKEY_APP_ID);
        }
    }

    private void checkForUpdates() {
        if (BuildConfig.HOCKEY_ENABLED) {
            UpdateManager.register(this, BuildConfig.HOCKEY_APP_ID);
        }
    }
}
