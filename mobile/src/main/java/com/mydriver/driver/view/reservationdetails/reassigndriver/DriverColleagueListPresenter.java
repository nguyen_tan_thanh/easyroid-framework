package com.mydriver.driver.view.reservationdetails.reassigndriver;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.ShowFeedbackMessageEvent;
import com.mydriver.driver.view.base.GenericListItem;
import com.sixt.core.eventbus.presenter.EventListeningPresenter;
import com.sixt.mydriver.model.FeedbackMessage;
import com.sixt.mydriver.model.SxMdDriver;
import com.sixt.mydriver.model.SxMdDriverList;

import java.util.ArrayList;
import java.util.List;

public class DriverColleagueListPresenter extends EventListeningPresenter {
    private DriverColleagueListView view;
    private SxMdDriverList driverColleagues;
    private Reservation reservation;

    public DriverColleagueListPresenter(Reservation reservation) {
        this.reservation = reservation;
    }

    public void start(DriverColleagueListView view) {
        this.view = view;
        startEventListening();
    }

    public void stop() {
        stopEventListening();
        this.view = null;
    }

    public void onEvent(DriverColleagueListUpdatedEvent event) {
        drawDriverColleagues(event.getDriverColleagues());
    }

    private void drawDriverColleagues(SxMdDriverList driverColleagues) {

        this.view.hideProgressBar();

        if ( null == driverColleagues || driverColleagues.isEmpty() ) {
            FeedbackMessage message = new FeedbackMessage(FeedbackMessage.Type.ERROR, MainApplication.getStringResource(R.string.assign_reservation_title_no_available_driver));
            post(new ShowFeedbackMessageEvent(message));

            return;
        }

        this.driverColleagues = driverColleagues;

        List<GenericListItem> items = new ArrayList<>();
        for (SxMdDriver driver : driverColleagues) {
            String fullName = driver.getFirstName() + " " + driver.getLastName();
            GenericListItem item = new GenericListItem(fullName);
            item.setSubTitle(driver.getMobile());
            item.setIconResourceId(R.drawable.ico_driver_black);
            item.setActionIconResourceId(R.drawable.ico_phone_selector);
            item.setActionEvent(new CallHotlineEvent(driver.getMobile()));

            items.add(item);
        }

        this.view.drawDriverColleagues(items);
    }

    public void doSelectItemAtPosition(Context context, int position) {
        final SxMdDriver driver = this.driverColleagues.get(position);

        String fullName = driver.getFirstName() + " " + driver.getLastName();
        String confirmMsg = MainApplication.getStringResource(R.string.assign_reservation_confirm, fullName);

        new AlertDialog.Builder(context)
                .setMessage(confirmMsg)
                .setPositiveButton(R.string.dialog_accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        post(new ReservationReassignDriverEvent(reservation, driver.getDriverId()));
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create().show();

    }
}
