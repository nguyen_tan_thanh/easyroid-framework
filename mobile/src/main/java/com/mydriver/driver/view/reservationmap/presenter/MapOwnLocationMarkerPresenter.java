package com.mydriver.driver.view.reservationmap.presenter;


import android.location.Location;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mydriver.driver.R;
import com.mydriver.driver.view.reservationmap.ReservationMapView;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.presenter.EventListeningChildPresenter;

class MapOwnLocationMarkerPresenter extends EventListeningChildPresenter<ReservationMapView> {

    private Marker ownLocationMarker;

    @Override
    public void start(ReservationMapView reservationMapView) {
        super.start(reservationMapView);
        SxEventBus.postEvent(new LocationDoUpdateEvent());
    }

    private void updateLocationMarker(Location location) {
        if (location == null) {
            hideLocationMarkerIfShown();
            return;
        }
        if (ownLocationMarker == null) {
            createNewLocationMarker(location);
            return;
        }
        ownLocationMarker.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
    }

    private void createNewLocationMarker(Location location) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(location.getLatitude(), location.getLongitude()));
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ico_map_marker_own_position));
        ownLocationMarker = view.addMarker(markerOptions);
    }

    public void hideLocationMarkerIfShown() {
        if (ownLocationMarker == null) {
            return;
        }
        ownLocationMarker.remove();
    }

    public void onEvent(LocationUpdatedEvent event) {
        updateLocationMarker(event.getLocation());
    }
}
