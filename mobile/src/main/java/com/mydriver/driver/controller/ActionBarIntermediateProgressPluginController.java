package com.mydriver.driver.controller;


import com.sixt.core.eventbus.plugincontroller.SxPluginController;

public class ActionBarIntermediateProgressPluginController extends SxPluginController {

    private boolean isPushNotificationActivationLoading;

    public void onEvent(PushNotificationActivationDoChangeEvent event) {
        isPushNotificationActivationLoading = true;
        postRequestRunningUpdate();
    }

    public void onEvent(PushNotificationActivationChangedEvent event) {
        isPushNotificationActivationLoading = false;
        postRequestRunningUpdate();
    }

    private void postRequestRunningUpdate(){
        post(new ActionBarLoadingIndicatorUpdatedEvent(getRequestRunning()));
    }

    private boolean getRequestRunning(){
        return isPushNotificationActivationLoading;
    }
}
