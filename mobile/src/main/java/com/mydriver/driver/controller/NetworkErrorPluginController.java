package com.mydriver.driver.controller;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.ShowFeedbackMessageEvent;
import com.octo.android.robospice.exception.NoNetworkException;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.model.FeedbackMessage;
import com.sixt.mydriver.service.SxMdServerSideException;
import com.sixt.mydriver.service.SxMdUnauthorizedException;

public class NetworkErrorPluginController extends SxPluginController {

    public void onEvent(NetworkErrorEvent event) {
        if (event.getError() instanceof NoNetworkException || event.getError().getCause() instanceof NoNetworkException) {
            showNetworkErrorMessage();
            return;
        }
        if (event.getError() instanceof SxMdServerSideException) {
            processServerError((SxMdServerSideException) event.getError());
            return;
        }
        if (event.getError() instanceof SxMdUnauthorizedException) {
            post(new DriverDoLogoutEvent());
            showUnauthorizedErrorMessage();
            return;
        }
        showUnknownErrorMessage(event.getError());
    }

    private void showNetworkErrorMessage() {
        String messageText = MainApplication.getStringResource(R.string.no_connection_error_message);
        FeedbackMessage message = new FeedbackMessage(FeedbackMessage.Type.ERROR, messageText);
        post(new ShowFeedbackMessageEvent(message));
    }

    private void processServerError(SxMdServerSideException error) {
        String messageText = error.getServerError().getErrorMessage();
        FeedbackMessage message = new FeedbackMessage(FeedbackMessage.Type.ERROR, messageText);
        post(new ShowFeedbackMessageEvent(message));
    }

    private void showUnknownErrorMessage(Throwable error) {
        String messageText = error.getLocalizedMessage();
        FeedbackMessage message = new FeedbackMessage(FeedbackMessage.Type.ERROR, messageText);
        post(new ShowFeedbackMessageEvent(message));
    }

    private void showUnauthorizedErrorMessage() {
        String messageText = MainApplication.getStringResource(R.string.session_expired_error_message);
        FeedbackMessage message = new FeedbackMessage(FeedbackMessage.Type.ERROR, messageText);
        postDelayed(new ShowFeedbackMessageEvent(message), 1000);
    }
}
