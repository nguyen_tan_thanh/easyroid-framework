package com.mydriver.driver.view.leftmenu;

import com.mydriver.driver.view.base.GenericListItem;
import com.sixt.core.app.SxAppConst;

import java.util.List;

interface LeftMenuView {
    void drawMenuItems(List<GenericListItem> leftMenuItems);
    void drawVersion(String version);
    void updateMenuItems();
    void updateEnvironmentInformation(SxAppConst.SxMdAppEnvironment event);
}

