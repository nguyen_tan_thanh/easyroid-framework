package com.sixt.core.ui;

import android.animation.LayoutTransition;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.util.Calendar;

public class SxCoCreditsScrollView extends ScrollView {
    private static final int OVERSCROLL_MAX_COUNT = 5;
    private static final int SCROLL_MIN_DISTANCE_TO_HIDE = 500;
    private static final int MIN_TIME_IN_MILLIS_BETWEEN_SCROLLS = 2000;

    private boolean mTouch = false;
    private int mTopOverScrollCounter = 0;
    private long mLastTopOverScrollEvent = 0;
    private SxCoCreditsView mCreditsView;
    private boolean mCreditsViewAdded = false;

    public SxCoCreditsScrollView(Context context) {
        super(context);
    }

    public SxCoCreditsScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SxCoCreditsScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private void addLinearLayoutIfNeeded() {
        View scrollViewChild = this.getChildAt(0);
        if (scrollViewChild instanceof LinearLayout) {
            return;
        }

        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linearLayout.setLayoutTransition(new LayoutTransition());
        this.removeView(scrollViewChild);
        linearLayout.addView(scrollViewChild);
        this.addView(linearLayout);
    }

    public void enableCreditsData(String[] content, Drawable imageDrawable) {
        if (imageDrawable != null) {
            getCreditsView().setImageSrc(imageDrawable);
        }
        enableCreditsData(content);
    }

    public void enableCreditsData(String[] content) {
        String[] leftContent;
        String[] rightContent;
        int midlleIdx = (int) content.length / 2;

        if (content.length % 2 != 0) {
            midlleIdx += 1;
            rightContent = new String[midlleIdx -1];
        } else {
            rightContent = new String[midlleIdx];
        }
        leftContent = new String[midlleIdx];
        for (int i=0; i < content.length; i++) {
            if (i < midlleIdx) {
                leftContent[i] = content[i];
            } else {
                rightContent[i - midlleIdx] = content[i];
            }
        }
        getCreditsView().setLeftContent(leftContent);
        getCreditsView().setRightContent(rightContent);
        addLinearLayoutIfNeeded();
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN || ev.getAction() == MotionEvent.ACTION_UP) {
            mTouch = true;
        }
        return super.onTouchEvent(ev);
    }

    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);

        if (clampedY && scrollY == 0 && mTouch) {
            if (Calendar.getInstance().getTimeInMillis() - mLastTopOverScrollEvent < MIN_TIME_IN_MILLIS_BETWEEN_SCROLLS) {
                mTopOverScrollCounter++;
            } else {
                mTopOverScrollCounter = 1;
            }
            if (mTopOverScrollCounter > OVERSCROLL_MAX_COUNT) {
                mTopOverScrollCounter = 1;
                addCreditsView();
            }
            mLastTopOverScrollEvent = Calendar.getInstance().getTimeInMillis();
            mTouch = false;

            return;
        }

        if (!clampedX && !clampedY && scrollY > SCROLL_MIN_DISTANCE_TO_HIDE) {
            removeCreditsView();
        }

        mTouch = false;
    }

    private void addCreditsView() {
        if (mCreditsViewAdded) {
            return;
        }
        mCreditsViewAdded = true;
        ((LinearLayout) this.getChildAt(0)).addView(getCreditsView(), 0);
    }

    private void removeCreditsView() {
        if (!mCreditsViewAdded) {
            return;
        }

        ((LinearLayout) this.getChildAt(0)).removeViewAt(0);
        mCreditsViewAdded = false;
    }

    public SxCoCreditsView getCreditsView() {
        if (mCreditsView == null) {
            mCreditsView = new SxCoCreditsView(getContext());
        }
        return mCreditsView;
    }
}
