package com.mydriver.driver.view.leftmenu;

import android.content.Context;

import com.mydriver.driver.BuildConfig;
import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.ShowEnvironmentSwitchDialogEvent;
import com.mydriver.driver.view.base.GenericListItem;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.presenter.EventListeningPresenter;
import com.sixt.mydriver.model.SxMdDriver;

import java.util.ArrayList;
import java.util.List;

class LeftMenuPresenter extends EventListeningPresenter {

    private final List<GenericListItem> menuItems = new ArrayList<>();
    private final String appVersion;
    private LeftMenuView view;

    public LeftMenuPresenter() {
        appVersion = getAppVersion();
    }

    public void start(LeftMenuView view) {
        this.view = view;
        menuItems.clear();
        menuItems.addAll(createMenuItems());
        startEventListening();
        view.drawMenuItems(menuItems);
        view.drawVersion(appVersion);
    }

    public void stop() {
        stopEventListening();
    }

    private String getAppVersion() {
        return MainApplication.getStringResource(R.string.app_version, BuildConfig.VERSION_NAME);
    }

    public List<GenericListItem> createMenuItems() {
        SxMdDriver driver = SxEventBus.getSticky(DriverUpdatedEvent.class).getDriver();
        if (driver == null) {
            throw new IllegalStateException("Driver should not be null");
        }
        List<GenericListItem> leftMenuItems = new ArrayList<>();
        leftMenuItems.add(LeftMenuListItemFactory.newShowProfileListItem(driver));
        leftMenuItems.add(LeftMenuListItemFactory.newShowRidesListItem());
        leftMenuItems.add(LeftMenuListItemFactory.newShowStatisticsListItem());
        return leftMenuItems;
    }

    public void logout() {
        SxEventBus.postEvent(new DriverDoLogoutEvent());
    }

    public void onEvent(EnvironmentUpdatedEvent event) {
        view.updateEnvironmentInformation(event.getEnvironment());
    }

    public void onMenuItemSelected(GenericListItem item) {
        SxEventBus.postEvent(new LeftMenuDoToggleEvent(LeftMenuDoToggleEvent.MenuState.CLOSE));
        SxEventBus.postEvent(item.getSelectionEvent());
        selectCurrentMenuItem(item);
    }

    private void selectCurrentMenuItem(GenericListItem newSelectedItem) {
        for (GenericListItem item : menuItems) {
            item.setSelected(false);
        }
        newSelectedItem.setSelected(true);
        view.updateMenuItems();
    }

    public void showEnvironmentSwitchDialog(Context context) {
        post(new ShowEnvironmentSwitchDialogEvent(context));
    }
}