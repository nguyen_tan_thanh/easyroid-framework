package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.event.actionshow.ShowHotlineSelectionDialogEvent;
import com.mydriver.driver.view.hotlineselection.HotlineSelectionDialogFragment;
import com.sixt.core.eventbus.plugincontroller.SxSingleFragmentPluginController;

public class ShowHotlineSelectionDialogPluginController extends SxSingleFragmentPluginController<ShowHotlineSelectionDialogEvent, HotlineSelectionDialogFragment> {

    public ShowHotlineSelectionDialogPluginController() {
        super(HotlineSelectionDialogFragment.class);
    }

    public void onEvent(ShowHotlineSelectionDialogEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldDisplayFragment(ShowHotlineSelectionDialogEvent ShowHotlineSelectionDialogEvent) {
        return true;
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return false;
    }

    @Override
    protected HotlineSelectionDialogFragment createFragment(ShowHotlineSelectionDialogEvent event) {
        return HotlineSelectionDialogFragment.newInstance(event.getHotlineNumberList());
    }
}
