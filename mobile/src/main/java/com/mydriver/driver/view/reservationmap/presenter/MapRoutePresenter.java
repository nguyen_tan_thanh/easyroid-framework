package com.mydriver.driver.view.reservationmap.presenter;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.view.reservationmap.ReservationMapView;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.presenter.EventListeningChildPresenter;
import com.sixt.core.google.maps.routes.model.GMapsRoute;

import java.util.List;

class MapRoutePresenter extends EventListeningChildPresenter<ReservationMapView> {

    private final Reservation reservation;
    private Polyline routePolyline;

    public MapRoutePresenter(Reservation reservation) {
        this.reservation = reservation;
    }

    @Override
    public void start(ReservationMapView reservationMapView) {
        super.start(reservationMapView);
        if (routePolyline == null) {
            SxEventBus.postEvent(new ReservationRouteDoLoadEvent(reservation));
        }
    }

    public void onEvent(ReservationRouteUpdatedEvent event) {
        drawRoute(event.getRoute());
    }

    private void drawRoute(GMapsRoute route) {
        if (routePolyline != null) {
            routePolyline.remove();
        }
        List<LatLng> points = getRoutePoints(route);
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.color(MainApplication.getInstance().getResources().getColor(R.color.MyDriver_Design_blue));
        polylineOptions.width(MainApplication.getInstance().getResources().getDimension(R.dimen.MyDriver_Map_Route_width));
        polylineOptions.addAll(points);
        routePolyline = view.addPolyline(polylineOptions);
    }

    public List<LatLng> getRoutePoints(GMapsRoute route) {
        List<LatLng> points = PolyUtil.decode(route.getOverviewPolyLine().getPoints());
        LatLng origin  = new LatLng(reservation.getOrigin().getLatitude(), reservation.getOrigin().getLongitude());
        LatLng destination  = new LatLng(reservation.getDestination().getLatitude(), reservation.getDestination().getLongitude());
        points.add(0, origin);
        points.add(destination);
        return points;
    }
}
