package com.sixt.core.app;


import com.sixt.core.webservice.client.SxHttpConnectionClient;
import com.sixt.mydriver.request.SxMdBaseRequest;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

public class SxAppEnvironmentManager {

    protected static SxAppConst.SxMdAppEnvironment sxMdAppEnvironment = SxAppConst.SxMdAppEnvironment.STAGING;

    protected static final String DRIVENOW_ENVIRONMENT_NEW_DOMAIN = "api.drive-now.com";
    protected static final String DRIVENOW_ENVIRONMENT_DOMAIN = "metrows.drive-now.com";
    protected static final String SIXT_RAC_ENVIRONMENT_DOMAIN = "app.sixt.de";
    protected static final String ETURN_ENVIRONMENT_DOMAIN = "webservices.sixt.com";

    public static void setMyDriverAppEnvironment(SxAppConst.SxMdAppEnvironment environment) {
        SxMdBaseRequest.setMyDriverHost(environment);
        sxMdAppEnvironment = environment;
    }

    public static SxAppConst.SxMdAppEnvironment getMyDriverAppEnvironment() {
        return sxMdAppEnvironment;
    }

    public static SxAppConst.SxMdAppEnvironment getMyDriverAppEnvironmentUrl() {
        return sxMdAppEnvironment;
    }

    public static void setDriveNowAppEnvironment(SxAppConst.SxAppEnvironment appEnvironment) {
        setCookieBasedAppEnvironment(appEnvironment, DRIVENOW_ENVIRONMENT_NEW_DOMAIN);
        setCookieBasedAppEnvironment(appEnvironment, DRIVENOW_ENVIRONMENT_DOMAIN);
    }

    public static void setSixtRacAppEnvironment(SxAppConst.SxAppEnvironment appEnvironment) {
        if (appEnvironment.equals(SxAppConst.SxAppEnvironment.DEV_CATALINA)) {
            setCookieBasedAppEnvironment(SxAppConst.SxAppEnvironment.DEVELOPMENT, SIXT_RAC_ENVIRONMENT_DOMAIN);
            SxHttpConnectionClient.setCookie("app.sixt.de", SxAppConst.SxAppEnvironment.DEV_CATALINA.getKey(), SxAppConst.SxAppEnvironment.DEV_CATALINA.getValue());
        } else if (appEnvironment.equals(SxAppConst.SxAppEnvironment.DEV_HUBER)) {
            setCookieBasedAppEnvironment(SxAppConst.SxAppEnvironment.DEVELOPMENT, SIXT_RAC_ENVIRONMENT_DOMAIN);
            SxHttpConnectionClient.setCookie("app.sixt.de", SxAppConst.SxAppEnvironment.DEV_HUBER.getKey(), SxAppConst.SxAppEnvironment.DEV_HUBER.getValue());
        } else {
            setCookieBasedAppEnvironment(appEnvironment, SIXT_RAC_ENVIRONMENT_DOMAIN);
        }
    }

    public static void setEturnAppEnvironment(SxAppConst.SxAppEnvironment appEnvironment) {
        setCookieBasedAppEnvironment(appEnvironment, ETURN_ENVIRONMENT_DOMAIN);
    }

    public static SxAppConst.SxAppEnvironment getDriveNowAppEnvironment() {
        return getCookieBasedAppEnvironment(DRIVENOW_ENVIRONMENT_NEW_DOMAIN);
    }

    public static SxAppConst.SxAppEnvironment getSixtRacAppEnvironment() {
        return getCookieBasedAppEnvironment(SIXT_RAC_ENVIRONMENT_DOMAIN);
    }

    public static SxAppConst.SxAppEnvironment getEturnAppEnvironment() {
        return getCookieBasedAppEnvironment(ETURN_ENVIRONMENT_DOMAIN);
    }

    static void setCookieBasedAppEnvironment(SxAppConst.SxAppEnvironment appEnvironment, String domain) {
        SxHttpConnectionClient.removeAllDomainCookies(domain);

        if (appEnvironment == null)
            return;

        if ( !appEnvironment.equals(SxAppConst.SxAppEnvironment.PRODUCTION) ) {
            SxHttpConnectionClient.setCookie(domain, appEnvironment.getKey(), appEnvironment.getValue());
        }
    }

    static SxAppConst.SxAppEnvironment getCookieBasedAppEnvironment(String domain) {
        List<SxAppConst.SxAppEnvironment> appEnvironments = Arrays.asList(SxAppConst.SxAppEnvironment.values());

        for (SxAppConst.SxAppEnvironment appEnvironment : appEnvironments) {
            HttpCookie environmentCookie = SxHttpConnectionClient.getCookie(domain, appEnvironment.getKey());
            if (environmentCookie != null) {
                return appEnvironment;
            }
        }

        return SxAppConst.SxAppEnvironment.PRODUCTION;
    }

}
