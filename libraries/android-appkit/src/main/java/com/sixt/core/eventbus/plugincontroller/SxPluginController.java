package com.sixt.core.eventbus.plugincontroller;

import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.SxEventBusRegistry;

import java.util.concurrent.ScheduledFuture;

public abstract class SxPluginController implements SxEventBusRegistry.EventBusSubscriber {

    private SxEventBus mEventBus;

    @Override
    public final Object register(SxEventBus eventBus) {
        mEventBus = eventBus;
        mEventBus.register(this);
        return this;
    }

    public final void unregister(SxEventBus eventBus) {
        eventBus.unregister(this);
        mEventBus = null;
    }

    protected void post(Object event) {
        if (mEventBus == null) {
            throw new NullPointerException("PluginController.register() was not called. Is the controller registered in the EventBusRegistry?");
        }
        mEventBus.post(event);
    }

    protected void postSticky(Object event) {
        if (mEventBus == null) {
            throw new NullPointerException("PluginController.register() was not called. Is the controller registered in the EventBusRegistry?");
        }
        mEventBus.postSticky(event);
    }

    protected <T> T removeStickyEvent(Class<T> eventType) {
        if (mEventBus == null) {
            throw new NullPointerException("PluginController.register() was not called. Is the controller registered in the EventBusRegistry?");
        }
        return mEventBus.removeStickyEvent(eventType);
    }

    protected boolean removeStickyEvent(Object event) {
        if (mEventBus == null) {
            throw new NullPointerException("PluginController.register() was not called. Is the controller registered in the EventBusRegistry?");
        }
        return mEventBus.removeStickyEvent(event);
    }

    protected <T> T getStickyEvent(Class<T> eventType) {
        if (mEventBus == null) {
            throw new NullPointerException("PluginController.register() was not called. Is the controller registered in the EventBusRegistry?");
        }
        return mEventBus.getStickyEvent(eventType);
    }

    protected ScheduledFuture postDelayed(Object event, long delay) {
        if (mEventBus == null) {
            throw new NullPointerException("PluginController.register() was not called. Is the controller registered in the EventBusRegistry?");
        }
        return mEventBus.postDelayed(event, delay);
    }
}