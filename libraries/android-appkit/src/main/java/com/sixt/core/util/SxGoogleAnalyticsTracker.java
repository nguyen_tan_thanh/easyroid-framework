package com.sixt.core.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.sixt.sixtappkit.BuildConfig;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class SxGoogleAnalyticsTracker {

    private static Context context;
    private static boolean isActivated = true;
    private static SxGoogleAnalyticsTracker instance;
    private static Tracker tracker;
    private static String trackingId;
    private static int sessionTimeoutInSeconds = 30;

    public static SxGoogleAnalyticsTracker getInstance(Context context) {
        if (instance == null) {
            instance = new SxGoogleAnalyticsTracker();
            SxGoogleAnalyticsTracker.context = context;
            createTracker();
        }
        return instance;
    }

    private static void createTracker() {
        if (tracker != null) {
            return;
        }

        if (BuildConfig.DEBUG) {
            return;
        }

        if (trackingId == null) {
            return;
        }

        tracker = GoogleAnalytics.getInstance(context).newTracker(trackingId);
        tracker.setSessionTimeout(sessionTimeoutInSeconds);
        SharedPreferences settings = context.getSharedPreferences(SxConst.PREFS_NAME, 0);
        boolean activated = settings.getBoolean(SxConst.GOOGLE_ANALYTICS_PREF_NAME, true);
        setActivated(activated);
    }

    private SxGoogleAnalyticsTracker() {}

    public static void setTrackingId(String tracking_id) {
        trackingId = tracking_id;

        if(tracker == null && instance != null) {
            createTracker();
        }
    }

    public static void setActivated(boolean activated) {
        SharedPreferences settings = context.getSharedPreferences(SxConst.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(SxConst.GOOGLE_ANALYTICS_PREF_NAME, activated);
        editor.apply();

        isActivated = activated;
    }

    public static boolean getActivated() {
        SharedPreferences sp = context.getSharedPreferences(SxConst.PREFS_NAME, 0);
        boolean gAnalyticsActive = sp.getBoolean(SxConst.GOOGLE_ANALYTICS_PREF_NAME, true);
        isActivated = gAnalyticsActive;
        return gAnalyticsActive;
    }

    public void trackTransaction(String transactionId, String productName, String SKU, String productCategory, Double productPrice, int productQuantity, String currencyCode) {
        sendTrackingMap(new HitBuilders.TransactionBuilder()
                .setTransactionId(transactionId)
                .setRevenue(productPrice)
                .setCurrencyCode(currencyCode)
                .build(), null);

        sendTrackingMap(new HitBuilders.ItemBuilder()
                .setTransactionId(transactionId)
                .setName(productName)
                .setSku(SKU)
                .setCategory(productCategory)
                .setPrice(productPrice)
                .setQuantity(productQuantity)
                .setCurrencyCode(currencyCode)
                .build(), null);
    }

    public void trackTransaction(String transactionId, String affiliation, double totalTax, double shippingCosts, String productName, String SKU, String productCategory, Double productPrice,
                                 int productQuantity,
                                 String currencyCode) {
        sendTrackingMap(new HitBuilders.TransactionBuilder()
                .setTransactionId(transactionId)
                .setAffiliation(affiliation)
                .setRevenue(productPrice)
                .setTax(totalTax)
                .setShipping(shippingCosts)
                .setCurrencyCode(currencyCode)
                .build(), null);

        sendTrackingMap(new HitBuilders.ItemBuilder()
                .setTransactionId(transactionId)
                .setName(productName)
                .setSku(SKU)
                .setCategory(productCategory)
                .setPrice(productPrice)
                .setQuantity(productQuantity)
                .setCurrencyCode(currencyCode)
                .build(), null);
    }

    /**
     * @Deprecated use {@link #trackTransaction(String, String, String, String, Double, int, String)}
     * SKU will be ignored
     */
    public void trackTransaction(String transactionId, String productName, String SKU, String productCategory, Double productPrice, long productQuantity, String currencyCode) {
            trackTransaction(transactionId, productName, SKU, productCategory, productPrice, (int) productQuantity, currencyCode);
    }
    /**
     * @Deprecated use {@link #trackTransaction(String, String, double, double, String, String, String, Double, int, String)}
     *
     */
    public void trackTransaction(String transactionId, String affiliation, double totalTax, double shippingCosts, String productName, String SKU, String productCategory, Double productPrice,
                                 long productQuantity,
                                 String currencyCode) {
            trackTransaction(transactionId, affiliation, totalTax, shippingCosts, productName, SKU, productCategory, productPrice, (int) productQuantity, currencyCode);
    }


    public void trackEventwithCategory(String category, String action, String label, Long value, String view) {
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder();
        builder.setCategory(category);
        builder.setAction(action);
        builder.setLabel(label);
        if(value != null) {
            builder.setValue(value);
        }
        sendTrackingMap(builder.build(), view);
    }

    public void trackEventWithCategory(String category, String action) {
        trackEventwithCategory(category, action, null, null, null);
    }

    public static void trackEvent(String category, String action, String label, Long value) {
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder();
        builder.setCategory(category);
        builder.setAction(action);
        if (StringUtils.isNotEmpty(label)) {
            builder.setLabel(label);
        }
        if (value != null) {
            builder.setValue(value);
        }
        tracker.send(builder.build());
    }

    public static void trackEvent(String category, String action, String label) {
        trackEvent(category, action, label, null);
    }

    private void sendTrackingMap(Map<String, String> map, String view) {
        if (tracker == null) {
            return;
        }

        if (!isActivated) {
            return;
        }

        tracker.setScreenName(view);
        tracker.send(map);
    }

    public void trackPageView(String page) {
        if (tracker == null) {
            return;
        }

        if (!isActivated) {
            return;
        }

        tracker.setScreenName(page);
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    public void trackTiming(String category, long millis, String name, String label) {
        sendTrackingMap(new HitBuilders.TimingBuilder()
                .setCategory(category)
                .setValue(millis)
                .setLabel(label)
                .setVariable(name).build(), null);
    }

    public static void enableDebugTrackingID(String trackingId, Context context){
        if (BuildConfig.DEBUG) {
            GoogleAnalytics.getInstance(SxGoogleAnalyticsTracker.context)
                    .getLogger()
                    .setLogLevel(Logger.LogLevel.VERBOSE);

            tracker = GoogleAnalytics.getInstance(SxGoogleAnalyticsTracker.context).newTracker(trackingId);
        }
    }

    public void setDispatchPeriod(int seconds) {
        GoogleAnalytics.getInstance(context).setLocalDispatchPeriod(seconds);
    }
    public void dispatchRemainingData() {
        if (isActivated) {
            GoogleAnalytics.getInstance(context).dispatchLocalHits();
        }
    }

    public static int getSessionTimeoutInSeconds() {
        return sessionTimeoutInSeconds;
    }

    public static void setSessionTimeoutInSeconds(int sessionTimeoutInSeconds) {
        if (tracker != null) {
            SxGoogleAnalyticsTracker.sessionTimeoutInSeconds = sessionTimeoutInSeconds;
            tracker.setSessionTimeout(SxGoogleAnalyticsTracker.sessionTimeoutInSeconds);
        }
    }
}