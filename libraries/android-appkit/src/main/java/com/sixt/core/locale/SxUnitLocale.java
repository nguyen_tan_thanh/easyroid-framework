package com.sixt.core.locale;


import java.util.Locale;

public class SxUnitLocale {
	public static SxUnitLocale Imperial = new SxUnitLocale();
    public static SxUnitLocale Metric = new SxUnitLocale();

    private static final float METER_TO_YARD = 1.0936133f;
    
    public static float meter2yards(float meters) {
    	return meters * METER_TO_YARD;
    }
    
    public static SxUnitLocale getCurrent()
    {
        String countryCode = Locale.getDefault().getCountry();
        if ("US".equals(countryCode)) return Imperial; // USA
        return Metric;
    }
}
