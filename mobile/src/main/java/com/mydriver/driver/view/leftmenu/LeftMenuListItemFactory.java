package com.mydriver.driver.view.leftmenu;


import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.ShowDriverProfileEvent;
import com.mydriver.driver.event.actionshow.ShowReservationListsEvent;
import com.mydriver.driver.event.actionshow.ShowStatisticsOverviewEvent;
import com.mydriver.driver.view.base.GenericListItem;
import com.sixt.mydriver.model.SxMdDriver;

class LeftMenuListItemFactory {

    private LeftMenuListItemFactory() {}

    public static GenericListItem newShowProfileListItem(SxMdDriver driver) {
        String title = MainApplication.getStringResource(R.string.menu_profile);
        GenericListItem item = new GenericListItem(title, R.drawable.ico_menu_driver_selector);
        item.setSubTitle(driver.getEmail());
        item.setSelectionEvent(new ShowDriverProfileEvent());
        return item;
    }

    public static GenericListItem newShowRidesListItem() {
        String title = MainApplication.getStringResource(R.string.menu_rides);
        GenericListItem item = new GenericListItem(title, R.drawable.ico_menu_rides_selector);
        item.setSelectionEvent(new ShowReservationListsEvent());
        item.setSelected(true);
        return item;
    }

    public static GenericListItem newShowStatisticsListItem() {
        String title = MainApplication.getStringResource(R.string.menu_history);
        GenericListItem item = new GenericListItem(title, R.drawable.ico_menu_history_selector);
        item.setSelectionEvent(new ShowStatisticsOverviewEvent());
        return item;
    }
}