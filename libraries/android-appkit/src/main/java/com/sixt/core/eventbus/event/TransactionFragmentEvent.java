package com.sixt.core.eventbus.event;


import android.support.v4.app.Fragment;

import lombok.Data;

@Data
public class TransactionFragmentEvent {

    private Class<? extends Fragment> fragmentClass;

    public TransactionFragmentEvent(Class<? extends Fragment> fragmentClass) {
        this.fragmentClass = fragmentClass;
    }
}
