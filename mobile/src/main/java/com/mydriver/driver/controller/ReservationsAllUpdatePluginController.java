package com.mydriver.driver.controller;

import com.mydriver.driver.MainApplication;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdReservationManager;
import com.sixt.mydriver.model.SxMdReservationList;

public class ReservationsAllUpdatePluginController extends SxPluginController {

    private boolean isRequestRunning;
    private final SxMdReservationManager mReservationManager;

    private SxMdReservationManager.OnReservationsListReceivedListener mReservationsLoadedListener = new SxMdReservationManager.OnReservationsListReceivedListener() {
        @Override
        public void onFailure(Throwable error) {
            onRequestFailure(error);
            isRequestRunning = false;
        }

        @Override
        public void onSuccess(SxMdReservationList result) {
            onReservationListReceived(result);
            isRequestRunning = false;
        }
    };

    public ReservationsAllUpdatePluginController(SxMdReservationManager mReservationManager) {
        this.mReservationManager = mReservationManager;
    }

    private void onRequestFailure(Throwable error) {
        post(new NetworkErrorEvent(error));
    }

    private void onReservationListReceived(SxMdReservationList result) {
        post(new ReservationsRequestedRequestReturnedEvent(result.getRequestedReservations()));
        post(new ReservationsAcceptedRequestReturnedEvent(result.getAcceptedReservations()));
        post(new ReservationsCompletedRequestReturnedEvent(result.getCompletedReservations()));
    }

    public void onEvent(ReservationsAllDoUpdateEvent event) {
        if (isRequestRunning) {
            return;
        }
        isRequestRunning = true;
        mReservationManager.getDriverAllReservations(MainApplication.getInstance(), mReservationsLoadedListener);
    }
}