package com.mydriver.driver.view.reservationlist.tabs;

import com.sixt.core.eventbus.SxEventBus;

class ReservationsAcceptedPresenter extends ReservationListPresenter {

    public void onEvent(ReservationsAcceptedUpdatedEvent event) {
        updateReservations(event.getReservations());
    }

    @Override
    public void refreshReservations() {
        SxEventBus.postEvent(new ReservationsAcceptedDoUpdateEvent());
    }
}
