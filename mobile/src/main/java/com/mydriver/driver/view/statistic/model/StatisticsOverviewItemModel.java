package com.mydriver.driver.view.statistic.model;


import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.sixt.core.util.SxCurrencyUtils;

import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

import lombok.Data;

@Data
public class StatisticsOverviewItemModel implements Serializable {

    private final static DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();

    private final StatisticsOverviewItemIdModel id;
    private final List<Reservation> reservations;
    private final int reservationCount;
    private final String totalValue;
    private final String totalDistance;
    private final float averageRating;
    private final String month;

    public StatisticsOverviewItemModel(StatisticsOverviewItemIdModel id, List<Reservation> reservations) {
        this.id = id;
        this.reservations = reservations;

        reservationCount = reservations.size();
        float totalValue = 0;
        float totalDistance = 0;
        for (Reservation reservation : reservations) {
            totalValue += reservation.getFloatingPointDriverRideValueNet();
            totalDistance += reservation.getDistanceInKilometers();
        }
        this.month = createDisplayedMonth(id.getMonth());
        this.totalValue = createDisplayedPrice(totalValue, getCurrencyCode(reservations));
        this.totalDistance = MainApplication.getStringResource(R.string.reservation_distance_in_km, createDisplayedDistance(totalDistance));
        this.averageRating = 5.0f; //TODO show correct value as soon as backend provides data
    }

    private String getCurrencyCode(List<Reservation> reservations) {
        if (reservations.isEmpty()) {
            return Currency.getInstance(Locale.getDefault()).getCurrencyCode();
        } else {
            return reservations.get(0).getOffer().getCurrency();
        }
    }

    private String createDisplayedPrice(float value, String currency) {
        NumberFormat priceFormatter = SxCurrencyUtils.getLocalizedPriceFormatter(currency);
        priceFormatter.setMaximumFractionDigits(2);
        return priceFormatter.format(value);
    }

    private String createDisplayedDistance(float distance){
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits(2);
        return decimalFormat.format(distance);
    }

    private String createDisplayedMonth(int month) {
        return dateFormatSymbols.getMonths()[month];
    }
}
