package com.sixt.core.eventbus.presenter;

import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.SxEventBusRegistry;

public abstract class EventListeningPresenter extends EventPostingPresenter {

    public void startEventListening() {
        registerForEvents();
    }

    public void stopEventListening() {
        SxEventBusRegistry.getInstance().unregisterSubscriber(this);
    }

    private void registerForEvents() {
        SxEventBusRegistry.getInstance().registerSubscriber(new SxEventBusRegistry.EventBusSubscriber() {
            @Override
            public Object register(SxEventBus eventBus) {
                eventBus.registerSticky(EventListeningPresenter.this);
                return EventListeningPresenter.this;
            }

            @Override
            public void unregister(SxEventBus eventBus) {
                eventBus.unregister(EventListeningPresenter.this);
            }
        });
    }
}
