package com.sixt.core.eventbus;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;

public class SxEventBus extends EventBus {
    private static volatile SxEventBus defaultInstance;
    private final ScheduledExecutorService mExecutorService;

    private SxEventBus() {
        super();
        mExecutorService = Executors.newSingleThreadScheduledExecutor();
    }

    public ScheduledFuture<Object> postDelayed(Object event, long delay) {
        return mExecutorService.schedule(new PostEventCallable(this, event), delay, TimeUnit.MILLISECONDS);
    }

    private class PostEventCallable implements Callable<Object> {
        private final SxEventBus mEventBus;
        private final Object mEvent;

        public PostEventCallable(SxEventBus eventBus, Object event) {
            mEventBus = eventBus;
            mEvent = event;
        }

        @Override
        public Object call() throws Exception {
            mEventBus.post(mEvent);
            return null;
        }
    }

    public static SxEventBus getInstance() {
        if (defaultInstance == null) {
            synchronized (SxEventBus.class) {
                if (defaultInstance == null) {
                    defaultInstance = new SxEventBus();
                }
            }
        }
        return defaultInstance;
    }

    public static void postEvent(Object event) {
        getInstance().post(event);
    }

    public static void postEventDelayed(Object event, long delay) {
        getInstance().postDelayed(event, delay);
    }

    public static void postStickyEvent(Object event) {
        getInstance().postSticky(event);
    }

    public static <T> T getSticky(Class<T> eventType) {
        return getInstance().getStickyEvent(eventType);
    }

    public static <T> T removeSticky(Class<T> eventType) {
        return getInstance().removeStickyEvent(eventType);
    }
}
