package com.sixt.core.eventbus.presenter;


public abstract class SimpleChildPresenter<View> extends EventPostingPresenter implements ChildPresenter<View> {

    protected View view;

    public void start(View view) {
        this.view = view;
    }

    public void stop() {
        this.view = null;
    }
}
