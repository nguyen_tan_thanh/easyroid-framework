package com.mydriver.driver.view.reservationmap.presenter;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.mydriver.driver.event.actionshow.ShowLocationServiceDisabledDialog;
import com.mydriver.driver.view.reservationmap.ReservationMapView;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.presenter.SimpleChildPresenter;
import com.sixt.mydriver.model.SxMdReservation;

class MapCameraPresenter extends SimpleChildPresenter<ReservationMapView> {

    private static final float DEFAULT_ZOOM_LEVEL = 13;
    private static final int DEFAULT_AREA_PADDING = 150;

    private final Reservation reservation;
    private boolean initialCameraPositionSet;

    public MapCameraPresenter(Reservation reservation) {
        this.reservation = reservation;
    }

    @Override
    public void start(ReservationMapView reservationMapView) {
        super.start(reservationMapView);
        setInitialPosition();
    }

    private void setInitialPosition() {
        if (initialCameraPositionSet) {
            return;
        }
        initialCameraPositionSet = true;
        if (reservation.getType() == SxMdReservation.Type.DISTANCE) {
            setInitialArea();
            return;
        }

        LatLng origin = new LatLng(reservation.getOrigin().getLatitude(), reservation.getOrigin().getLongitude());
        view.moveCameraToPosition(origin, DEFAULT_ZOOM_LEVEL, false);
    }

    private void setInitialArea() {
        LatLng origin = new LatLng(reservation.getOrigin().getLatitude(), reservation.getOrigin().getLongitude());
        LatLng destination  = new LatLng(reservation.getDestination().getLatitude(), reservation.getDestination().getLongitude());
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(origin);
        builder.include(destination);
        LatLngBounds area = builder.build();
        view.moveCameraToArea(area, DEFAULT_AREA_PADDING, false);
    }

    public void onOwnLocationButtonClick() {
        LocationUpdatedEvent locationUpdatedEvent = SxEventBus.getSticky(LocationUpdatedEvent.class);
        if (locationUpdatedEvent == null || locationUpdatedEvent.getLocation() == null) {
            SxEventBus.postEvent(new ShowLocationServiceDisabledDialog());
            return;
        }
        LatLng origin = new LatLng(locationUpdatedEvent.getLocation().getLatitude(), locationUpdatedEvent.getLocation().getLongitude());
        view.moveCameraToPosition(origin, DEFAULT_ZOOM_LEVEL, true);
    }

}
