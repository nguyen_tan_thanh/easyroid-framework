package com.sixt.core.google.maps.geocode;

import com.google.android.gms.maps.model.LatLng;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.sixt.core.google.maps.GMapsApiEndpoint;
import com.sixt.core.google.maps.geocode.model.GMapsAddressPredictionList;
import com.sixt.core.webservice.request.SxBaseRequest;


class GMapsAddressSearchRequest extends SxBaseRequest<GMapsApiEndpoint, GMapsAddressPredictionList> {

    private static final String SERVER_URL = "http://maps.googleapis.com/maps/api";
    private final LatLng location;
    private final String language;
    private final boolean hasSensor = true;

    public GMapsAddressSearchRequest(LatLng location, String language) {
        super(GMapsApiEndpoint.class, GMapsAddressPredictionList.class, SERVER_URL);
        this.location = location;
        this.language = language;
    }

    @Override
    public long getCacheDuration() {
        return DurationInMillis.ONE_MINUTE;
    }

    @Override
    public Object getIdentifier() {
        return (SERVER_URL + GMapsApiEndpoint.URL_GET_ADDRESS_PREDICTION + location.latitude +location.longitude + language).hashCode();
    }

    @Override
    public GMapsAddressPredictionList loadDataFromNetwork() throws Exception {
        return getEndpoint().getAddressForLocation(getLocationAsString(), language, hasSensor, getComponents());
    }

    public String getComponents() {
        return "";
    }

    public String getLocationAsString() {
        return location.latitude +","+ location.longitude;
    }
}
