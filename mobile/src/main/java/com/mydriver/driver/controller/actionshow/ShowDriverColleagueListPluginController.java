package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.controller.base.AnimatingSingleFragmentPluginController;
import com.mydriver.driver.event.actionshow.ShowDriverColleagueListEvent;
import com.mydriver.driver.view.reservationdetails.reassigndriver.DriverColleagueListFragment;


public class ShowDriverColleagueListPluginController extends AnimatingSingleFragmentPluginController<ShowDriverColleagueListEvent, DriverColleagueListFragment> {

    public ShowDriverColleagueListPluginController() {
        super(DriverColleagueListFragment.class);
    }

    public void onEvent(ShowDriverColleagueListEvent event) {
        super.onCapturedEvent(event);

        post(new DriverColleagueListDoUpdateEvent());
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return true;
    }

    @Override
    protected String createActionBarTitle() {
        return "";
    }

    @Override
    protected DriverColleagueListFragment createFragment(ShowDriverColleagueListEvent event) {
        return DriverColleagueListFragment.newInstance(event.getReservation());
    }
}
