package com.mydriver.driver.controller;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.ShowFeedbackMessageEvent;
import com.mydriver.driver.event.actionshow.ShowLocationServiceDisabledDialog;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.core.position.SxPositionManager;
import com.sixt.mydriver.model.FeedbackMessage;

public class LocationPluginController extends SxPluginController {

    private final SxPositionManager positionManager;
    private final LocationListener locationListener = new LocationListener() {
        @Override public void onStatusChanged(String provider, int status, Bundle extras) {}
        @Override public void onProviderEnabled(String provider) {}
        @Override public void onProviderDisabled(String provider) {}

        @Override
        public void onLocationChanged(Location location) {
            postSticky(new LocationUpdatedEvent(location));
        }
    };

    public LocationPluginController(SxPositionManager positionManager) {
        this.positionManager = positionManager;
    }

    public void onEvent(ReservationMapFragmentAttachedEvent event) {
        positionManager.addLocationListener(locationListener);
    }

    public void onEvent(ReservationMapFragmentDetachedEvent event) {
        positionManager.removeLocationListener(locationListener);
    }

    public void onEvent(ShowLocationServiceDisabledDialog event) {
        if (!positionManager.isGPSorNetworkLocationEnabled()) {
            //positionManager.showSettingsAlert(); //TODO
            return;
        }
        FeedbackMessage feedbackMessage = new FeedbackMessage(FeedbackMessage.Type.TOAST, MainApplication.getStringResource(R.string.message_no_gps));
        post(new ShowFeedbackMessageEvent(feedbackMessage));
    }

    public void onEvent(LocationDoUpdateEvent event) {
        Location location = positionManager.getLastKnownLocation();
        postSticky(new LocationUpdatedEvent(location));
    }
}