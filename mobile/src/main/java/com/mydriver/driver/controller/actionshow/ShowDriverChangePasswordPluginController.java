package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.controller.base.AnimatingSingleFragmentPluginController;
import com.mydriver.driver.event.actionshow.ShowDriverEditPasswordEvent;
import com.mydriver.driver.view.driverprofile.editpassword.DriverEditPasswordFragment;

public class ShowDriverChangePasswordPluginController extends AnimatingSingleFragmentPluginController<ShowDriverEditPasswordEvent, DriverEditPasswordFragment> {

    public ShowDriverChangePasswordPluginController() {
        super(DriverEditPasswordFragment.class);
    }

    public ShowDriverChangePasswordPluginController(Class<? extends DriverEditPasswordFragment> fragmentClass) {
        super(fragmentClass);
    }

    public void onEvent(ShowDriverEditPasswordEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldDisplayFragment(ShowDriverEditPasswordEvent showDriverEditPasswordNumberEvent) {
        return true;
    }

    @Override
    protected String createActionBarTitle() {
        return MainApplication.getStringResource(R.string.edit_password);
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return true;
    }

    @Override
    protected DriverEditPasswordFragment createFragment(ShowDriverEditPasswordEvent event) {
        return DriverEditPasswordFragment.newInstance();
    }
}
