package com.mydriver.driver.view.login;

import android.content.Context;

import com.mydriver.driver.event.actionshow.ShowEnvironmentSwitchDialogEvent;
import com.sixt.core.eventbus.presenter.EventListeningPresenter;

import org.apache.commons.lang3.StringUtils;

class LoginPresenter extends EventListeningPresenter {

    private final DriverPreferences driverPreferences;
    private LoginView view;

    public LoginPresenter(DriverPreferences driverPreferences) {
        this.driverPreferences = driverPreferences;
    }

    public void start(LoginView view) {
        this.view = view;
        startEventListening();
        tryAutoLoginOrShowLoginForm();
    }

    public void stop() {
        stopEventListening();
    }

    private void tryAutoLoginOrShowLoginForm() {
        String authToken = driverPreferences.getAuthToken();
        if (StringUtils.isNotEmpty(authToken)) {
            post(new DriverDoLoginEvent(authToken));
            view.setLoginState();
        } else {
            view.showLoginForm();
        }
    }

    public void onEvent(DriverUpdatedEvent event) {
        if (event.getDriver() != null) {
            view.directToMainActivity();
        }
    }

    public void onEvent(DriverLoginReturnedEvent event) {
        if (event.getError() == null) {
            return;
        }
        view.disableLoginButtonProgressState();
        if (event.isErrorBecauseOfWrongCredentials()) {
            view.showInvalidCredentialsValidationErrors();
        }
    }

    public void showEnvironmentSwitchDialog(Context context) {
       post(new ShowEnvironmentSwitchDialogEvent(context));
    }

    public void setInitialEmail() {
        view.fillEmailLastLoggedInDriver(driverPreferences.getEmailLastLoggedInDriver());
    }
}
