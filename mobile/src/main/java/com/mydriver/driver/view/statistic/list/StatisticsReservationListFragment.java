package com.mydriver.driver.view.statistic.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mydriver.driver.R;
import com.mydriver.driver.view.base.ButterKnifeEventReportingFragment;
import com.mydriver.driver.view.reservationlist.tabs.ReservationListAdapter;
import com.mydriver.driver.view.reservationlist.tabs.ReservationListItemModel;
import com.mydriver.driver.view.statistic.model.StatisticsOverviewItemModel;

import java.util.List;
import butterknife.InjectView;

public class StatisticsReservationListFragment extends ButterKnifeEventReportingFragment implements StatisticsReservationListView {

    private static final String EXTRA_STATISTICS_ITEMS = "statistics_items";
    private StatisticsReservationListPresenter mPresenter;
    private ReservationListAdapter mAdapter;

    @InjectView(R.id.statisticsReservationListView)
    ListView mReservationsListView;

    @InjectView(R.id.staticsReservationsRides) TextView mRidesTextView;
    @InjectView(R.id.statisticsReservationsDistance) TextView mTotalDistanceTextView;
    @InjectView(R.id.statisticsReservationsValue) TextView mTotalValueTextView;
    @InjectView(R.id.statisticsReservationsRating) RatingBar mAverageRating;

    public static StatisticsReservationListFragment newInstance(StatisticsOverviewItemModel statisticsOverviewItem) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(EXTRA_STATISTICS_ITEMS, statisticsOverviewItem);
        StatisticsReservationListFragment fragment =  new StatisticsReservationListFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatisticsOverviewItemModel items = (StatisticsOverviewItemModel) getArguments().getSerializable(EXTRA_STATISTICS_ITEMS);
        mPresenter = new StatisticsReservationListPresenter(items);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.statistics_reservation_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupReservationsList();
        mPresenter.start(this);
    }

    private void setupReservationsList() {
        mAdapter = new ReservationListAdapter(getActivity());
        mReservationsListView.setAdapter(mAdapter);
        mReservationsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mPresenter.onItemSelected(position);
            }
        });
    }

    @Override
    public void updateReservationsList(List<ReservationListItemModel> reservations) {
        mAdapter.updateReservationList(reservations);
    }

    @Override
    public void updateHeader(StatisticsOverviewItemModel model) {
        mRidesTextView.setText(getResources().getString(R.string.statistics_rides, model.getReservationCount()));
        mTotalDistanceTextView.setText(model.getTotalDistance());
        mTotalValueTextView.setText(model.getTotalValue());
        mAverageRating.setRating(model.getAverageRating());
    }
}
