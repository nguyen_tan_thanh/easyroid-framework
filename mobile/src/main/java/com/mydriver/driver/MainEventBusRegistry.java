package com.mydriver.driver;

import android.content.Context;

import com.mydriver.driver.controller.ActionBarIntermediateProgressPluginController;
import com.mydriver.driver.controller.CallCustomerPluginController;
import com.mydriver.driver.controller.CallHotlinePluginController;
import com.mydriver.driver.controller.ConnectivityPluginController;
import com.mydriver.driver.controller.DriverColleagueListDoUpdatePluginController;
import com.mydriver.driver.controller.DriverDoEditPasswordPluginController;
import com.mydriver.driver.controller.DriverDoEditPhoneNumberPluginController;
import com.mydriver.driver.controller.DriverDoLoginPluginController;
import com.mydriver.driver.controller.DriverDoLogoutPluginController;
import com.mydriver.driver.controller.EnvironmentSwitchPluginController;
import com.mydriver.driver.controller.EventBusLoggingPluginController;
import com.mydriver.driver.controller.FeedbackMessagePluginController;
import com.mydriver.driver.controller.FlightNumberCopyToClipboardPluginController;
import com.mydriver.driver.controller.GoogleAnalyticsPluginController;
import com.mydriver.driver.controller.LocationPluginController;
import com.mydriver.driver.controller.MainActivityStartupPluginController;
import com.mydriver.driver.controller.NavigateToAddressPluginController;
import com.mydriver.driver.controller.NetworkErrorPluginController;
import com.mydriver.driver.controller.PushNotificationPluginController;
import com.mydriver.driver.controller.PushNotificationReservationDetailsPluginController;
import com.mydriver.driver.controller.ReservationAddToCalendarPluginController;
import com.mydriver.driver.controller.ReservationConfirmAcceptPluginController;
import com.mydriver.driver.controller.ReservationConfirmDropOffPluginController;
import com.mydriver.driver.controller.ReservationConfirmPickUpPluginController;
import com.mydriver.driver.controller.ReservationReassignDriverPluginController;
import com.mydriver.driver.controller.ReservationsAcceptedUpdatePluginController;
import com.mydriver.driver.controller.ReservationsAllUpdatePluginController;
import com.mydriver.driver.controller.ReservationsCompletedUpdatePluginController;
import com.mydriver.driver.controller.ReservationsRequestedUpdatePluginController;
import com.mydriver.driver.controller.RouteRequestPluginController;
import com.mydriver.driver.controller.VehicleListDoUpdatePluginController;
import com.mydriver.driver.controller.actionshow.ShowDriverChangePasswordPluginController;
import com.mydriver.driver.controller.actionshow.ShowDriverChangePhonePluginController;
import com.mydriver.driver.controller.actionshow.ShowDriverColleagueListPluginController;
import com.mydriver.driver.controller.actionshow.ShowDriverProfilePluginController;
import com.mydriver.driver.controller.actionshow.ShowHotlineSelectionDialogPluginController;
import com.mydriver.driver.controller.actionshow.ShowLeftMenuPluginController;
import com.mydriver.driver.controller.actionshow.ShowLoginPluginController;
import com.mydriver.driver.controller.actionshow.ShowSimpleAlertDialogPluginController;
import com.mydriver.driver.controller.actionshow.ShowReservationDetailsPluginController;
import com.mydriver.driver.controller.actionshow.ShowReservationListPluginController;
import com.mydriver.driver.controller.actionshow.ShowStatisticsOverviewPluginController;
import com.mydriver.driver.controller.actionshow.ShowReservationMapFragmentPluginController;
import com.mydriver.driver.controller.actionshow.ShowStatisticsReservationListPluginController;
import com.mydriver.driver.controller.actionshow.ShowVehicleSelectionDialogPluginController;
import com.mydriver.driver.pushnotification.PushNotificationManager;
import com.sixt.core.app.SxAppConst;
import com.sixt.core.eventbus.SxEventBusRegistry;
import com.sixt.core.google.maps.routes.GMapsRouteManager;
import com.sixt.core.position.SxPositionManager;
import com.sixt.mydriver.manager.SxMdAuthenticationManager;
import com.sixt.mydriver.manager.SxMdDriverManager;
import com.sixt.mydriver.manager.SxMdHotlineNumberManager;
import com.sixt.mydriver.manager.SxMdReservationManager;
import com.sixt.mydriver.manager.SxMdVehicleManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainEventBusRegistry extends SxEventBusRegistry {

    private final DriverPreferences mDriverPreferences = DriverPreferences.getInstance();

    private SxMdAuthenticationManager mAuthenticationManager;
    private SxMdDriverManager mDriverManager;
    private SxMdVehicleManager mVehicleManager;
    private SxMdReservationManager mReservationManager;
    private SxMdHotlineNumberManager mHotlineNumberManager;
    private GMapsRouteManager mMapsRouteManager;
    private boolean mManagersStarted;

    public MainEventBusRegistry(Context applicationContext) {
        super(applicationContext);
    }

    private void startManagers() {
        if (mManagersStarted) {
            return;
        }
        mManagersStarted = true;
        mAuthenticationManager.startNotifications(applicationContext);
        mReservationManager.startNotifications(applicationContext);
        mDriverManager.startNotifications(applicationContext);
        mVehicleManager.startNotifications(applicationContext);
        mMapsRouteManager.startNotifications(applicationContext);
        mHotlineNumberManager.startNotifications(applicationContext);
    }

    private void createManagers() {
        mAuthenticationManager = new SxMdAuthenticationManager();
        mDriverManager = new SxMdDriverManager();
        mReservationManager = new SxMdReservationManager();
        mVehicleManager = new SxMdVehicleManager();
        mMapsRouteManager = new GMapsRouteManager();
        mHotlineNumberManager = new SxMdHotlineNumberManager();
    }

    private void stopManagers() {
        mManagersStarted = false;
        mReservationManager.stopNotifications();
        mDriverManager.stopNotifications();
        mVehicleManager.stopNotifications();
        mMapsRouteManager.stopNotifications();
    }

    @Override
    protected void onBeforeRegisterDefaultSubscribers() {
        createManagers();
        startManagers();
    }

    @Override
    protected void onBeforeUnregisterAllEventSubscribers() {
        stopManagers();
    }

    @Override
    protected List<EventBusSubscriber> createDefaultSubscribers() {
        List<EventBusSubscriber> subscribers = new ArrayList<>();
        subscribers.addAll(Arrays.asList(
                new MainActivityStartupPluginController(),
                new ShowLeftMenuPluginController(),
                new ShowReservationDetailsPluginController(),
                new ShowStatisticsOverviewPluginController(),
                new ShowDriverProfilePluginController(),
                new ShowLoginPluginController(),
                new ShowVehicleSelectionDialogPluginController(),
                new ShowReservationMapFragmentPluginController(),
                new ShowReservationListPluginController(),
                new ShowStatisticsReservationListPluginController(),
                new ConnectivityPluginController(applicationContext),
                new DriverDoLoginPluginController(mAuthenticationManager, mDriverManager, mDriverPreferences),
                new DriverDoLogoutPluginController(mDriverPreferences),
                new EnvironmentSwitchPluginController(),
                new VehicleListDoUpdatePluginController(mVehicleManager),
                new RouteRequestPluginController(mMapsRouteManager),
                new LocationPluginController(SxPositionManager.getInstance(applicationContext)),
                new ReservationsAllUpdatePluginController(mReservationManager),
                new ReservationsRequestedUpdatePluginController(mReservationManager),
                new ReservationsAcceptedUpdatePluginController(mReservationManager),
                new ReservationsCompletedUpdatePluginController(mReservationManager),
                new ReservationsRequestedUpdatePluginController(mReservationManager),
                new ReservationConfirmAcceptPluginController(mReservationManager),
                new ReservationConfirmPickUpPluginController(mReservationManager),
                new ReservationConfirmDropOffPluginController(mReservationManager),
                new ReservationAddToCalendarPluginController(),
                new ReservationReassignDriverPluginController(mReservationManager),
                new FeedbackMessagePluginController(),
                new CallCustomerPluginController(),
                new CallHotlinePluginController(mHotlineNumberManager),
                new PushNotificationPluginController(mDriverPreferences, new PushNotificationManager(applicationContext)),
                new PushNotificationReservationDetailsPluginController(),
                new ActionBarIntermediateProgressPluginController(),
                new NavigateToAddressPluginController(),
                new NetworkErrorPluginController(),
                new ShowDriverChangePhonePluginController(),
                new DriverDoEditPhoneNumberPluginController(mDriverManager),
                new DriverDoEditPasswordPluginController(mDriverManager),
                new DriverColleagueListDoUpdatePluginController(mDriverManager),
                new ShowDriverChangePasswordPluginController(),
                new ShowSimpleAlertDialogPluginController(),
                new ShowHotlineSelectionDialogPluginController(),
                new GoogleAnalyticsPluginController(applicationContext),
                new FlightNumberCopyToClipboardPluginController(),
                new ShowDriverColleagueListPluginController()
        ));
        if (BuildConfig.DEBUG) {
            subscribers.add(new EventBusLoggingPluginController());
        }
        return subscribers;
    }

    @Override
    public void registerDefaultSubscribers() {
        super.registerDefaultSubscribers();
        if (!BuildConfig.BUILD_TYPE.equals("release")) {
            eventBus.postSticky(new EnvironmentUpdatedEvent(SxAppConst.SxMdAppEnvironment.STAGING));
        } else {
            eventBus.postSticky(new EnvironmentUpdatedEvent(SxAppConst.SxMdAppEnvironment.PRODUCTION));
        }
    }
}