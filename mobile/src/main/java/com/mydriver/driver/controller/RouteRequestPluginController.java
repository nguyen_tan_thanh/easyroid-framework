package com.mydriver.driver.controller;

import com.google.android.gms.maps.model.LatLng;
import com.mydriver.driver.MainApplication;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.core.google.maps.routes.GMapsRouteManager;
import com.sixt.core.google.maps.routes.model.GMapsRoute;
import com.sixt.core.google.maps.routes.model.GMapsRouteList;

public class RouteRequestPluginController extends SxPluginController {

    private final GMapsRouteManager mRouteManager;

    public RouteRequestPluginController(GMapsRouteManager manager){
        mRouteManager = manager;
    }

    private final GMapsRouteManager.OnRoutesReceivedListener mRouteReceivedListener = new GMapsRouteManager.OnRoutesReceivedListener() {
        @Override
        public void onFailure(Throwable error) {

        }

        @Override
        public void onSuccess(GMapsRouteList routes) {
            if (routes.getItems().size() == 0) {
                return;
            }
            post(new ReservationRouteUpdatedEvent(routes.getItems().get(0)));
        }
    };

    public void onEvent(ReservationRouteDoLoadEvent event) {
        LatLng origin = new LatLng(event.getReservation().getOrigin().getLatitude(), event.getReservation().getOrigin().getLongitude());
        LatLng destination = new LatLng(event.getReservation().getDestination().getLatitude(), event.getReservation().getDestination().getLongitude());
        mRouteManager.getRoutes(MainApplication.getInstance(), origin, destination, GMapsRoute.Mode.DRIVING, mRouteReceivedListener);
    }
}