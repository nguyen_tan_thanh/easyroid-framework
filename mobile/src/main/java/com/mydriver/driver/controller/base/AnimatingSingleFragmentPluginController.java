package com.mydriver.driver.controller.base;


import android.support.v4.app.Fragment;
import com.mydriver.driver.R;

import com.sixt.core.eventbus.plugincontroller.SxSingleFragmentPluginController;

public abstract class AnimatingSingleFragmentPluginController<ShowFragmentTriggerEvent, FragmentType extends Fragment> extends SxSingleFragmentPluginController<ShowFragmentTriggerEvent, FragmentType> {

    public AnimatingSingleFragmentPluginController(Class fragmentClass) {
        super(fragmentClass);
    }

    @Override
    protected int createEnterAnimationResId() {
        if (shouldBeAddedToBackStack()) {
            return R.anim.slide_in_right;
        }
        return R.anim.abc_fade_in;
    }

    @Override
    protected int createExitAnimationResId() {
        if (shouldBeAddedToBackStack()) {
            return R.anim.slide_out_left;
        }
        return R.anim.abc_fade_out;
    }

    @Override
    protected int createPopEnterAnimationResId() {
        return 0;
    }

    @Override
    protected int createPopExitAnimationResId() {
        if (shouldBeAddedToBackStack()) {
            return R.anim.slide_out_left;
        }
        return 0;
    }
}
