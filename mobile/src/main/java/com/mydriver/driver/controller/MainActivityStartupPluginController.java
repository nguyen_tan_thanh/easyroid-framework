package com.mydriver.driver.controller;

import com.mydriver.driver.event.actionshow.ShowLeftMenuEvent;
import com.mydriver.driver.event.actionshow.ShowReservationListsEvent;
import com.sixt.core.eventbus.event.ActivityLifeCycleEvent;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;

public class MainActivityStartupPluginController extends SxPluginController {

    public void onEvent(ActivityLifeCycleEvent event) {
        if (event.isOnPostCreate()) {
            post(new ShowReservationListsEvent());
            post(new ShowLeftMenuEvent());
        }
    }
}