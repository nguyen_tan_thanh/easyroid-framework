package com.mydriver.driver.view.reservationlist.tabs;

import com.mydriver.driver.event.actionshow.ShowReservationDetailsEvent;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.presenter.EventListeningPresenter;

import java.util.ArrayList;
import java.util.List;

abstract class ReservationListPresenter extends EventListeningPresenter {

    protected ReservationListView mView;
    protected final ArrayList<ReservationListItemModel> mReservations = new ArrayList<>();

    private List<ReservationListItemModel> computeReservationListItems(List<Reservation> reservations) {
        List<ReservationListItemModel> reservationListItems = new ArrayList<>();
        for (Reservation reservation : reservations) {
            mReservations.add(new ReservationListItemModel(reservation));
        }
        return reservationListItems;
    }

    public void start(ReservationListView view) {
        mView = view;
        startEventListening();
    }

    public void stop() {
        stopEventListening();
        mView = null;
    }

    public void onItemSelected(int position) {
        SxEventBus.postEvent(new ShowReservationDetailsEvent(mReservations.get(position).getReservation()));
    }

    protected void updateReservations(List<Reservation> reservations) {
        mReservations.clear();
        mReservations.addAll(computeReservationListItems(reservations));
        mView.updateReservationsList(mReservations);
    }

    public abstract void refreshReservations();
}