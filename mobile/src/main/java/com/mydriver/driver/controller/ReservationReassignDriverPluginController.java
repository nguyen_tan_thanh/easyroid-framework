package com.mydriver.driver.controller;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.ShowFeedbackMessageEvent;
import com.mydriver.driver.view.reservationdetails.ReservationDetailsFragment;
import com.mydriver.driver.view.reservationdetails.reassigndriver.DriverColleagueListFragment;
import com.sixt.core.eventbus.event.DetachFragmentEvent;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdReservationManager;
import com.sixt.mydriver.model.FeedbackMessage;
import com.sixt.mydriver.model.SxMdReservation;

public class ReservationReassignDriverPluginController extends SxPluginController {

    private final SxMdReservationManager mReservationManager;

    private SxMdReservationManager.OnReassignToDriverReceivedListener mReceivedListener = new SxMdReservationManager.OnReassignToDriverReceivedListener() {
        @Override
        public void onFailure(Throwable error) {
            onRequestFailure(error);
        }

        @Override
        public void onSuccess(SxMdReservation result) {
            FeedbackMessage message = new FeedbackMessage(FeedbackMessage.Type.TOAST, MainApplication.getStringResource(R.string.assign_reservation_success));
            post(new ShowFeedbackMessageEvent(message));

            reloadAcceptedReservation();
        }
    };

    public ReservationReassignDriverPluginController(SxMdReservationManager mReservationManager) {
        this.mReservationManager = mReservationManager;
    }

    private void onRequestFailure(Throwable error) {
        post(new NetworkErrorEvent(error));
    }

    public void onEvent(ReservationReassignDriverEvent event) {
        mReservationManager.reassignReservationToDriver(event.getReservation().getItem(), event.getNewDriverId(), mReceivedListener);
    }

    private void reloadAcceptedReservation() {
        post(new DetachFragmentEvent(DriverColleagueListFragment.class));
        post(new DetachFragmentEvent(ReservationDetailsFragment.class));
        post(new ReservationsAcceptedDoUpdateEvent());
    }
}