package com.mydriver.driver.controller;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.view.driverprofile.editpassword.DriverEditPasswordFragment;
import com.sixt.core.eventbus.event.DetachFragmentEvent;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdDriverManager;

public class DriverDoEditPasswordPluginController extends SxPluginController {

    private final SxMdDriverManager driverManager;
    private final SxMdDriverManager.OnPasswordChangedListener receivedListener = new SxMdDriverManager.OnPasswordChangedListener() {

        @Override
        public void onFailure(Throwable error) {
            post(new NetworkErrorEvent(error));
        }

        @Override
        public void onSuccess(Object result) {
            post(new DetachFragmentEvent(DriverEditPasswordFragment.class));
        }

    } ;

    public DriverDoEditPasswordPluginController(SxMdDriverManager driverManager) {
        this.driverManager = driverManager;
    }

    public void onEvent(DriverDoEditPasswordEvent event) {
        driverManager.changePasswordNumberDriver(event.getPasswordOld(), event.getPasswordNew(), MainApplication.getInstance(), receivedListener);
    }
}