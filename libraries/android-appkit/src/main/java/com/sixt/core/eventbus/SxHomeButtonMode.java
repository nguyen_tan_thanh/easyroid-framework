package com.sixt.core.eventbus;

import java.io.Serializable;

public enum SxHomeButtonMode implements Serializable {

    NONE, DRAWER_TOGGLE, HOME_AS_UP, HOME_AS_CLOSE;

    public static final String BUNDLE_ID = "extra_home_button_mode";
}
