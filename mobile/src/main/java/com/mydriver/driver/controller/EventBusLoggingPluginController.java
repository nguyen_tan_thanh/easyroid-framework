package com.mydriver.driver.controller;

import android.util.Log;

import com.sixt.core.eventbus.plugincontroller.SxPluginController;

public class EventBusLoggingPluginController extends SxPluginController {

    private static final String TAG = "EventBus";

    public void onEvent(Object event) {
        Log.d(TAG, event.toString());
    }
}
