package com.mydriver.driver.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.DismissFeedbackMessageEvent;
import com.mydriver.driver.event.actionshow.ShowFeedbackMessageEvent;
import com.sixt.core.eventbus.event.ActivityLifeCycleEvent;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.model.FeedbackMessage;

import org.apache.commons.lang3.StringUtils;

public class ConnectivityPluginController extends SxPluginController {

    private final Context context;
    private String noInternetFeedbackMessageId = "";

    public ConnectivityPluginController(Context context) {
        this.context = context;
        registerConnectionReceiver();
    }

    private void registerConnectionReceiver(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        context.registerReceiver(new NetworkStateReceiver(), filter);
    }

    private class NetworkStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean connected = hasNetworkConnectivity();
            if (hasConnectivityChanged(connected)) {
                postConnectivityUpdate(connected);
            }
        }
    }

    private void postConnectivityUpdate(boolean connected) {
        postSticky(new ConnectivityUpdatedEvent(connected));
        if (!connected) {
            FeedbackMessage feedbackMessage = getNoInternetMessage();
            noInternetFeedbackMessageId = feedbackMessage.getId();
            post(new ShowFeedbackMessageEvent(feedbackMessage));
        }
        else if (connected && StringUtils.isNotEmpty(noInternetFeedbackMessageId)) {
            post(new DismissFeedbackMessageEvent(noInternetFeedbackMessageId));
            noInternetFeedbackMessageId = null;
        }
    }

    private boolean hasNetworkConnectivity() {
        boolean hasConnectedWifi = false;
        boolean hasConnectedMobile = false;

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfoList = connectivityManager.getAllNetworkInfo();
        for (NetworkInfo netInfo : netInfoList) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI") && netInfo.isConnected()) {
                hasConnectedWifi = true;
            } else if (netInfo.getTypeName().equalsIgnoreCase("MOBILE") && netInfo.isConnected()) {
                hasConnectedMobile = true;
            }
        }
        return hasConnectedWifi || hasConnectedMobile;
    }

    public void onEvent(ActivityLifeCycleEvent event) {
        if (event.isOnResume()) {
            boolean connected = hasNetworkConnectivity();
            postConnectivityUpdate(connected);
        }
    }

    private boolean hasConnectivityChanged(boolean newConnectivityState) {
        ConnectivityUpdatedEvent event = getStickyEvent(ConnectivityUpdatedEvent.class);
        if (event == null) {
            return true;
        }
        return event.isConnected() != newConnectivityState;
    }

    private FeedbackMessage getNoInternetMessage() {
        String message = MainApplication.getStringResource(R.string.error_no_internet_connection_message);
        return new FeedbackMessage(FeedbackMessage.Type.ERROR, message, FeedbackMessage.Duration.INFINITE);
    }
}