package com.sixt.core.eventbus.event;


import android.support.annotation.AnimRes;
import android.support.v4.app.Fragment;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AttachFragmentEvent extends TransactionFragmentEvent {
    private final Fragment fragment;
    private final boolean addToBackStack;
    private final String actionBarTitle;
    private int enterAnimation;
    private int exitAnimation;
    private int popEnterAnimation;
    private int popExitAnimation;

    public AttachFragmentEvent(Fragment fragment) {
        this(fragment, false, null);
    }

    public AttachFragmentEvent(Fragment fragment, boolean addToBackStack, String actionBarTitle) {
        super((Class<? extends Fragment>) ((Object)fragment).getClass());
        this.fragment = fragment;
        this.addToBackStack = addToBackStack;
        this.actionBarTitle = actionBarTitle;
    }

    public void setAnimations(@AnimRes int enter, @AnimRes int exit, @AnimRes int popEnter, @AnimRes int popExit) {
        enterAnimation = enter;
        exitAnimation = exit;
        popEnterAnimation = popEnter;
        popExitAnimation = popExit;
    }
}
