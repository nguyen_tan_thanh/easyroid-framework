package com.sixt.core.google.maps.geocode.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

import static com.sixt.core.google.maps.geocode.model.GMapsAddressPrediction.Type;

@Data
public class GMapsAddressComponent {
    @SerializedName("long_name")
    private String longName;

    @SerializedName("short_name")
    private String shortName;

    private List<Type> types;
}
