package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.controller.base.AnimatingSingleFragmentPluginController;
import com.mydriver.driver.event.actionshow.ShowStatisticsOverviewEvent;
import com.mydriver.driver.view.statistic.overview.StatisticsOverviewFragment;

public class ShowStatisticsOverviewPluginController extends AnimatingSingleFragmentPluginController<ShowStatisticsOverviewEvent, StatisticsOverviewFragment> {

    public ShowStatisticsOverviewPluginController() {
        super(StatisticsOverviewFragment.class);
    }

    public void onEvent(ShowStatisticsOverviewEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldDisplayFragment(ShowStatisticsOverviewEvent event) {
        return true;
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return false;
    }

    @Override
    protected StatisticsOverviewFragment createFragment(ShowStatisticsOverviewEvent event) {
        return StatisticsOverviewFragment.newInstance();
    }

    @Override
    protected String createActionBarTitle() {
        return MainApplication.getStringResource(R.string.history_title);
    }
}
