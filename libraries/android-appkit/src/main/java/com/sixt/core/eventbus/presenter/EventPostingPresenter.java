package com.sixt.core.eventbus.presenter;

import com.sixt.core.eventbus.SxEventBus;

public abstract class EventPostingPresenter {

    protected void post(Object event) {
        SxEventBus.postEvent(event);
    }

    protected void postSticky(Object event) {
        SxEventBus.postStickyEvent(event);
    }

    protected <T> T getSticky(Class<T> eventClass) {
        return SxEventBus.getSticky(eventClass);
    }
}
