package com.mydriver.driver.view.reservationdetails;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.view.base.GenericListItem;
import com.sixt.core.util.SxCurrencyUtils;
import com.sixt.mydriver.SxMdConstants;
import com.sixt.mydriver.model.SxMdOffer;
import com.sixt.mydriver.model.SxMdReservation;

class ReservationDetailsListItemFactory {

    private ReservationDetailsListItemFactory() {}

    public static GenericListItem newCustomerNameItem(Reservation reservation) {
        String fullName = reservation.getTravellingCustomer().getFirstName() +" "+ reservation.getTravellingCustomer().getLastName();
        GenericListItem item = new GenericListItem(fullName, R.drawable.ico_customer_black);
        item.setActionIconResourceId(R.drawable.ico_phone_selector);
        item.setActionEvent(new CallCustomerEvent(reservation.getContactPhoneNumber(), reservation));
        return item;
    }

    public static GenericListItem newTripDateAndTimeItem(Reservation reservation) {
        String dateAndTime = DateUtils.getDisplayDate(reservation.getPlannedStartDate()) +"     "+ DateUtils.getDisplayTime(reservation.getPlannedStartDate());
        GenericListItem item = new GenericListItem(dateAndTime, R.drawable.ico_calendar_black);
        item.setActionIconResourceId(R.drawable.ico_add_to_calendar_selector);
        item.setActionEvent(new ReservationAddToCalendarEvent(reservation));
        return item;
    }

    public static GenericListItem newTripOriginItem(Reservation reservation) {
        String[] parts = reservation.getOrigin().getName().split(",", 2);
        String title = parts[0].trim();
        GenericListItem item = new GenericListItem(title, R.drawable.ico_route_start_black);
        if (parts.length == 2) {
            item.setSubTitle(parts[1].trim());
        }
        return item;
    }

    public static GenericListItem newTripDestinationItem(Reservation reservation) {
        String[] parts = reservation.getDestination().getName().split(",", 2);
        String title = parts[0].trim();
        GenericListItem item = new GenericListItem(title, R.drawable.ico_route_destination_black);
        if (parts.length == 2) {
            item.setSubTitle(parts[1].trim());
        }
        return item;
    }

    public static GenericListItem newTripDurationItem(Reservation reservation) {
        String title = MainApplication.getStringResource(R.string.reservation_duration_hours, reservation.getDurationInHours());
        return new GenericListItem(title, R.drawable.ico_route_duration_black);
    }

    public static GenericListItem newTripDistanceItem(Reservation reservation) {
        String distance = String.format("%.1f", reservation.getDistanceInKilometers());
        String title = MainApplication.getStringResource(R.string.reservation_distance_in_km, distance);
        return new GenericListItem(title, R.drawable.ico_route_length_black);
    }

    public static GenericListItem newDriverProvisionItem(Reservation reservation) {
        SxMdOffer offer = reservation.getOffer();
        String price = SxCurrencyUtils.getLocalizedPriceFormattedString(reservation.getFloatingPointDriverRideValueNet(), offer.getCurrency());
        String title = MainApplication.getStringResource(R.string.reservation_details_price, price);
        return new GenericListItem(title, R.drawable.ico_money_black);
    }

    public static GenericListItem newVehicleInfoItem(Reservation reservation) {
        String title = MainApplication.getStringResource(reservation.getOffer().getVehicleType().getName().getResourceTextId());
        String subTitle = reservation.getOffer().getVehicleType().getExampleCar();
        GenericListItem item = new GenericListItem(title, R.drawable.ico_vehicle_type_black);
        item.setSubTitle(subTitle);
        return item;
    }

    public static GenericListItem newFlightNumberItem(Reservation reservation) {
        String title = reservation.getFlightNumber();
        String subTitle = MainApplication.getStringResource(R.string.flight_number);
        GenericListItem item = new GenericListItem(title, R.drawable.ico_flight_black);
        item.setSubTitle(subTitle);
        item.setActionIconResourceId(R.drawable.ico_check_fligt_status_selector);
        item.setActionEvent(new FlightNumberDoCopyEvent(reservation.getFlightNumber()));
        return item;
    }

    public static GenericListItem newTrainNumberItem(Reservation reservation) {
        String title = reservation.getTrainNumber();
        String subTitle = MainApplication.getStringResource(R.string.train_number);
        GenericListItem item = new GenericListItem(title, R.drawable.ico_train_black);
        item.setSubTitle(subTitle);
        return item;
    }

    public static GenericListItem newCommentsItem(Reservation reservation) {
        String title = MainApplication.getStringResource(R.string.customer_comments);
        String subTitle = reservation.getComment();
        GenericListItem item = new GenericListItem(title, R.drawable.ico_comments_black);
        item.setSubTitle(subTitle);
        return item;
    }

    public static GenericListItem newSelectedVehicleItem(Reservation reservation) {
        String title = reservation.getVehicle().getLicensePlate();
        String subTitle = reservation.getVehicle().getVehicleDescription();
        GenericListItem item = new GenericListItem(title, R.drawable.ico_vehicle_chosen_black);
        item.setSubTitle(subTitle);
        return item;
    }
}