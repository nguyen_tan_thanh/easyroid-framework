package com.mydriver.driver.view.driverprofile.editphone;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mydriver.driver.R;
import com.mydriver.driver.view.base.ButterKnifeEventReportingFragment;
import com.sixt.core.util.KeyboardUtils;
import com.sixt.core.eventbus.SxEventBus;

import butterknife.InjectView;
import butterknife.OnTextChanged;

public class DriverEditPhoneFragment extends ButterKnifeEventReportingFragment implements DriverEditPhoneView {

    @InjectView(R.id.editPhoneNumberEditText)
    EditText mEditPhoneNumber;

    private DriverEditPhonePresenter mPresenter;

    public static DriverEditPhoneFragment newInstance() {
        return new DriverEditPhoneFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mPresenter = new DriverEditPhonePresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.driver_change_phone_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        preFillData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_change_phone_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save_details:
                KeyboardUtils.hideSoftKeyboard(getActivity());
                mPresenter.changePhone(mEditPhoneNumber.getText().toString());
                break;
        }
        return true;
    }

    @Override
    public void showError() {
        String error = getString(R.string.wrong_international_phone_format);
        mEditPhoneNumber.setError(error);
    }

    @OnTextChanged(R.id.editPhoneNumberEditText)
    void onPhoneNumberTextChanged(CharSequence text) {
        mEditPhoneNumber.setError(null);
    }

    private void preFillData() {
        String phoneNumber = SxEventBus.getSticky(DriverUpdatedEvent.class).getDriver().getMobile();
        mEditPhoneNumber.setText(phoneNumber);
    }
}