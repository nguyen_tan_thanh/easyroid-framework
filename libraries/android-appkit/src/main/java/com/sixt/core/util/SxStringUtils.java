package com.sixt.core.util;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import lombok.NonNull;

public class SxStringUtils {

    public static boolean booleanValue(String value){
        if(value != null){
            return value.equalsIgnoreCase("Y") || value.equalsIgnoreCase("I") || value.equalsIgnoreCase("1");
        }
        return false;
    }

    public static int intValue(String value){
        if(value != null){
            if(value.equalsIgnoreCase("Y") || value.equalsIgnoreCase("I") || value.equalsIgnoreCase("1")) {
                return 1;
            }
        }
        return 0;
    }

    public static Calendar getCalendarFromString(@NonNull String date, @NonNull String format){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            cal.setTime(sdf.parse(date));
            return cal;
        } catch (ParseException e) {
            return cal;
        }
    }

    public static Calendar getCalendarFromStringWithTimezone(String date, String format, String timezone) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        try {
            if (!TextUtils.isEmpty(date)) {
                cal.setTime(sdf.parse(date));
            }
        } catch (ParseException e) {}

        if (!TextUtils.isEmpty(timezone)) {
            cal.setTimeZone(TimeZone.getTimeZone(timezone));
        } else {
            cal.setTimeZone(TimeZone.getDefault());
        }
        return cal;
    }

    public static boolean hasGermanPhoneNumberPrefix(String phoneNumber) {
        return phoneNumber.startsWith("+49");
    }

    public static String getPriceInNumberFormat(String price) {
        String priceCopy = price;
        if (priceCopy.contains(",")) {
            int lastIndexOfComma = priceCopy.lastIndexOf(",");
            if (lastIndexOfComma == priceCopy.indexOf(",")) {
                priceCopy = priceCopy.replace(",", ".");
            } else {
                priceCopy = new StringBuilder(priceCopy).replace(lastIndexOfComma, lastIndexOfComma + 1, ".").toString();
                if (priceCopy.contains(",")) {
                    priceCopy = priceCopy.replace(",", "");
                }
            }
        }
        return priceCopy;
    }

    public static String getLatLongStringFormat(Float value) {
        if (value == null) {
            return null;
        }
        return getLatLongStringFormat(value.floatValue());
    }

    public static String getLatLongStringFormat(float value) {
        return String.format(Locale.US, "%f", value);
    }

    public static String getFormattedDistanceString(float distanceInMeters) {
        if (distanceInMeters < 1000) {
            return ((int) distanceInMeters) + "m";
        } else if (distanceInMeters < 10000) {
            return getFormattedDecimalString(distanceInMeters / 1000f, 1) + "km";
        } else {
            return ((int) (distanceInMeters / 1000f)) + "km";
        }
    }

    static String getFormattedDecimalString(float distanceInKilometers, int decimalPlaces) {
        int factor = (int) Math.pow(10, decimalPlaces);

        int front = (int) (distanceInKilometers);
        int back = (int) Math.abs(distanceInKilometers * (factor)) % factor;

        return front + "." + back;
    }

}
