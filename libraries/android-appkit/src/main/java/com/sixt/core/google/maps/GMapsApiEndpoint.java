package com.sixt.core.google.maps;


import com.sixt.core.google.maps.geocode.model.GMapsAddressPredictionList;
import com.sixt.core.google.maps.routes.model.GMapsRouteList;

import retrofit.http.GET;
import retrofit.http.Query;

public interface GMapsApiEndpoint {

    public static final String URL_GET_ROUTE = "/directions/json";
    public static final String URL_GET_ADDRESS_PREDICTION = "/geocode/json";
    public static final String URL_GET_ADDRESS_FOR_LOCATION = "/geocode/json";

    @GET(URL_GET_ROUTE)
    GMapsRouteList getRoute(
            @Query("origin") String origin,
            @Query("destination") String destination,
            @Query("sensor") boolean hasSensor,
            @Query("units") String units,
            @Query("mode") String mode,
            @Query("language") String language);

    @GET(URL_GET_ADDRESS_PREDICTION)
    GMapsAddressPredictionList getAddressPrediction(
            @Query("address") String address,
            @Query("language") String language,
            @Query("sensor") boolean sensor,
            @Query("components") String components,
            @Query("region") String region);

    @GET(URL_GET_ADDRESS_FOR_LOCATION)
    GMapsAddressPredictionList getAddressForLocation(
            @Query("latlng") String location,
            @Query("language") String language,
            @Query("sensor") boolean sensor,
            @Query("components") String components);
}
