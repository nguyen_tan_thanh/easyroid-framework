package com.sixt.core.google.maps;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sixt.core.google.maps.routes.deserializer.GMapsApiLatLngBoundsDeserializer;
import com.sixt.core.google.maps.routes.deserializer.GMapsApiLatLngDeserializer;
import com.sixt.core.webservice.client.SxHttpConnectionClient;
import com.sixt.core.webservice.exception.SxErrorHandler;
import com.sixt.core.webservice.service.SxBaseRetrofitGsonSpiceService;

import retrofit.ErrorHandler;
import retrofit.client.Client;
import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;

public class GMapsApiGsonSpiceService extends SxBaseRetrofitGsonSpiceService {

    @Override
    protected Client createHttpClient() {
        return new SxHttpConnectionClient();
    }

    @Override
    protected ErrorHandler createErrorHandler() {
        return new SxErrorHandler();
    }

    @Override
    protected Converter createConverter() {
        return new GsonConverter(getGson());
    }

    public Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LatLng.class, new GMapsApiLatLngDeserializer());
        builder.registerTypeAdapter(LatLngBounds.class, new GMapsApiLatLngBoundsDeserializer());
        return builder.create();
    }
}
