package com.sixt.core.eventbus.plugincontroller;

import android.os.Bundle;
import android.support.annotation.AnimRes;
import android.support.v4.app.Fragment;

import com.sixt.core.eventbus.SxHomeButtonMode;
import com.sixt.core.eventbus.event.ActivityLifeCycleEvent;
import com.sixt.core.eventbus.event.AttachFragmentEvent;
import com.sixt.core.eventbus.event.BringFragmentToFrontEvent;
import com.sixt.core.eventbus.event.DetachFragmentEvent;
import com.sixt.core.eventbus.event.FragmentLifeCycleEvent;

public abstract class SxSingleFragmentPluginController<ShowFragmentTriggerEvent, FragmentType extends Fragment> extends SxPluginController {

    private boolean mIsAttached = false;
    private boolean mIsAttachPending = false;
    private boolean mIsActivityStarted = false;

    private Class<? extends Fragment> mFragmentClass;

    public SxSingleFragmentPluginController(Class<? extends FragmentType> fragmentClass) {
        mFragmentClass = fragmentClass;
    }

    public void onCapturedEvent(ShowFragmentTriggerEvent event) {
        if (!shouldDisplayFragment(event)) {
            removeFragment();
            return;
        }

        if (!mIsActivityStarted) {
            return;
        }

        if (mIsAttachPending) {
            return;
        }

        if (mIsAttached) {
            if (shouldBeAddedToBackStack()) {
                post(new BringFragmentToFrontEvent(mFragmentClass, createActionBarTitle()));
            }
            return;
        }

        attachFragment(event);
    }

    protected boolean shouldDisplayFragment(ShowFragmentTriggerEvent event) {
        return true;
    }

    private void attachFragment(ShowFragmentTriggerEvent event) {
        mIsAttachPending = true;
        FragmentType fragment = createFragment(event);
        if (fragment.getArguments() == null) {
            fragment.setArguments(new Bundle());
        }
        fragment.getArguments().putSerializable(SxHomeButtonMode.BUNDLE_ID, createHomeButtonMode());
        AttachFragmentEvent attachFragmentEvent = new AttachFragmentEvent(fragment, shouldBeAddedToBackStack(), createActionBarTitle());
        attachFragmentEvent.setAnimations(createEnterAnimationResId(), createExitAnimationResId(), createPopEnterAnimationResId(), createPopExitAnimationResId());
        post(attachFragmentEvent);
    }

    /**
     * Override this method to set an actionbar title. Return String, empty String or null.
     * Return String to change the actionBarTitle when the fragment is attached
     * Return empty String in case you want to force no title to be shown
     * Return null if the current title should not be changed when this fragment is attached.
     * @return String the actionbar title
     */
    protected String createActionBarTitle() {
        return null;
    }

    protected @AnimRes int createEnterAnimationResId() {
        return 0;
    }

    protected @AnimRes int createExitAnimationResId() {
        return 0;
    }

    protected @AnimRes int createPopEnterAnimationResId() {
        return 0;
    }

    protected @AnimRes int createPopExitAnimationResId() {
        return 0;
    }

    protected abstract boolean shouldBeAddedToBackStack();

    protected abstract FragmentType createFragment(ShowFragmentTriggerEvent event);

    public void onEvent(FragmentLifeCycleEvent event) {
        if (!isOfFragmentType(event)) {
            return;
        }

        if (event.isOnAttach()) {
            onAttach(event);
        }

        if (event.isOnDetach()) {
            onDetach(event);
        }
    }

    protected boolean isOfFragmentType(FragmentLifeCycleEvent event) {
        return isOfFragmentType(event.getFragmentClass());
    }

    protected boolean isOfFragmentType(Class fragmentClass) {
        return fragmentClass.equals(mFragmentClass);
    }

    protected void onAttach(FragmentLifeCycleEvent event) {
        mIsAttached = true;
        mIsAttachPending = false;
    }

    protected void onDetach(FragmentLifeCycleEvent event) {
        mIsAttached = false;
    }

    public void onEvent(ActivityLifeCycleEvent lifeCycleEvent){
        if (lifeCycleEvent.isOnStop()){
            mIsActivityStarted = false;
        } else if (lifeCycleEvent.isOnStart()) {
            mIsActivityStarted = true;
        }
    }

    private void removeFragment() {
        if (!isAttached()) {
            return;
        }
        if (!mIsActivityStarted) {
            return;
        }

        post(new DetachFragmentEvent(mFragmentClass));
    }

    public boolean isAttached() {
        return mIsAttached || mIsAttachPending;
    }

    public boolean shouldHideOnHomeClick() {
        return false;
    }

    public SxHomeButtonMode createHomeButtonMode() {
        if (shouldBeAddedToBackStack()) {
            return SxHomeButtonMode.HOME_AS_UP;
        }
        return SxHomeButtonMode.DRAWER_TOGGLE;
    }
}
