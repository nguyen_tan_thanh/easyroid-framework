package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.controller.base.AnimatingSingleFragmentPluginController;
import com.mydriver.driver.view.reservationmap.ReservationMapFragment;
import com.mydriver.driver.event.actionshow.ShowReservationMapEvent;
import com.sixt.core.eventbus.event.FragmentLifeCycleEvent;

public class ShowReservationMapFragmentPluginController extends AnimatingSingleFragmentPluginController<ShowReservationMapEvent, ReservationMapFragment> {

    public ShowReservationMapFragmentPluginController() {
        super(ReservationMapFragment.class);
    }

    public void onEvent(ShowReservationMapEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldDisplayFragment(ShowReservationMapEvent showReservationMapEvent) {
        return showReservationMapEvent.getReservation() != null;
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return true;
    }

    @Override
    protected ReservationMapFragment createFragment(ShowReservationMapEvent showReservationMapEvent) {
        return ReservationMapFragment.newInstance(showReservationMapEvent.getReservation());
    }

    @Override
    protected String createActionBarTitle() {
        return MainApplication.getStringResource(R.string.ride_map);
    }

    @Override
    protected void onAttach(FragmentLifeCycleEvent event) {
        super.onAttach(event);
        post(new ReservationMapFragmentAttachedEvent());
    }

    @Override
    protected void onDetach(FragmentLifeCycleEvent event) {
        super.onDetach(event);
        post(new ReservationMapFragmentDetachedEvent());
    }
}