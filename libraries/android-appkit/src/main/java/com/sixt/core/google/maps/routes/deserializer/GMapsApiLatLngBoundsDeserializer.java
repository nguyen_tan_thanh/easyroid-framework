package com.sixt.core.google.maps.routes.deserializer;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class GMapsApiLatLngBoundsDeserializer implements JsonDeserializer<LatLngBounds> {

    @Override
    public LatLngBounds deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws
            JsonParseException {

        JsonObject northEastElement = ((JsonObject) jsonElement).get("northeast").getAsJsonObject();
        LatLng northEastLocation = context.deserialize(northEastElement, LatLng.class);

        JsonObject southWestElement = ((JsonObject) jsonElement).get("southwest").getAsJsonObject();
        LatLng southWestLocation = context.deserialize(southWestElement, LatLng.class);

        return new LatLngBounds(southWestLocation, northEastLocation);
    }
}
