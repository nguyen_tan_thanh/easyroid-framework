package com.sixt.core.eventbus.presenter;

public interface ChildPresenter<View>  {
    void start(View view);
    void stop();
}
