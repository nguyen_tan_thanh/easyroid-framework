package com.sixt.core.google.maps.geocode.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class GMapsAddressPredictionList {

    @SerializedName("results")
    private List<GMapsAddressPrediction> items;

    private String status;
}
