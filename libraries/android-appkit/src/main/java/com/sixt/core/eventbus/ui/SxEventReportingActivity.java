package com.sixt.core.eventbus.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * NOTE: The eventbus based architecture only supports one instance of EventReportingActivity so there should also exist one
 * extension of SxEventReportingActivity per application.
 */
public abstract class SxEventReportingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SxLifeCycleEventReporter.reportOnCreate(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        SxLifeCycleEventReporter.reportOnPostCreate(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        SxLifeCycleEventReporter.reportOnStart(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SxLifeCycleEventReporter.reportOnResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SxLifeCycleEventReporter.reportOnPause(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        SxLifeCycleEventReporter.reportOnStop(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SxLifeCycleEventReporter.reportOnDestroy(this);
    }
}
