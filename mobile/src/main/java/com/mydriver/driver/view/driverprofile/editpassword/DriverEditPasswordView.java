package com.mydriver.driver.view.driverprofile.editpassword;

interface DriverEditPasswordView {
    void showErrorPasswordDoNotMatch();
    void showErrorWrongOldPassword();
    void hideError();
}
