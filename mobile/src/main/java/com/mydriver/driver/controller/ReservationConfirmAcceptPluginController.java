package com.mydriver.driver.controller;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.event.actionshow.DismissVehicleSelectionDialogEvent;
import com.mydriver.driver.event.actionshow.ShowVehicleSelectionDialogEvent;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdReservationManager;
import com.sixt.mydriver.model.SxMdReservation;

import java.util.Collections;
import java.util.List;

public class ReservationConfirmAcceptPluginController extends SxPluginController {

    private final SxMdReservationManager reservationManager;
    private Reservation reservationToAccept;

    private final SxMdReservationManager.OnReservationReceivedListener receivedListener = new SxMdReservationManager.OnReservationReceivedListener() {
        @Override
        public void onFailure(Throwable error) {
            post(new NetworkErrorEvent(error));
            post(new DismissVehicleSelectionDialogEvent());
            reservationToAccept = null;
        }

        @Override
        public void onSuccess(SxMdReservation result) {
            post(new DismissVehicleSelectionDialogEvent());
            onReservationAccepted(result);
            reservationToAccept = null;
        }
    };

    public ReservationConfirmAcceptPluginController(SxMdReservationManager reservationManager) {
        this.reservationManager = reservationManager;
    }

    public void onEvent(ReservationConfirmAcceptEvent event) {
        reservationToAccept = event.getReservation();
        post(new ShowVehicleSelectionDialogEvent(reservationToAccept));
    }

    public void onEvent(VehicleSelectedEvent event) {
        reservationManager.acceptReservation(MainApplication.getInstance(), reservationToAccept.getReservationId(), event.getSelectedVehicle().getVehicleId(), receivedListener);
    }

    protected void onReservationAccepted(SxMdReservation reservation) {
        Reservation acceptedReservation = new Reservation(reservation, true);
        updateReservationLists(acceptedReservation);
        post(new ReservationStatusUpdatedEvent(acceptedReservation));
    }

    private void updateReservationLists(Reservation newAcceptedReservation) {
        List<Reservation> requestedReservations = getStickyEvent(ReservationsRequestedUpdatedEvent.class).getReservations();
        List<Reservation> acceptedReservations = getStickyEvent(ReservationsAcceptedUpdatedEvent.class).getReservations();
        int indexToBeRemoved = -1;
        for (int i = 0; i < requestedReservations.size(); i++) {
            if (requestedReservations.get(i).getReservationId() == newAcceptedReservation.getReservationId()) {
                indexToBeRemoved = i;
                break;
            }
        }
        if (indexToBeRemoved > -1) {
            requestedReservations.remove(indexToBeRemoved);
        }
        acceptedReservations.add(newAcceptedReservation);
        Collections.sort(acceptedReservations, new ReservationComparator());
        postSticky(new ReservationsRequestedUpdatedEvent(requestedReservations));
        postSticky(new ReservationsAcceptedUpdatedEvent(acceptedReservations));
    }
}