package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.event.actionshow.ShowVehicleSelectionDialogEvent;
import com.mydriver.driver.view.vehicleselection.VehicleSelectionDialogFragment;
import com.sixt.core.eventbus.plugincontroller.SxSingleFragmentPluginController;

public class ShowVehicleSelectionDialogPluginController extends SxSingleFragmentPluginController<ShowVehicleSelectionDialogEvent, VehicleSelectionDialogFragment> {

    public ShowVehicleSelectionDialogPluginController() {
        super(VehicleSelectionDialogFragment.class);
    }

    public void onEvent(ShowVehicleSelectionDialogEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldDisplayFragment(ShowVehicleSelectionDialogEvent showVehicleSelectionDialogEvent) {
        return true;
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return false;
    }

    @Override
    protected VehicleSelectionDialogFragment createFragment(ShowVehicleSelectionDialogEvent event) {
        return VehicleSelectionDialogFragment.newInstance(event.getReservation());
    }
}