package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.controller.base.AnimatingSingleFragmentPluginController;
import com.mydriver.driver.event.actionshow.ShowDriverEditPhoneNumberEvent;
import com.mydriver.driver.view.driverprofile.editphone.DriverEditPhoneFragment;

public class ShowDriverChangePhonePluginController extends AnimatingSingleFragmentPluginController<ShowDriverEditPhoneNumberEvent, DriverEditPhoneFragment> {

    public ShowDriverChangePhonePluginController() {
        super(DriverEditPhoneFragment.class);
    }

    public ShowDriverChangePhonePluginController(Class<? extends DriverEditPhoneFragment> fragmentClass) {
        super(fragmentClass);
    }

    public void onEvent(ShowDriverEditPhoneNumberEvent event) {
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldDisplayFragment(ShowDriverEditPhoneNumberEvent showDriverEditPhoneNumberEvent) {
        return true;
    }

    @Override
    protected String createActionBarTitle() {
        return MainApplication.getStringResource(R.string.edit_phone_number);
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return true;
    }

    @Override
    protected DriverEditPhoneFragment createFragment(ShowDriverEditPhoneNumberEvent event) {
        return DriverEditPhoneFragment.newInstance();
    }
}
