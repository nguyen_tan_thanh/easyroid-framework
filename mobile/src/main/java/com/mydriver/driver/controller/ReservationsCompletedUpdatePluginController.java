package com.mydriver.driver.controller;


import com.mydriver.driver.MainApplication;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdReservationManager;
import com.sixt.mydriver.model.SxMdReservation;
import com.sixt.mydriver.model.SxMdReservationList;

import java.util.Collections;
import java.util.List;

public class ReservationsCompletedUpdatePluginController extends SxPluginController {

    private final SxMdReservationManager reservationManager;
    private boolean isRequestRunning;

    private SxMdReservationManager.OnReservationsListReceivedListener reservationsLoadedListener = new SxMdReservationManager.OnReservationsListReceivedListener() {
        @Override
        public void onFailure(Throwable error) {
            onRequestFailure(error);
            isRequestRunning = false;
        }

        @Override
        public void onSuccess(SxMdReservationList result) {
            processServerSideReservations(result);
            isRequestRunning = false;
        }
    };

    public ReservationsCompletedUpdatePluginController(SxMdReservationManager reservationManager) {
        this.reservationManager = reservationManager;
    }

    public void onEvent(ReservationsCompletedDoUpdateEvent event) {
        if (isRequestRunning) {
            return;
        }
        isRequestRunning = true;
        reservationManager.getDriverCompletedReservations(MainApplication.getInstance(), reservationsLoadedListener);
    }

    public void onEvent(ReservationsCompletedRequestReturnedEvent event) {
        processServerSideReservations(event.getReservations());
    }

    private void processServerSideReservations(List<SxMdReservation> reservations) {
        List<Reservation> completedReservations = ModelWrapperUtils.wrapReservations(reservations);
        Collections.sort(completedReservations, new ReservationCompletedComparator());
        postSticky(new ReservationsCompletedUpdatedEvent(completedReservations));
    }

    private void onRequestFailure(Throwable error) {
        SxEventBus.postEvent(new NetworkErrorEvent(error));
    }
}
