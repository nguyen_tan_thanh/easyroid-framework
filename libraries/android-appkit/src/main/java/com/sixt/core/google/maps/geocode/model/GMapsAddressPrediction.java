package com.sixt.core.google.maps.geocode.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class GMapsAddressPrediction {
    public static enum Type {
        @SerializedName("route")
        ROUTE,

        @SerializedName("locality")
        LOCALITY,

        @SerializedName("sublocality")
        SUBLOCALITY,

        @SerializedName("political")
        POLITICAL,

        @SerializedName("country")
        COUNTRY,

        @SerializedName("postal_code")
        POSTAL_CODE,

        @SerializedName("point_of_interest")
        POI,

        @SerializedName("establishment")
        ESTABLISHMENT,

        @SerializedName("administrative_area_level_1")
        ADMINISTRATIVE_AREA_1,

        @SerializedName("administrative_area_level_2")
        ADMINISTRATIVE_AREA_2,

        @SerializedName("administrative_area_level_3")
        ADMINISTRATIVE_AREA_3,
    }

    @SerializedName("address_components")
    private List<GMapsAddressComponent> addressComponents;

    @SerializedName("formatted_address")
    private String formattedAddress;

    private GMapsGeometry geometry;

    private List<Type> types;
}
