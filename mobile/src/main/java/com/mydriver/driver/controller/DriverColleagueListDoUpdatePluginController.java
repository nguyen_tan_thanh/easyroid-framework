package com.mydriver.driver.controller;


import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdDriverManager;
import com.sixt.mydriver.model.SxMdDriverList;

public class DriverColleagueListDoUpdatePluginController extends SxPluginController {

    private final SxMdDriverManager driverManager;

    private SxMdDriverManager.OnDriverColleaguesReceivedListener mDriverColleaguesListener = new SxMdDriverManager.OnDriverColleaguesReceivedListener(){

        @Override
        public void onFailure(Throwable error) {
            post(new DriverColleagueListUpdatedEvent(null));
        }

        @Override
        public void onSuccess(SxMdDriverList result) {
            onDriverColleaguesReceived(result);
        }
    };

    public DriverColleagueListDoUpdatePluginController(SxMdDriverManager driverManager) {
        this.driverManager = driverManager;
    }

    public void onEvent(DriverColleagueListDoUpdateEvent event) {
        getDriverColleagues();
    }

    private void getDriverColleagues() {
        driverManager.getDriverColleagues(mDriverColleaguesListener);
    }

    private void onDriverColleaguesReceived(SxMdDriverList result) {
        post(new DriverColleagueListUpdatedEvent(result));
    }
}

