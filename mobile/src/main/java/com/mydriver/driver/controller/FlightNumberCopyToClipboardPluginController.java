package com.mydriver.driver.controller;

import android.content.ClipData;
import android.content.ClipboardManager;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.ShowFeedbackMessageEvent;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.model.FeedbackMessage;

public class FlightNumberCopyToClipboardPluginController extends SxPluginController {

    private final static String FLIGHT_NUMBER_CLIPBOARD_LABEL = "FLIGHT_NUMBER_CLIPBOARD_MANAGER";

    public void onEvent(FlightNumberDoCopyEvent event) {
        ClipboardManager clipboard = (ClipboardManager) MainApplication.getInstance().getSystemService(MainApplication.getInstance().CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(FLIGHT_NUMBER_CLIPBOARD_LABEL, event.getFlightNumber());
        clipboard.setPrimaryClip(clip);

        SxEventBus.postEvent(new ShowFeedbackMessageEvent(new FeedbackMessage(FeedbackMessage.Type.TOAST,
                MainApplication.getInstance().getResources().getString(R.string.flight_number_copied_clipboard) )));
    }
}
