package com.mydriver.driver.view.vehicleselection;

import com.mydriver.driver.event.actionshow.DismissVehicleSelectionDialogEvent;
import com.mydriver.driver.view.base.GenericListItem;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.presenter.EventListeningPresenter;
import com.sixt.mydriver.model.SxMdVehicle;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

class VehicleSelectionPresenter extends EventListeningPresenter {

    private final VehicleSelectionView view;
    private List<SxMdVehicle> vehicles;

    public VehicleSelectionPresenter(VehicleSelectionView view, Reservation reservation) {
        this.view = view;
        SxEventBus.postEvent(new VehicleListDoUpdateEvent(reservation));
    }

    public void onEvent(VehicleListUpdatedEvent event) {
        vehicles = event.getVehicles();
        drawVehicles(vehicles);
    }

    public void onEvent(DismissVehicleSelectionDialogEvent event) {
        view.close();
    }

    private void drawVehicles(List<SxMdVehicle> vehicles) {
        List<GenericListItem> items = new ArrayList<>();
        for (SxMdVehicle vehicle : vehicles) {
            GenericListItem item = new GenericListItem(vehicle.getLicensePlate());
            if (StringUtils.isNotEmpty(vehicle.getVehicleDescription())) {
                item.setSubTitle(vehicle.getVehicleDescription());
            }
            items.add(item);
        }
        view.drawVehicleList(items);
        view.setStateLoadComplete();
    }

    public void onSelectVehicle(int position) {
      SxEventBus.postEvent(new VehicleSelectedEvent(vehicles.get(position)));
        view.setStateLoading();
        view.drawVehicleList(new ArrayList<GenericListItem>());
    }
}