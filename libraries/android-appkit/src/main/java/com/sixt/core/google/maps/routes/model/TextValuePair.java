package com.sixt.core.google.maps.routes.model;

import lombok.Data;

@Data
public class TextValuePair<TextType, ValueType> {
    private TextType text;
    private ValueType value;

    public TextValuePair(TextType text, ValueType value) {
        this.text = text;
        this.value = value;
    }
}
