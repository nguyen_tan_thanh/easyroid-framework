package com.mydriver.driver.view.hotlineselection;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mydriver.driver.R;
import com.mydriver.driver.view.base.StyledEventReportingDialogFragment;
import com.mydriver.driver.view.base.DividerDecoration;
import com.mydriver.driver.view.base.GenericListItem;
import com.mydriver.driver.view.base.GenericListItemAdapter;
import com.mydriver.driver.view.base.WrappingRecyclerViewLayoutManager;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.mydriver.model.SxMdHotlineNumber;
import com.sixt.mydriver.model.SxMdHotlineNumberList;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class HotlineSelectionDialogFragment extends StyledEventReportingDialogFragment {

    private static final String EXTRA_HOTLINE_LIST = "hotline_list";

    @InjectView(R.id.hotlineListView)
    RecyclerView mHotlineListView;

    private GenericListItemAdapter mAdapter;

    public static HotlineSelectionDialogFragment newInstance(SxMdHotlineNumberList hotlineNumberList) {
        HotlineSelectionDialogFragment fragment = new HotlineSelectionDialogFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(EXTRA_HOTLINE_LIST, hotlineNumberList);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View content = getActivity().getLayoutInflater().inflate(R.layout.hotline_list_dialog_content, null);
        ButterKnife.inject(this, content);
        mAdapter = new GenericListItemAdapter();
        mAdapter.setOnItemSelectedListener(new GenericListItemAdapter.OnItemSelectedListener() {
            @Override
            public void onItemSelected(GenericListItem item, int position) {
                SxMdHotlineNumberList hotlineNumberList = (SxMdHotlineNumberList ) getArguments().getSerializable(EXTRA_HOTLINE_LIST);
                SxEventBus.postEvent(new CallHotlineEvent(hotlineNumberList.get(position).getPhoneNumber()));
            }
        });
        mHotlineListView.addItemDecoration(new DividerDecoration(getResources().getDrawable(R.drawable.divider_vertical)));
        mHotlineListView.setLayoutManager(new WrappingRecyclerViewLayoutManager(getActivity()));
        mHotlineListView.setAdapter(mAdapter);
        SxMdHotlineNumberList hotlineNumberList = (SxMdHotlineNumberList ) getArguments().getSerializable(EXTRA_HOTLINE_LIST);
        drawHotlines(hotlineNumberList);
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.select_hotline)
                .setView(content)
                .setNegativeButton(R.string.dialog_cancel, null)
                .create();
    }

    private void drawHotlines(SxMdHotlineNumberList hotlineNumberList) {
        List<GenericListItem> items = new ArrayList<>();
        for (SxMdHotlineNumber hotlineNumber : hotlineNumberList) {
            Locale localCountriesObject = new Locale(Locale.getDefault().getLanguage(), hotlineNumber.getCountryCode());
            GenericListItem item = new GenericListItem(localCountriesObject.getDisplayCountry());
            item.setSubTitle(hotlineNumber.getPhoneNumber());
            items.add(item);
        }
        drawHotlines(items);
    }

    private void drawHotlines(List<GenericListItem> hotlineList) {
        mAdapter.setItems(hotlineList);
    }
}