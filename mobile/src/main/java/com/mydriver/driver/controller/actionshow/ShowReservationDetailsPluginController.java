package com.mydriver.driver.controller.actionshow;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.controller.base.AnimatingSingleFragmentPluginController;
import com.mydriver.driver.event.actionshow.ShowReservationDetailsEvent;
import com.mydriver.driver.view.reservationdetails.ReservationDetailsFragment;

public class ShowReservationDetailsPluginController extends AnimatingSingleFragmentPluginController<ShowReservationDetailsEvent, ReservationDetailsFragment> {

    private String title = "";
    public ShowReservationDetailsPluginController() {
        super(ReservationDetailsFragment.class);
    }

    public void onEvent(ShowReservationDetailsEvent event) {
        title = MainApplication.getStringResource(R.string.reservation_details_title, event.getReservation().getReservationId());
        super.onCapturedEvent(event);
    }

    @Override
    protected boolean shouldDisplayFragment(ShowReservationDetailsEvent showReservationDetailsEvent) {
        return true;
    }

    @Override
    protected String createActionBarTitle() {
        return title;
    }

    @Override
    protected boolean shouldBeAddedToBackStack() {
        return true;
    }

    @Override
    protected ReservationDetailsFragment createFragment(ShowReservationDetailsEvent showReservationDetailsEvent) {
        return ReservationDetailsFragment.newInstance(showReservationDetailsEvent.getReservation());
    }
}