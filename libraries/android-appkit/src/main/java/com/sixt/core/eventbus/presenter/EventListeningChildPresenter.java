package com.sixt.core.eventbus.presenter;

public abstract class EventListeningChildPresenter<View> extends EventListeningPresenter implements ChildPresenter<View> {

    protected View view;

    public void start(View view) {
        this.view = view;
        startEventListening();
    }

    public void stop() {
        stopEventListening();
        this.view = null;
    }
}
