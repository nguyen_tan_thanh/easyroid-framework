package com.mydriver.driver.view.reservationlist.tabs;

import com.mydriver.driver.view.reservationlist.ReservationListTabItem;

class ReservationListPresenterFactory {

    public static ReservationListPresenter createReservationsPresenter(ReservationListTabItem tab) {
        ReservationListPresenter presenter;
        if (tab == ReservationListTabItem.AVAILABLE) {
            presenter = new ReservationsRequestedPresenter();
        } else {
            presenter = new ReservationsAcceptedPresenter();
        }
        return presenter;
    }
}
