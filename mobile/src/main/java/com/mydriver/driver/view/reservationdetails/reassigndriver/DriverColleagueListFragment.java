package com.mydriver.driver.view.reservationdetails.reassigndriver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mydriver.driver.R;
import com.mydriver.driver.view.base.ButterKnifeEventReportingFragment;
import com.mydriver.driver.view.base.DividerDecoration;
import com.mydriver.driver.view.base.GenericListItem;
import com.mydriver.driver.view.base.GenericListItemAdapter;
import com.mydriver.driver.view.base.WrappingRecyclerViewLayoutManager;

import java.util.List;

import butterknife.InjectView;

public class DriverColleagueListFragment extends ButterKnifeEventReportingFragment implements DriverColleagueListView {
    private static final String EXTRA_RESERVATION = "extra_reservation";

    @InjectView(R.id.fragmentDriverColleaguesListView) RecyclerView mListView;

    @InjectView(R.id.fragmentDriverColleaguesProgressBar) ProgressBar mProgressBar;

    private GenericListItemAdapter mAdapter;
    private DriverColleagueListPresenter mPresenter;

    public static DriverColleagueListFragment newInstance(Reservation reservation) {
        DriverColleagueListFragment fragment = new DriverColleagueListFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(EXTRA_RESERVATION, reservation);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new DriverColleagueListPresenter((Reservation) getArguments().getSerializable(EXTRA_RESERVATION));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.driver_colleages_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupDriverColleaguesListView();

        mPresenter.start(this);
    }

    @Override
    public void onDestroyView() {
        mPresenter.stop();
        super.onDestroyView();
    }

    private void setupDriverColleaguesListView() {
        mAdapter = new GenericListItemAdapter();
        mAdapter.setOnItemSelectedListener(new GenericListItemAdapter.OnItemSelectedListener() {
            @Override
            public void onItemSelected(GenericListItem item, int position) {
                mPresenter.doSelectItemAtPosition(getActivity(), position);
            }
        });

        mListView.addItemDecoration(new DividerDecoration(getResources().getDrawable(R.drawable.divider_vertical)));
        mListView.setLayoutManager(new WrappingRecyclerViewLayoutManager(getActivity()));
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void drawDriverColleagues(List<GenericListItem> items) {
        mAdapter.setItems(items);
    }

    @Override
    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }
}