package com.sixt.core.google.maps.routes.model;

import lombok.Data;

@Data
public class GMapsEncodedPolyline {

    private String points;
}
