package com.mydriver.driver.controller;


import com.mydriver.driver.MainApplication;
import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.mydriver.manager.SxMdReservationManager;
import com.sixt.mydriver.model.SxMdReservation;
import com.sixt.mydriver.model.SxMdReservationList;

import java.util.Collections;
import java.util.List;

public class ReservationsAcceptedUpdatePluginController extends SxPluginController {

    private final SxMdReservationManager reservationManager;
    private boolean isRequestRunning;

    private SxMdReservationManager.OnReservationsListReceivedListener reservationsLoadedListener = new SxMdReservationManager.OnReservationsListReceivedListener() {
        @Override
        public void onFailure(Throwable error) {
            onRequestFailure(error);
            isRequestRunning = false;
        }

        @Override
        public void onSuccess(SxMdReservationList result) {
            processServerSideReservations(result);
            isRequestRunning = false;
        }
    };

    public ReservationsAcceptedUpdatePluginController(SxMdReservationManager reservationManager) {
        this.reservationManager = reservationManager;
    }

    public void onEvent(ReservationsAcceptedDoUpdateEvent event) {
        if (isRequestRunning) {
            return;
        }
        reservationManager.getDriverAcceptedAndStartedReservations(MainApplication.getInstance(), reservationsLoadedListener);
    }

    public void onEvent(ReservationsAcceptedRequestReturnedEvent event) {
        processServerSideReservations(event.getReservations());
    }

    private void processServerSideReservations(List<SxMdReservation> reservations) {
        List<Reservation> completedReservations = ModelWrapperUtils.wrapReservations(reservations);
        Collections.sort(completedReservations, new ReservationComparator());
        postSticky(new ReservationsAcceptedUpdatedEvent(completedReservations));
    }

    private void onRequestFailure(Throwable error) {
        SxEventBus.postEvent(new NetworkErrorEvent(error));
    }
}
