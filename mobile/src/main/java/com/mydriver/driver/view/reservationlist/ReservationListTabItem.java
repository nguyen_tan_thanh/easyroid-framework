package com.mydriver.driver.view.reservationlist;


import com.mydriver.driver.R;


public enum ReservationListTabItem {
    AVAILABLE(R.string.reservations_title_available),
    ACCEPTED(R.string.reservations_title_accepted);

    private final int tabTitleResId;

    ReservationListTabItem(int titleId) {
        tabTitleResId = titleId;
    }

    public int getTabTitleResId() {
        return tabTitleResId;
    }
}
