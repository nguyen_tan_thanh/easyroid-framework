package com.mydriver.driver.view.driverprofile.editpassword;

import com.sixt.core.eventbus.SxEventBus;
import com.sixt.core.eventbus.presenter.EventListeningPresenter;

class DriverEditPasswordPresenter extends EventListeningPresenter {

    private final DriverEditPasswordView view;

    DriverEditPasswordPresenter(DriverEditPasswordView view) {
        this.view = view;
    }

    public void changePassword(String passwordOld, String passwordNew, String passwordNewConfirm) {
        if (!passwordNewConfirm.equals(passwordNew)) {
            view.showErrorPasswordDoNotMatch();
        } else {
            SxEventBus.postEvent(new DriverDoEditPasswordEvent(passwordOld,passwordNew));
        }
    }
}