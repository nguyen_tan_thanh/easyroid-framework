package com.mydriver.driver.view.alert;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.mydriver.driver.R;
import com.mydriver.driver.view.base.StyledEventReportingDialogFragment;

public class SimpleAlertDialogFragment extends StyledEventReportingDialogFragment {

    private static final String EXTRA_DIALOG_TITLE = "extra_title";
    private static final String EXTRA_DIALOG_MESSAGE = "extra_message";

    public static SimpleAlertDialogFragment newInstance(String title, String message) {
        SimpleAlertDialogFragment fragment = new SimpleAlertDialogFragment();
        Bundle arguments = new Bundle();
        arguments.putString(EXTRA_DIALOG_TITLE, title);
        arguments.putString(EXTRA_DIALOG_MESSAGE, message);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(getArguments().getString(getArguments().getString(EXTRA_DIALOG_TITLE)))
                .setMessage(getArguments().getString(EXTRA_DIALOG_MESSAGE))
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getDialog().dismiss();
                    }
                })
                .create();
    }
}
