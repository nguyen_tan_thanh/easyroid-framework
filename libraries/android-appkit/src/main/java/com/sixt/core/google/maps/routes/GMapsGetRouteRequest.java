package com.sixt.core.google.maps.routes;


import com.google.android.gms.maps.model.LatLng;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.sixt.core.google.maps.GMapsApiEndpoint;
import com.sixt.core.google.maps.routes.model.GMapsRoute;
import com.sixt.core.google.maps.routes.model.GMapsRouteList;
import com.sixt.core.webservice.request.SxBaseRequest;

public class GMapsGetRouteRequest extends SxBaseRequest<GMapsApiEndpoint, GMapsRouteList> {

    private static final String SERVER_URL = "http://maps.googleapis.com/maps/api";
    private static final String DEFAULT_UNITS = "metric";

    private final LatLng origin;
    private final LatLng destination;
    private final boolean hasSensor;
    private final GMapsRoute.Mode mode;
    private String language = "en";

    public GMapsGetRouteRequest(LatLng origin, LatLng destination, boolean hasSensor, GMapsRoute.Mode mode) {
        super(GMapsApiEndpoint.class, GMapsRouteList.class, SERVER_URL);
        this.origin = origin;
        this.destination = destination;
        this.hasSensor = hasSensor;
        this.mode = mode;
    }

    public GMapsGetRouteRequest(LatLng origin, LatLng destination, boolean hasSensor, GMapsRoute.Mode mode, String language) {
        super(GMapsApiEndpoint.class, GMapsRouteList.class, SERVER_URL);
        this.origin = origin;
        this.destination = destination;
        this.hasSensor = hasSensor;
        this.mode = mode;
        this.language = language;
    }

    @Override
    public long getCacheDuration() {
        return DurationInMillis.ONE_MINUTE;
    }

    @Override
    public Object getIdentifier() {
        return (SERVER_URL + GMapsApiEndpoint.URL_GET_ROUTE + origin.toString() + destination.toString() + mode.toString() + language).hashCode();
    }

    @Override
    public GMapsRouteList loadDataFromNetwork() throws Exception {
        return getEndpoint().getRoute(getStringValue(origin), getStringValue(destination), hasSensor, DEFAULT_UNITS,
                getModeValue(), language);
    }

    private String getModeValue() {
        return mode.toString().toLowerCase();
    }

    private String getStringValue(LatLng location) {
        return location.latitude +","+ location.longitude;
    }
}
