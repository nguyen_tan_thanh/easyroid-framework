package com.mydriver.driver.pushnotification;


import android.content.Context;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import com.sixt.core.app.SxAppConst;
import com.sixt.core.app.SxAppEnvironmentManager;
import com.sixt.mydriver.model.SxMdDriver;

public class PushNotificationManager {

    private final static String PARSE_PARAM_DRIVER_ID = "driverId";
    private final static String PARSE_PARAM_DRIVER_CHANEL_VALUE = "drivers";
    private final Context context;

    private final static String PARSE_APPLICATION_ID = "5VxZIA7O3W6gzfi2tiiFEHLuJFvbpBHy3gyInIoo";
    private final static String PARSE_CLIENT_ID = "DPqhR2xbJUjYCgRoq17DgPpm7gF2yqhbTESosEz3";

    private final static String PARSE_APPLICATION_ID_TEST = "WUWmp55VlC51aC8loPgMruQdPmgcKfqTJFtwMaMR";
    private final static String PARSE_CLIENT_ID_TEST = "HO3Jjcw44iVZWunr0yLYQMhPjRGHowI230GYFrnO";

    public interface OnPushNotificationRequestReturnedListener {
        void onSuccess(boolean active);
        void onFailure(Throwable error);
    }

    public PushNotificationManager(Context context) {
        this.context = context;
    }

    public void initPushNotificationService() {

        if (SxAppEnvironmentManager.getMyDriverAppEnvironment() != SxAppConst.SxMdAppEnvironment.PRODUCTION) {
            Parse.initialize(context, PARSE_APPLICATION_ID_TEST, PARSE_CLIENT_ID_TEST);
            return;
        }

        Parse.initialize(context, PARSE_APPLICATION_ID, PARSE_CLIENT_ID);

    }

    public void subscribeForPushService(final SxMdDriver driver, final OnPushNotificationRequestReturnedListener listener) {

        initPushNotificationService();

        if (driver == null || driver.getDriverId() < 1) {
            throw new IllegalArgumentException("Invalid driver: " + driver);
        }

        ParsePush.subscribeInBackground(PARSE_PARAM_DRIVER_CHANEL_VALUE, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    listener.onFailure(e);
                    return;
                }
                listener.onSuccess(true);
                setDriverOnCurrentInstallationId(driver);
            }
        });
    }

    public void unsubscribeForPushService(final OnPushNotificationRequestReturnedListener listener) {
        ParsePush.unsubscribeInBackground(PARSE_PARAM_DRIVER_CHANEL_VALUE, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    listener.onFailure(e);
                    return;
                }
                listener.onSuccess(false);
            }
        });
    }

    private void setDriverOnCurrentInstallationId(SxMdDriver driver) {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        if (!installation.has(PARSE_PARAM_DRIVER_ID) || driver.getDriverId() != (int) installation.get(PARSE_PARAM_DRIVER_ID)) {
            installation.put(PARSE_PARAM_DRIVER_ID, driver.getDriverId());
            installation.saveInBackground();
        }
    }
}
