package com.mydriver.driver.view.statistic.model;

import lombok.Data;

@Data
public class StatisticsRow {
    private final StatisticsOverviewItemModel first;
    private StatisticsOverviewItemModel second;
    private final String headerText;

    public StatisticsRow(StatisticsOverviewItemModel first, StatisticsOverviewItemModel second) {
        this.first = first;
        this.second = second;
        this.headerText = String.valueOf(first.getId().getYear());
    }

    public StatisticsRow(StatisticsOverviewItemModel first) {
        this(first, null);
    }

    public boolean isRowFull() {
        return first != null && second != null;
    }
}
