package com.mydriver.driver.view.vehicleselection;

import com.mydriver.driver.view.base.GenericListItem;

import java.util.List;


interface VehicleSelectionView {
    void drawVehicleList(List<GenericListItem> vehicles);
    void close();
    void setStateLoadComplete();
    void setStateLoading();
}
