package com.sixt.core.google.maps.routes.model;

import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class GMapsRoute {

    public static enum Mode {
        WALKING, DRIVING;
    }

    private LatLngBounds bounds;

    private List<GMapsLeg> legs;

    @SerializedName("overview_polyline")
    private GMapsEncodedPolyline overviewPolyLine;
}
