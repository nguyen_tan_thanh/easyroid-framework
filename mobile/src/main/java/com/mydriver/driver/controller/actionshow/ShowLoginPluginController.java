package com.mydriver.driver.controller.actionshow;

import android.content.Intent;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.event.actionshow.ShowLoginEvent;
import com.mydriver.driver.view.login.LoginActivity;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;

public class ShowLoginPluginController extends SxPluginController {

    public void onEvent(ShowLoginEvent event) {
        Intent intent = new Intent(MainApplication.getInstance(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MainApplication.getInstance().startActivity(intent);
    }
}
