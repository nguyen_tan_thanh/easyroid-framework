package com.sixt.core.google.maps.routes.model;


import com.google.android.gms.maps.model.LatLng;

import lombok.Data;

@Data
public class GMapsBounds {

    private LatLng northeast;
    private LatLng southwest;
}
