package com.sixt.core.google.maps.routes;


import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.sixt.core.google.maps.GMapsApiGsonSpiceService;
import com.sixt.core.google.maps.routes.model.GMapsRoute;
import com.sixt.core.google.maps.routes.model.GMapsRouteList;
import com.sixt.core.webservice.manager.SxBaseResponseReceivedListener;
import com.sixt.core.webservice.manager.SxBaseRobospiceManager;
import com.sixt.core.webservice.manager.SxRobospiceRequestListener;

public class GMapsRouteManager extends SxBaseRobospiceManager {

    public interface OnRoutesReceivedListener extends SxBaseResponseReceivedListener<GMapsRouteList> {}

    private GMapsGetRouteRequest lastRouteRequest;

    public GMapsRouteManager() {
        super(GMapsApiGsonSpiceService.class);
    }

    public void getRoutes(Context context, LatLng origin, LatLng destination, GMapsRoute.Mode mode,
                          final OnRoutesReceivedListener listener) {
        this.getRoutes(context, origin, destination, mode, listener, false);
    }

    public void getRoutes(Context context, LatLng origin, LatLng destination, GMapsRoute.Mode mode,
                          final OnRoutesReceivedListener listener, boolean ignoreCache) {
        cancelEventuallyRunningRouteRequest();
        lastRouteRequest = new GMapsGetRouteRequest(origin, destination, false, mode);
        RequestListener<GMapsRouteList> requestListener = new RouteRequestListener(listener);
        super.executeRequest(context, lastRouteRequest, requestListener, ignoreCache);
    }

    public void getRoutes(Context context, LatLng origin, LatLng destination, GMapsRoute.Mode mode, final OnRoutesReceivedListener listener, String language, boolean ignoreCache) {
        cancelEventuallyRunningRouteRequest();
        lastRouteRequest = new GMapsGetRouteRequest(origin, destination, false, mode, language);
        RequestListener<GMapsRouteList> requestListener = new RouteRequestListener(listener);
        super.executeRequest(context, lastRouteRequest, requestListener, ignoreCache);
    }

    private void cancelEventuallyRunningRouteRequest() {
        if (lastRouteRequest == null || lastRouteRequest.isCancelled()) {
            return;
        }
        lastRouteRequest.cancel();
        lastRouteRequest = null;
    }

    private class RouteRequestListener extends SxRobospiceRequestListener<GMapsRouteList> {
        public RouteRequestListener(SxBaseResponseReceivedListener<GMapsRouteList> listener) {
            super(listener);
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            lastRouteRequest = null;
            super.onRequestFailure(spiceException);
        }

        @Override
        public void onRequestSuccess(GMapsRouteList result) {
            lastRouteRequest = null;
            super.onRequestSuccess(result);
        }
    }
}