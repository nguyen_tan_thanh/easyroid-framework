package com.sixt.core.app;

import com.sixt.core.webservice.SxJniConnection;

import lombok.Getter;

public class SxAppConst {
    public enum SxAppEnvironment{
        DEVELOPMENT("mmth", SxJniConnection.getDevelopmentCookieValue()),
        TEST("mmps", SxJniConnection.getTestCookieValue()),
        STAGING("mmrd", SxJniConnection.getStagingCookieValue()),
        PRODUCTION("", ""),
        DEV_CATALINA("sx_developer", "cturlea"),
        DEV_HUBER("sx_developer", "huber"),
        ETURN_PROD_CLUSTER("mmct",SxJniConnection.getEturnProdClusterCookieValue()),
        ETURN_TRAINING("mmtr", SxJniConnection.getEturnTrainingCookieValue()),
        EDRIVER_PRODUCTION("mmkp", SxJniConnection.getEdriverProductionCookieValue());

        @Getter final String key;
        @Getter final String value;

        private SxAppEnvironment(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    public enum SxMdAppEnvironment {
        DEMO("https://demo.mydriver-international.com"),
        TEST("https://staging.mydriver-international.com"),
        STAGING("https://staging.mydriver-international.com"),
        PRODUCTION("https://my:d-river@www.mydriver-international.com");

        @Getter final String domain;

        SxMdAppEnvironment(String domain) {
            this.domain = domain;
        }

        public static SxMdAppEnvironment getByOrdinal(int ordinal) {
            return SxMdAppEnvironment.values()[ordinal];
        }
    }
}