package com.mydriver.driver.controller;

import android.content.Context;

import com.mydriver.driver.BuildConfig;
import com.mydriver.driver.view.driverprofile.DriverProfileFragment;
import com.mydriver.driver.view.login.LoginActivity;
import com.mydriver.driver.view.reservationlist.ReservationListTabFragment;
import com.mydriver.driver.view.reservationmap.ReservationMapFragment;
import com.mydriver.driver.view.statistic.list.StatisticsReservationListFragment;
import com.mydriver.driver.view.statistic.overview.StatisticsOverviewFragment;
import com.sixt.core.eventbus.event.ActivityLifeCycleEvent;
import com.sixt.core.eventbus.event.FragmentLifeCycleEvent;
import com.sixt.core.eventbus.plugincontroller.SxPluginController;
import com.sixt.core.util.SxGoogleAnalyticsTracker;

import static com.mydriver.driver.GoogleAnalyticsConstants.*;

public class GoogleAnalyticsPluginController extends SxPluginController {

    private final SxGoogleAnalyticsTracker tracker;

    public GoogleAnalyticsPluginController(Context context) {
        String googleAnalyticsTrackingId = BuildConfig.GA_API_KEY;
        tracker = SxGoogleAnalyticsTracker.getInstance(context);
        tracker.setTrackingId(googleAnalyticsTrackingId);
    }

    public void onEvent(DriverLoginReturnedEvent event) {
        if (event.getError() == null) {
            post(new TrackActionEvent(CATEGORY_DRIVER_ACTION, ACTION_LOGIN_SUCCESS));
        }
        else {
            post(new TrackActionEvent(CATEGORY_DRIVER_ACTION, ACTION_LOGIN_FAILURE));
        }
    }

    public void onEvent(DriverDoLogoutEvent event) {
        post(new TrackActionEvent(CATEGORY_DRIVER_ACTION, ACTION_LOGOUT));
    }

    public void onEvent(ReservationConfirmAcceptEvent event) {
        post(new TrackActionEvent(CATEGORY_RESERVATION_LIFECYCLE_ACTION, ACTION_RESERVATION_ACCEPT));
    }

    public void onEvent(ReservationConfirmPickUpEvent event) {
        post(new TrackActionEvent(CATEGORY_RESERVATION_LIFECYCLE_ACTION, ACTION_RESERVATION_START));
    }

    public void onEvent(ReservationConfirmDropOffEvent event) {
        post(new TrackActionEvent(CATEGORY_RESERVATION_LIFECYCLE_ACTION, ACTION_RESERVATION_COMPLETE));
    }

    public void onEvent(FragmentLifeCycleEvent event) {
        if (!event.isOnCreate()) {
            return;
        }
        String view = null;
        Class fragmentClass = event.getFragmentClass();
        if (fragmentClass == DriverProfileFragment.class) {
            view = VIEW_SCREEN_PROFILE;
        }
        else if (fragmentClass == ReservationMapFragment.class) {
            view = VIEW_SCREEN_RESERVATION_MAP;
        }
        else if (fragmentClass == StatisticsOverviewFragment.class) {
            view =  VIEW_SCREEN_RESERVATION_HISTORY_OVERVIEW;
        }
        else if (fragmentClass == StatisticsReservationListFragment.class) {
            view = VIEW_SCREEN_RESERVATION_HISTORY_MONTH;
        }
        else if (fragmentClass == ReservationListTabFragment.class) {
            view = VIEW_SCREEN_RESERVATION_LIST;
        }
        if (view != null) {
            post(new TrackViewEvent(view));
        }
    }

    public void onEvent(ActivityLifeCycleEvent event) {
        if (!event.isOnCreate()) {
            return;
        }
        if (event.getActivityClass() == LoginActivity.class) {
            post(new TrackViewEvent(VIEW_SCREEN_LOGIN));
        }
    }

    public void onEvent(TrackReservationDetailsViewEvent event) {
        String view = getReservationDetailsViewName(event.getReservation());
        post(new TrackViewEvent(view));
    }

    public void onEvent(CallCustomerEvent event) {
        post(new TrackActionEvent(CATEGORY_DRIVER_ACTION, ACTION_CALL_CUSTOMER));
    }

    public void onEvent(CallHotlineEvent event) {
        post(new TrackActionEvent(CATEGORY_DRIVER_ACTION, ACTION_CALL_HOTLINE, null, event.getPhoneNumber()));
    }

    public void onEvent(DriverDoEditPasswordEvent event) {
        post(new TrackActionEvent(CATEGORY_DRIVER_ACTION, ACTION_CHANGE_PASSWORD));
    }

    public void onEvent(DriverDoEditPhoneNumberEvent event) {
        post(new TrackActionEvent(CATEGORY_DRIVER_ACTION, ACTION_CHANGE_PHONE));
    }

    public void onEvent(ReservationAddToCalendarEvent event) {
        post(new TrackActionEvent(CATEGORY_DRIVER_ACTION, ACTION_RESERVATION_ADD_CALENDAR));
    }

    public void onEvent(PushNotificationActivationDoChangeEvent event) {
        if (event.shouldActivate()) {
            post(new TrackActionEvent(CATEGORY_DRIVER_ACTION, ACTION_PUSH_NOTIFICATION_ENABLE));
        } else {
            post(new TrackActionEvent(CATEGORY_DRIVER_ACTION, ACTION_PUSH_NOTIFICATION_DISABLE));
        }
    }

    private String getReservationDetailsViewName(Reservation reservation) {
        switch (reservation.getStatus()) {
            case REQUESTED:
                return VIEW_SCREEN_RESERVATION_DETAILS_AVAILABLE;
            case ACCEPTED:
                return VIEW_SCREEN_RESERVATION_DETAILS_ACCEPTED;
            case STARTED:
                return VIEW_SCREEN_RESERVATION_DETAILS_STARTED;
            case COMPLETED:
                return VIEW_SCREEN_RESERVATION_DETAILS_COMPLETED;
            default:
                return VIEW_SCREEN_RESERVATION_DETAILS;
        }
    }

    public void onEvent(TrackViewEvent event) {
        tracker.trackPageView(event.getView());
    }

    public void onEvent(TrackActionEvent event) {
        tracker.trackEventwithCategory(event.getCategory(), event.getAction(), event.getLabel(), 0L, event.getView());
    }
}
