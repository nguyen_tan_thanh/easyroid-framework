package com.mydriver.driver.view.reservationlist;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.view.reservationlist.tabs.ReservationListFragment;

class ReservationListTabPagerAdapter extends FragmentStatePagerAdapter implements PagerSlidingTabStrip.TabCustomViewProvider {

    private final static int POSITION_AVAILABLE_RESERVATIONS = 0;
    private final ReservationListTabItem[] mStatusList;
    private final Context mContext;
    private int mUnseenReservations;

    public ReservationListTabPagerAdapter(FragmentManager fm, Context context, int unseenReservations) {
        super(fm);
        mContext = context;
        mStatusList = new ReservationListTabItem[] {ReservationListTabItem.AVAILABLE, ReservationListTabItem.ACCEPTED};
        mUnseenReservations = unseenReservations;
    }

    @Override
    public Fragment getItem(int position) {
        return ReservationListFragment.newInstance(mStatusList[position]);
    }

    @Override
    public int getCount() {
        return mStatusList.length;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public View getPageTabCustomView(int position) {
        View tabView = LayoutInflater.from(mContext).inflate(R.layout.reservation_list_tab, null, false);

        TextView textViewTitle = (TextView) tabView.findViewById(R.id.textViewTabReservations);
        TextView textViewIcon = (TextView) tabView.findViewById(R.id.textViewIconTabReservations);

        textViewTitle.setText(MainApplication.getStringResource(mStatusList[position].getTabTitleResId()).toUpperCase());

        if (position == POSITION_AVAILABLE_RESERVATIONS) {
            textViewIcon.setText(String.valueOf(mUnseenReservations));
        } else {
            textViewIcon.setVisibility(View.GONE);
        }

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width/2, ViewGroup.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        tabView.setLayoutParams(params);

        return tabView;
    }

    public void setUnseenReservations(int unseenReservations) {
        mUnseenReservations = unseenReservations;
    }
}