package com.mydriver.driver.view.statistic.overview;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mydriver.driver.MainApplication;
import com.mydriver.driver.R;
import com.mydriver.driver.event.actionshow.ShowStatisticsReservationListEvent;
import com.mydriver.driver.view.statistic.model.StatisticsOverviewItemModel;
import com.mydriver.driver.view.statistic.model.StatisticsRow;
import com.sixt.core.eventbus.SxEventBus;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

class StatisticsOverviewAdapter extends ArrayAdapter<StatisticsRow> implements StickyListHeadersAdapter {

    private final List<String> mSections = new ArrayList<>();

    public StatisticsOverviewAdapter(Context context) {
        super(context, R.layout.statistics_overview_list_item_row);
    }

    @Override
    public View getHeaderView(int position, View view, ViewGroup viewGroup) {
        View headerView = view;
        StickyHeaderViewHolder titleViewHolder;
        if (headerView == null) {
            headerView = LayoutInflater.from(getContext()).inflate(R.layout.sticky_header_list_title, viewGroup, false);
            titleViewHolder = new StickyHeaderViewHolder(headerView);
            headerView.setTag(titleViewHolder);
        } else {
            titleViewHolder = (StickyHeaderViewHolder) headerView.getTag();
        }

        titleViewHolder.stickyHeaderTitle.setText(getItem(position).getHeaderText().toUpperCase(MainApplication.getUserLocale()));
        return headerView;
    }

    @Override
    public long getHeaderId(int position) {
        StatisticsRow item = getItem(position);
        return mSections.indexOf(item.getHeaderText());
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        StatisticsOverviewRowViewHolder viewHolder;

        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(R.layout.statistics_overview_list_item_row, parent, false);
            viewHolder = new StatisticsOverviewRowViewHolder(rowView);
            rowView.setTag(viewHolder);
        } else {
            viewHolder = (StatisticsOverviewRowViewHolder) rowView.getTag();
        }

        final StatisticsRow rowModel = getItem(position);
        fillItemWithContent(rowModel.getFirst(), viewHolder.leftItemViewHolder);
        viewHolder.leftContentContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectItem(rowModel.getFirst());
            }
        });
        if (rowModel.isRowFull()) {
            viewHolder.rightContentContainer.setVisibility(View.VISIBLE);
            fillItemWithContent(rowModel.getSecond(), viewHolder.rightItemViewHolder);
            viewHolder.rightContentContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSelectItem(rowModel.getSecond());
                }
            });
        } else {
            viewHolder.rightContentContainer.setVisibility(View.INVISIBLE);
        }

        return rowView;
    }

    private void fillItemWithContent(final StatisticsOverviewItemModel model, StatisticsOverviewItemViewHolder view) {
        view.monthTextView.setText(model.getMonth());
        view.ridesTextView.setText(getContext().getResources().getString(R.string.statistics_rides, model.getReservationCount()));
        view.totalDistanceTextView.setText(model.getTotalDistance());
        view.totalValueTextView.setText(model.getTotalValue());
        view.averageRating.setRating(model.getAverageRating());
    }

    public void updateList(List<StatisticsRow> items) {
        mSections.clear();
        mSections.addAll(createSections(items));
        clear();
        addAll(items);
    }

    private void onSelectItem(StatisticsOverviewItemModel itemModel) {
        SxEventBus.postEvent(new ShowStatisticsReservationListEvent(itemModel));
    }

    private Set<String> createSections(List<StatisticsRow> items) {
        Set<String> sections = new HashSet<>();
        for(StatisticsRow item : items) {
            sections.add(item.getHeaderText());
        }
        return sections;
    }

    static class StatisticsOverviewRowViewHolder {
        @InjectView(R.id.staticsOverviewLeftContainer) ViewGroup leftContentContainer;
        @InjectView(R.id.staticsOverviewRightContainer) ViewGroup rightContentContainer;
        StatisticsOverviewItemViewHolder leftItemViewHolder;
        StatisticsOverviewItemViewHolder rightItemViewHolder;

        public StatisticsOverviewRowViewHolder(View view) {
            ButterKnife.inject(this, view);
            leftItemViewHolder = new StatisticsOverviewItemViewHolder(leftContentContainer);
            leftContentContainer.requestDisallowInterceptTouchEvent(true);
            rightItemViewHolder = new StatisticsOverviewItemViewHolder(rightContentContainer);
            rightContentContainer.requestDisallowInterceptTouchEvent(true);
        }
    }

    static class StatisticsOverviewItemViewHolder {
        @InjectView(R.id.statisticsOverviewItemMonth) TextView monthTextView;
        @InjectView(R.id.statisticsOverviewItemRides) TextView ridesTextView;
        @InjectView(R.id.statisticsOverviewItemDistance) TextView totalDistanceTextView;
        @InjectView(R.id.statisticsOverviewItemValue) TextView totalValueTextView;
        @InjectView(R.id.statisticsOverviewItemAverageRating) RatingBar averageRating;

        public StatisticsOverviewItemViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    static class StickyHeaderViewHolder {
        @InjectView(R.id.stickyHeaderListTitleTextView) TextView stickyHeaderTitle;

        public StickyHeaderViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}