package com.mydriver.driver.view.statistic.overview;


import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mydriver.driver.R;
import com.mydriver.driver.view.base.ButterKnifeEventReportingFragment;
import com.mydriver.driver.view.statistic.model.StatisticsRow;

import java.util.List;

import butterknife.InjectView;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class StatisticsOverviewFragment extends ButterKnifeEventReportingFragment implements StatisticsOverviewView {

    @InjectView(R.id.staticsOverviewRefreshContainer)
    SwipeRefreshLayout mSwipeToRefreshContainer;

    @InjectView(R.id.staticsReservationsListView)
    StickyListHeadersListView mStatisticsListView;

    private StatisticsOverviewPresenter mPresenter;
    private StatisticsOverviewAdapter mAdapter;

    public static StatisticsOverviewFragment newInstance() {
        return new StatisticsOverviewFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new StatisticsOverviewPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.statistics_overview_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupSwipeToRefreshContainer();
        setupStatisticsList();
        mPresenter.start(this);
    }

    @Override
    public void onDestroyView() {
        mPresenter.stop();
        super.onDestroyView();
    }

    private void setupSwipeToRefreshContainer() {
        mSwipeToRefreshContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.refreshStatistics();
            }
        });
        mSwipeToRefreshContainer.setColorSchemeResources(
                R.color.MyDriver_Design_blue,
                R.color.MyDriver_Design_yellow,
                R.color.MyDriver_Design_green,
                R.color.MyDriver_Design_red);
    }

    private void setupStatisticsList() {
        mAdapter = new StatisticsOverviewAdapter(getActivity());
        mStatisticsListView.setAdapter(mAdapter);
    }

    @Override
    public void updateStatisticsOverview(List<StatisticsRow> rowItems) {
        mSwipeToRefreshContainer.setRefreshing(false);
        mAdapter.updateList(rowItems);
    }
}
